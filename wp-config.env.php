<?php
/**
 * Setup environments
 *
 * Set environment based on the current server hostname, this is stored
 * in the $hostname variable
 *
 * You can define the current environment via:
 *     define('WP_ENV', 'production');
 *
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */


/*
 * Set environment based on hostname
 *
 * If you just use localhost for your local test environment then in place of:
 *   case 'domain.dev':
 *
 * Just use:
 *   case 'localhost':
 *
 */
switch ($hostname) {
	case 'localhost:8080':
        define('WP_ENV', 'development');
        break;
    case 'funnelwide.dp.dsdev':
        define('WP_ENV', 'dp');
        break;
    case 'funnelwide.ac.dsdev':
        define('WP_ENV', 'ac');
        break;
    case 'funnelwide.ads.dsdev':
        define('WP_ENV', 'ads');
        break;
    case 'funnelwide.df.dsdev':
        define('WP_ENV', 'df');
        break;
    case 'funnelwide.jb.dsdev':
        define('WP_ENV', 'jb');
        break;
    case 'stage.funnelwide.com':
    case 'wordpress-36807-78388-215451.cloudwaysapps.com':
        define('WP_ENV', 'staging');
        break;
    case 'www.funnelwide.com':
    case 'wordpress-36807-78388-215434.cloudwaysapps.com':
    default:
        define('WP_ENV', 'production');
}
