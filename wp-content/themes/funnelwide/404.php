<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package funnelwide
 */

get_header('generic'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

      <div class="Panel Panel--soft-double Panel--neon-carrot Panel--center">
        <?php 
          $errorpage = get_page_by_title( '404' );
          print_r($errorpage);
        ?>
        <p>404 Error</p>
        <h1>Page Not Found</h1>
        <p>We’re sorry, the page you are looking for was removed or does not exist.</p>
        <a href="/" class="Button Button--block">Return Home</a>
      </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
