<?php
/**
 * funnelwide functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package funnelwide
 */

if ( ! function_exists( 'funnelwide_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function funnelwide_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on funnelwide, use a find and replace
	 * to change 'funnelwide' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'funnelwide', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'funnelwide' ),
		'footer' => esc_html__( 'Footer', 'funnelwide' ),
		'sub_footer' => esc_html__( 'Sub Footer', 'funnelwide' ),
		'mobile' => esc_html__( 'Mobile', 'funnelwide' ),
	) );


	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'funnelwide_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'funnelwide_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function funnelwide_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'funnelwide_content_width', 640 );
}
add_action( 'after_setup_theme', 'funnelwide_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function funnelwide_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Pullout', 'funnelwide' ),
		'id'            => 'pullout',
		'description'   => esc_html__( 'Displayed in the pullout.', 'funnelwide' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Contact', 'funnelwide' ),
		'id'            => 'contact',
		'description'   => esc_html__( 'Displayed anywhere the contact form is visible.', 'funnelwide' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Below Article', 'funnelwide' ),
		'id'            => 'below-article',
		'description'   => esc_html__( 'Displayed below article content.', 'funnelwide' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar', 'funnelwide' ),
		'id'            => 'right-rail-sidebar',
		'description'   => esc_html__( 'Displayed on the right rail of posts.', 'funnelwide' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'funnelwide_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function funnelwide_scripts() {
	wp_enqueue_style('funnelwide-style', get_template_directory_uri() . '/dist/style.css', false, '1.3.5', 'all');

    wp_enqueue_script( 'funnelwide-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

    wp_enqueue_script( 'funnelwide-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '1.0', true );

    wp_enqueue_script( 'funnelwide-svgxuse', get_template_directory_uri() . '/dist/svgxuse.js', array(), '1.0', true );

    wp_enqueue_script( 'funnelwide-vendor', get_template_directory_uri() . '/dist/vendor.js', array(), '1.0', true );


    wp_enqueue_script( 'funnelwide-app', get_template_directory_uri() . '/dist/bundle.js', array(), '1.1.6', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'funnelwide_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 *  Implement Customer header Menu Walker
 */
require get_template_directory() . '/inc/custom-walker-menus.php';

require get_template_directory() . '/inc/acf-svg-choices.php';
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 *  Implement Custom Post types.
 */

require get_template_directory() . '/inc/custom-post-types/index.php';

require get_template_directory() . '/inc/image_transforms.php';

require get_template_directory() . '/inc/options-pages.php';
add_filter('the_content', 'wpse_ad_content');

function wpse_ad_content($content)
{
    if (!is_single()) return $content;
    $paragraphAfter = 2; //Enter number of paragraphs to display ad after.
    $content = explode("</p>", $content);
    $new_content = '';
    for ($i = 0; $i < count($content); $i++) {
        if ($i == $paragraphAfter) {
            $new_content.= '<div style="">';
            $new_content.= '<script>!function(r,u,m,b,l,e){r._Rumble=b,r[b]||(r[b]=function(){(r[b]._=r[b]._||[]).push(arguments);if(r[b]._.length==1){l=u.createElement(m),e=u.getElementsByTagName(m)[0],l.async=1,l.src="https://rumble.com/embedJS/u7ekp"+(arguments[1].video?\'.\'+arguments[1].video:\'\')+"/?url="+encodeURIComponent(location.href)+"&args="+encodeURIComponent(JSON.stringify([].slice.apply(arguments))),e.parentNode.insertBefore(l,e)}})}(window, document, "script", "Rumble");</script><div id="rumble_caeditor-picks"></div><script>Rumble("play", {"video": "caeditor-picks","div": "rumble_caeditor-picks","autoplay":2, float_offset:[10,20,1,1],float_size:[310,310],float_mobile_offset:[\'4%\',20,1,1],float_mobile_size:[\'65%\',\'65%\',!1,\'215px\'],float:\'always\',rel:5});</script>';
            $new_content.= '</div>';
        }

        $new_content.= $content[$i] . "</p>";
    }

    return $new_content;
}