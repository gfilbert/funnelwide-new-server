<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package funnelwide
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main Case-study-single" role="main">

    <?php
    while ( have_posts() ) : the_post(); ?>

      <section class="Panel Panel--soft-double Panel--center Panel--fixed ">
        <div class="Panel__container">
          <div class="Case-study-single__challenge">
            <div class="Case-study-single__left-circles">
              <?php get_template_part('img/svg/left', 'panel-circles.svg'); ?>
            </div>
          <div class="Case-study-single__challenge-content">
            <h1>The Challenge</h1>
            <?php echo get_field('the_challenge') ?>
          </div>
          <div class="Case-study-single__right-circles">
            <?php get_template_part('img/svg/right', 'panel-circles.svg'); ?>
          </div>
          </div>
        </div>
      </section>

      <section class="Panel Panel--soft Panel--overflow Panel--concrete">
       <div class="Panel__container">
         <div class="Icon-copy Icon-copy--flipped Icon-copy--overflow-both">
           <div class="Icon-copy__icon">
             <div class="Icon-copy__icon-overlay">
               <?php get_template_part('img/svg/Funnelwide', 'CaseStudy-Approach-Overlay.svg'); ?>
             </div>
            <img src="<?php echo get_field('our_approach_image')['url'] ?>" alt="">
           </div>
           <div class="Icon-copy__content">
             <h1>Our Approach</h1>
             <?php echo get_field('our_approach') ?>
           </div>

         </div>
       </div>
     </section>

    <section class="Panel Panel--soft-double Panel--center">
      <div class="Panel__container">
        <h1>The Results</h1>
        <?php echo get_field('featured_content_grid_copy') ?>
  <?php if( have_rows('featured_content_grid_items') ): ?>
        <div class="Featured-content-grid Featured-content-grid--3 Featured-content-grid--animate">

      <?php while( have_rows('featured_content_grid_items') ): the_row(); ?>
          <div class="Featured-content-grid__item">
            <div class="Featured-content-grid__icon">
              <?php get_template_part('img/svg/icon', get_sub_field('svg_icon')); ?>
            </div>
            <h3><?php echo get_sub_field('heading'); ?></h3>
            <?php echo get_sub_field('copy') ?>
          </div>

          <?php endwhile; ?>


      </div>
          <?php endif; ?>
    </section>

     <section class="Panel Panel--neon-carrot Panel--soft-double Panel--center ">
       <div class="Panel__container">
          <div class="Testimonial">

            <div class="Testimonial__left-graphic">
              <?php get_template_part('img/svg/left', 'quote-panel-circles.svg'); ?>
            </div>

            <h2 class="Testimonial__cite">“<?php echo get_field('testimonial_cite'); ?>”</h2>
            <h3><?php echo get_field('testimonial_name'); ?></h3>
            <p><?php echo get_field('testimonial_role'); ?></p>

            <div class="Testimonial__right-graphic">
              <?php get_template_part('img/svg/right', 'quote-panel-circles.svg'); ?>
            </div>
         </div>
       </div>
     </section>

      <?php
      endwhile; // End of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer();
