<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package funnelwide
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<link rel="apple-touch-icon" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="384x384"  href="/android-chrome-384x384.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/mstile-150x150.png">
<meta name="theme-color" content="#ffffff">
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

  <?php include(locate_template('components/pullout.php')); ?>


  <header class="Header Header--at-top Header--masthead">
    <div class="Header__inner">
     <?php if(get_field('dashboard_login', 'option')) { ?>
      <!--<div class="Header__meta-nav">
        <ul class="Menu Menu--small">
          <li class="Menu__item"> <a href="<?php echo get_field('dashboard_login', 'option') ?>" target="_blank"><svg class="icon icon-user"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#user"></use></svg> Dashboard Login</a></li>
        </ul>
      </div>-->
      <?php } // END if(get_field('dashboard_login', 'option')) ?>
      <a href="/" class="Header__logo">
          <?php get_template_part('img/header', 'logo.svg'); ?>
      </a>
      <a href="#menuDrawer" class="Header__menu-icon open-pullout">
        <svg class="icon icon-menu"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#menu"></use></svg>
      </a>
      <nav class="Header__nav">
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'Menu', 'walker' => new funnelwide_walker_nav_menu) ); ?>
      </nav>
    </div>
  </header>

<?php get_template_part('components/masthead'); ?>
<div id="content" class="site-content">
