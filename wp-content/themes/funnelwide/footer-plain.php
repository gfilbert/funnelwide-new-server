<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package funnelwide
 */

?>

	</div><!-- #content -->

  <?php include(locate_template('page-builder/pre-footer-content.php', false, false)); ?>


</div><!-- #page -->
<div class="Pullout__cover-content"></div>
<?php wp_footer(); ?>
<script type='text/javascript'>
(function (d, t) {
  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
  bh.type = 'text/javascript';
  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=ubw4vxylawc7xzu6eonjfw';
  s.parentNode.insertBefore(bh, s);
  })(document, 'script');
</script>
</body>
</html>
