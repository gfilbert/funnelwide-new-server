<?php

if (!empty($_GET["name"])) {
  
  // Format Mail
  $to = '"Jonathan Markiles" <jmarkiles@funnelwide.com>';
  $subject = "Message from " .$_GET["name"];
  $body = str_replace("=", ": ", urldecode(http_build_query($_GET, '', "<br><br>")));
  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= 'From: "Funnelwide" <contact@funnelwide.com>';

  // Send Mail
  mail($to,$subject,$body,$headers);
  
}
?>