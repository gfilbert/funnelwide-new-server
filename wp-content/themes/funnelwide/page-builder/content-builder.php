<?php

// check if the flexible content field has rows of data
if( have_rows('content') ):

     // loop through the rows of data
    while ( have_rows('content') ) : the_row();

        if( get_row_layout() == 'copy_panel' ):

        include(locate_template('page-builder/components/copy-panel.php', false, false));

        elseif( get_row_layout() == 'icon_copy_panel' ):

          include(locate_template('page-builder/components/icon-copy-panel.php', false, false));

        elseif( get_row_layout() == 'icon_copy_group' ):

          include(locate_template('page-builder/components/icon-copy-group.php', false, false));

        elseif( get_row_layout() == 'full_bleed_image_panel' ):

          the_sub_field('copy');


        elseif( get_row_layout() == 'featured_content' ):

          include(locate_template('page-builder/components/featured-content.php', false, false));


        elseif( get_row_layout() == 'cta_panel' ):
          include(locate_template('page-builder/components/cta-panel.php', false, false));

        elseif( get_row_layout() == 'featured_content_grid' ):
          include(locate_template('page-builder/components/featured-content-grid.php', false, false));

        // elseif( get_row_layout() == '' ):


        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
