<?php
  $color = get_sub_field('color');
  $classes = 'Panel Panel--center ';
  if ($color != 'none') {
    $classes .= 'Panel--' . $color;
  }

$copy = get_sub_field('featured_content_grid_copy');
 ?>

<section class="Panel Panel--soft-double Panel--center<?php echo $classes; ?>">
  <div class="Panel__container">
    <?php echo $copy; ?>

    <?php if( have_rows('featured_content_grid_items') ):
      $count = 0;
      while (have_rows('featured_content_grid_items')) {
        the_row();
        $count++;
      } ?>
      <div class="Featured-content-grid Featured-content-grid--<?php echo $count; ?> Featured-content-grid--animate">
      <?php while( have_rows('featured_content_grid_items') ): the_row(); ?>
        <div class="Featured-content-grid__item Featured-content-grid__item--animate">
          <div class="Featured-content-grid__icon">
            <?php if(get_sub_field('svg_icon_or_image') == 'icon'): ?>
              <?php get_template_part('img/svg/icon', get_sub_field('svg_icon')); ?>
            <?php else: ?>
              <img src="<?php echo get_sub_field('image_icon')['url']; ?>" alt="">
            <?php endif; ?>
          </div>
          <h3 class="Featured-content-grid__title"><?php echo get_sub_field('heading'); ?></h3>
          <?php if (get_sub_field('copy')): ?>
            <div class="Featured-content-grid__copy">
              <?php echo get_sub_field('copy'); ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endwhile; ?>
      </div>
    <?php endif; ?>
    <?php if (have_rows('featured_content_grid_buttons')): ?>
      <div class="Masthead__button-group">
        <?php  while ( have_rows('featured_content_grid_buttons') ) : the_row();
        include(locate_template('page-builder/components/button.php')); ?>

        <?php endwhile; ?>
      </div>
    <?php endif ?>
  </div>
</section>
