<?php
  $color = get_sub_field('color');
  $classes = '';
  $icon_classes = '';

  if ($color != 'none') {
    $classes .= " Panel--{$color}";
  }

  switch (get_sub_field('overflow')) {
    case 'both':
      $classes .= ' Panel--overflow';
    break;

    case 'bottom':
      $classes .= ' Panel--overflow';
      $icon_classes .= ' Icon-copy--overflow-bottom ';
    break;

    case 'top':
      $classes .= ' Panel--overflow';
    break;

    case 'none':
      $classes .= '';
      $icon_classes = '';
    break;
  }

  switch (get_sub_field('padding')) {
    case 'none':
      $classes .= '';
    break;
    case 'padding':
      $classes .= ' Panel--soft';
    break;
    case 'double':
      $classes .= ' Panel--soft-double';
    break;
  }
  
  if (get_sub_field('icon_size') == 'large') {
    $icon_classes .= '';
  } elseif (get_sub_field('icon_size') == 'medium') {
    $icon_classes .= ' Icon-copy--group';
  } else {
    $icon_classes .= ' Icon-copy--small';
  }

  $icon_classes .= (get_sub_field('icon_position') == 'left' ? '' : ' Icon-copy--flipped');

  $media = get_sub_field('svg_icon_or_image');
?>

  <section class="Panel<?php echo $classes ?>">
    <div class="Panel__container">
      <div class="Icon-copy<?php echo $icon_classes ?>">
        <div class="Icon-copy__icon">
          <?php if($media == 'icon'): ?>
            <?php get_template_part('img/svg/icon', get_sub_field('svg_icon')); ?>
          <?php else: ?>
            <img src="<?php echo get_sub_field('image_icon')['url']; ?>" alt="">
          <?php endif; ?>
        </div>
        <div class="Icon-copy__content">
            <?php the_sub_field('title'); ?>
            <?php the_sub_field('copy'); ?>
        </div>
      </div>
      <div class="Icon-copy__buttons">
        <?php include(locate_template('page-builder/components/button.php')); ?>
      </div>
    </div>
  </section>
