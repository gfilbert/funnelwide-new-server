<?php
  $classes = ' ';
  $classes .= 'Panel--' . get_sub_field('background_color');
  $classes .= (get_sub_field('overlap') ? ' Panel--overlap ' : '' );
?>

<?php if( have_rows('icon_copy_panels') ): ?>

<div class="Panel Panel--soft <?php echo $classes; ?>">
  <div class="Panel__container">
    <?php $count = 0; ?>
    <?php while( have_rows('icon_copy_panels') ): the_row(); $count++; ?>
    <?php $icon_copy = ' ' ?>
    <?php if ($count % 2 == 0) {
      $icon_copy .= ' Icon-copy--flipped';
    } ?>
    <div class="Icon-copy Icon-copy--group <?php echo $icon_copy; ?>">
      <div class="Icon-copy__icon">
        <?php if(get_sub_field('svg_icon_or_image') == 'icon'): ?>
          <?php get_template_part('img/svg/icon', get_sub_field('svg_icon')); ?>
        <?php else: ?>
          <img src="<?php echo get_sub_field('image_icon')['url']; ?>" alt="">
        <?php endif; ?>
      </div>
      <div class="Icon-copy__content">
        <?php echo get_sub_field('title'); ?>
        <?php echo get_sub_field('copy'); ?>
      </div>
    </div>
    <?php endwhile; ?>

  </div>
</div>
    <?php endif; ?>
