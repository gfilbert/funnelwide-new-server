<?php
if (get_sub_field('show_button') == true):
$btnClasses = '';
$link = get_sub_field('url');
$new_tab = (get_sub_field('type') == 'external' ? 'target="_blank"' : '');
$btnClasses .= (get_sub_field('pullout') ? ' open-pullout ' : '');
$btnClasses .= get_sub_field('style'); ?>
  <a href="<?php echo $link ?>" class="Button <?php echo $btnClasses ?>" <?php echo $new_tab; ?>>
    <?php echo get_sub_field('text') ?>
  </a>
<?php endif; ?>
