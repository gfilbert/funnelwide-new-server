<?php
  $color = get_sub_field('color');
  $classes = 'Panel Panel--center ';
  if ($color != 'none') {
    $classes .= "Panel--{$color}";
  }
  $image = get_sub_field('image');
?>
<section class="Panel">
    <div class="Panel__container Panel__container--fluid">
      <div class="Featured-content">
        <div class="Featured-content__content">
          <div class="Featured-content__inner">
            <p class="Featured-content__title"><?php echo get_sub_field('title') ?></p>
            <?php echo get_sub_field('copy') ?>
            <?php include(locate_template('page-builder/components/button.php')); ?>
          </div>
        </div>
        <div class="Featured-content__image" style="background-image: url('<?php echo $image['url'] ?> ')"></div>
      </div>
    </div>
  </section>
