<?php

  $overlap = (get_sub_field('overlap') ? 'Panel--cta-overlap' : '' );
  $height = (get_sub_field('height') != 'content' ? 'Panel--'.get_sub_field('height')  : '' );
  $background = get_sub_field('background_image');
  $mobileBackground = get_sub_field('background_image_mobile');
  $animate = (get_sub_field('animate') ? 'Panel-cta--animate' : '')

?>
<section class="Panel Panel--cta <?php echo $overlap; ?> <?php echo $height; ?> <?php echo $animate; ?>" style="background-image: url('<?php echo $background['url']; ?>')">
  <div class="Panel__container">
    <?php the_sub_field('copy'); ?>
    <?php include(locate_template('page-builder/components/button.php')); ?>
  </div>
</section>

<section class="Panel Panel--cta Panel--fixed-mobile <?php echo $overlap; ?> <?php echo $height; ?>" style="background-image: url('<?php echo $mobileBackground['url']; ?>')">
  <div class="Panel__container">
    <?php the_sub_field('copy'); ?>
    <?php include(locate_template('page-builder/components/button.php')); ?>
  </div>
</section>
