<?php
$color = get_sub_field('color');
$alignment = get_sub_field('text_alignment');
$visibility = get_sub_field('visibility');

  $classes = '';

  if ($alignment == 'center') {
    $classes .= ' Panel--center';
  }

  switch (get_sub_field('padding')) {
    case 'none':
      $classes .= '';
    break;
    case 'padding':
      $classes .= ' Panel--soft';
    break;
    case 'double':
      $classes .= ' Panel--soft-double';
    break;
  }

  if ($color != 'none') {
    $classes .= ' Panel--' . $color;
  }

  for ($i = 0; $i < count($visibility); $i++) {
    if ($visibility[$i] != 'show') {
      $classes .= ' ' . $visibility[$i];
    }
  }

?>
<section class="Panel<?php echo $classes ?>">
 <div class="Panel__container">
    <?php the_sub_field('copy'); ?>
    <?php include(locate_template('page-builder/components/button.php')); ?>
 </div>
</section>
