
<?php

  $section_one_classes = '';
  $section_two_classes = '';

  $section_two_background_image = '';
  $section_one_background_image = '';



  if (get_sub_field('section_one')['background'] == 'image'):
    $section_one_background_image = 'style="background-image: url(\'' .  get_sub_field('section_one')['image']['url'] . '\');"';
    $section_one_classes .= ' Panel__section--image';
  else:
    if (get_field('section_one')['color'] != 'dark') {
      $section_one_classes = ' Panel__section--'.get_sub_field('section_one')['color'];
    }
  endif;


  if (get_sub_field('section_two')['background'] == 'image'):
    $section_two_background_image = 'style="background-image: url(\'' .  get_sub_field('section_two')['image']['url'] . '\');"';
    $section_two_classes .= ' Panel__section--image';
  else:
    if (get_field('section_two')['color'] != 'dark') {
      $section_two_classes = ' Panel__section--'.get_sub_field('section_two')['color'];
    }
  endif;
 ?>

<section class="Panel Panel--split">
  <div class="Panel__section <?php echo $section_one_classes; ?>" <?php echo $section_one_background_image; ?>>
    <div class="Panel__container">
      <?php echo get_sub_field('section_one')['copy']; ?>
      <?php
        $btnClasses = '';
        if (get_sub_field('section_one')['show_button'] == true):
          $link = get_sub_field('section_one')['url'];
          $new_tab = (get_sub_field('section_one')['type'] == 'external' ? 'target="_blank"' : '');
          $btnClasses .= (get_sub_field('section_one')['pullout'] ? ' open-pullout ' : '');
          $btnClasses .= get_sub_field('section_one')['style'];
        ?>
        <a href="<?php echo $link ?>" class="Button <?php echo $btnClasses ?>" <?php echo $new_tab; ?>>
          <?php echo  get_sub_field('section_one')['text'] ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
  <div class="Panel__section <?php echo $section_two_classes; ?>" <?php echo $section_two_background_image; ?>>
    <div class="Panel__container">
      <?php echo get_sub_field('section_two')['copy']; ?>
      <?php
        if (get_sub_field('section_two')['show_button'] == true):
          $btnClasses = '';
          $link = get_sub_field('section_two')['url'];
          $new_tab = (get_sub_field('section_two')['type'] == 'external' ? 'target="_blank"' : '');
          $btnClasses .= (get_sub_field('section_two')['pullout'] ? ' open-pullout ' : '');
          $btnClasses .= get_sub_field('section_two')['style'];
        ?>
        <a href="<?php echo $link ?>" class="Button <?php echo $btnClasses ?>" <?php echo $new_tab; ?>>
          <?php echo get_sub_field('section_two')['text'] ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</section>
