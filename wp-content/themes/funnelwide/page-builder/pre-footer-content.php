<?php
  if (get_field('enable_pre_footer') == true ):
    // check if the flexible content field has rows of data
    if( have_rows('footer_content') ):

         // loop through the rows of data
        while ( have_rows('footer_content') ) : the_row();

            if( get_row_layout() == 'copy_panel' ):
              include(locate_template('page-builder/components/copy-panel.php', false, false));

            elseif( get_row_layout() == 'split_panel' ):
              include(locate_template('page-builder/components/split-panel.php', false, false));

            elseif( get_row_layout() == 'cta_panel' ):
              include(locate_template('page-builder/components/cta-panel.php', false, false));

            endif;

        endwhile;

    else :

        // no layouts found

    endif;
  endif;
?>
