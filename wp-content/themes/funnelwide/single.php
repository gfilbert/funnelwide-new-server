<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package funnelwide
 */

get_header('generic'); ?>

	<section class="Article Article--single-column">
		<main id="main" class="Article__content" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

		endwhile; // End of the loop.
		?>
		<?php if ( is_active_sidebar( 'below-article' ) ) : ?>
		<?php dynamic_sidebar( 'below-article' ); ?>
		<?php endif; ?>
		<section class="Panel Panel--center Panel--soft-double Panel--white return-home">
			<div class="Panel__container">
			  <a href="/blog" class="Button Button--block">Return to Home</a>
			</div>
		</section>
		</main><!-- #main -->
	<aside class="post-sidebar">
		<?php if ( is_active_sidebar( 'right-rail-sidebar' ) ) : ?>
		<?php dynamic_sidebar( 'right-rail-sidebar' ); ?>
		<?php endif; ?>
	</aside>

	</section><!-- #primary -->



<?php
get_footer('alt');
