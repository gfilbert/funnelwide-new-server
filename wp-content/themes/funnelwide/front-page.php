<?php

get_header(); ?>


<?php

$custom_query_args['page'] = get_query_var( 'page' )
  ? get_query_var( 'page' )
  : 1;

  $query = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'post',
    'posts_per_page' => 5,
  ));

  $query_2 = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'post',
    'posts_per_page' => 5,
	'offset' => 5,
  )); ?>

<h1 style="display:none;">Trending News that's Fun to Read from funnelwide</h1>
  <section class="Blog-posts">
    <main id="main" class="Blog-posts__list" role="main">
	<h2>Latest News and Updates</h2>
      <?php while ($query->have_posts()): $query->the_post(); ?>
        <article class="Blog-posts__item Card" href="<?php the_permalink() ?>">
          <div class="Card__image">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="Card__content">
            <h3 class="card-post-title"><?php the_title(); ?></h3>
            <p class="card-post-date"><?php the_date(); ?></p>
			<p class="card-post-excerpt"><?php the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="Button Button--ghost">Read Article</a>
          </div>
        </article>
      <?php endwhile; ?>
		
      <?php $pagination =  paginate_links( array('total' => $query->max_num_pages, 'type' => 'array', 'prev_next' => false)); ?>

     <!-- Hide pagination <ul class="Pagination">
        <li class="Pagination__prev"><?php echo get_previous_posts_link('Previous'); ?></li>

        <li class="Pagination__pages">
          <?php foreach ($pagination as $item): ?>
            <?php echo $item; ?>
          <?php endforeach; ?>
        </li
        >
        <li class="Pagination__prev"><?php echo next_posts_link( 'Next' , $query->max_num_pages ); ?></li>
      </ul>-->
    </main><!-- #main -->
    <aside class="Blog-posts__categories">
      <h2 class="Blog-posts__categories__heading">Categories</h2>
      <ul class="Vertical-menu">
        <?php $terms = get_terms('category') ?>
        <?php foreach ($terms as $term):?>
          <li class="Vertical-menu__item"><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
        <?php endforeach; ?>
      </ul>
		<div class="aside-articles">
		<h2>You May Have Missed...</h2>
      <?php while ($query_2->have_posts()): $query_2->the_post(); ?>
        <article class="Blog-posts__item Card" href="<?php the_permalink() ?>">
          <div class="Card__image">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="Card__content">
            <h3><?php the_title(); ?></h3>
            <p><?php the_date(); ?></p>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="Button Button--ghost">Read Article</a>
          </div>
        </article>
      <?php endwhile; ?>
		</div>
    </aside>
  </section>

<?php
get_footer('alt');
