<?php
get_header(); ?>

<?php
  $query = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'careers',
    'posts_per_page' => -1
  ));

  ?>

  <?php the_content(); ?>

  <?php if ($query->have_posts()): ?>

  <div class="Panel Panel--soft-double">
    <div class="Panel__container">
      <div class="Careers-list">
        <h1 class="Careers-list__headline">Current Listings</h1>
      <?php
      while ($query->have_posts()): $query->the_post(); ?>

      <a class="Careers-list__item" href="<?php the_permalink() ?>">
        <div class="Careers-list__content">
          <p class="Careers-list__pre-title"><?php echo get_field('department'); ?></p>
          <p class="Careers-list__title"><?php the_title(); ?></p>
          <p class="Careers-list__post-title"><?php echo get_field('location'); ?></p>
        </div>
      </a>

    <?php
      endwhile;
      ?>
      </div>
    </div>
  </div>

<?php else: ?>

  <?php include(locate_template('page-builder/content-builder.php', false, false)); ?>

<?php endif; ?>

  <?php

  wp_reset_postdata();

get_footer();
