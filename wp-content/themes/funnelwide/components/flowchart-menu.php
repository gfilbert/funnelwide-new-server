<div class="Flowchart-sidebar">
  <div class="Flowchart-menu">

    <div class="Flowchart-menu__item">
      <a href="#masthead" class="Flowchart-menu__dot"><p class="Flowchart-menu__title"></p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="funnels" class="Flowchart-menu__item">
      <a href="#funnels" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Funnels</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="adGroups" class="Flowchart-menu__item">
      <a href="#adGroups" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Ad Groups</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="midFunnel" class="Flowchart-menu__item">
      <a href="#midFunnel" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Mid-Funnel</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="conversion"  class="Flowchart-menu__item">
      <a href="#conversion" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Conversion</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="retargeting" class="Flowchart-menu__item">
      <a href="#retargeting" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Retargeting</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="testing" class="Flowchart-menu__item">
      <a href="#testing" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Testing</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="predictions" class="Flowchart-menu__item">
      <a href="#predictions" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Predictions</p></a>
      <div class="Flowchart-menu__path">
        <div class="Flowchart-menu__line Flowchart-menu__line--draw"></div>
      </div>
    </div>

    <div data-section="optimization" class="Flowchart-menu__item">
      <a href="#optimization" class="Flowchart-menu__dot"><p class="Flowchart-menu__title">Optimization</p></a>
    </div>

  </div>
</div>
