<div class="Flowchart">
  <!-- Funnels -->
  <div id="funnels" class="Flowchart-menu__anchor">
    <div class="Flowchart__item Flowchart__item--no-animation">
      <div class="Flowchart__icon-circle">
        <?php get_template_part('img/svg/icon', 'our-platform-funnel-creative.svg'); ?>
      </div>
      <h1 class="Flowchart__title">Building the Funnels</h1>
      <div class="Flowchart__copy">
        <?php the_field('building_funnels_content'); ?>
      </div>
    </div>
  </div>

  <!-- adGroups -->
  <div id="adGroups" class="Flowchart-menu__anchor">
    <div class="Flowchart__item Flowchart__item--no-animation Flowchart__item--small-gap">
      <div class="Flowchart__line">
        <svg version="1.1" class="animated-lines lines-grey " xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="17.8px" height="69.9px" viewBox="0 0 17.8 69.9" style="enable-background:new 0 0 17.8 69.9;" xml:space="preserve">
          <line class="first-line" x1="8.9" y1="2" x2="8.9" y2="67"/>
          <polyline class="arrow-heads" points="2,61 8.9,67.9 15.8,61 "/>
        </svg>
      </div>
      <h1 class="Flowchart__title">Creating Native Ad Groups</h1>
      <div class="Flowchart__copy">
        <?php the_field('ad_groups_content'); ?>
      </div>
    </div>

    <div class="Flowchart__item Flowchart__item--small-gap Flowchart__item--no-animation">
      <div class="Flowchart__line Flowchart__line--small-screen">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="213.8px" height="68px" viewBox="0 0 213.8 68" enable-background="new 0 0 213.8 68" xml:space="preserve">
          <g>
            <polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
            198,59.1 204.9,66 211.8,59.1 	"/>
            <path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
            M106.9,2v42.8h88c5.5,0,10,4.5,10,10V66"/>

            <polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
            2,59.1 8.9,66 15.8,59.1 	"/>
            <path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
            M106.9,2v42.8h-88c-5.5,0-10,4.5-10,10V66"/>
          </g>
        </svg>
      </div>
      <div class="Flowchart__line Flowchart__line--large-screen">
        <svg version="1.1" class="animated-lines lines-grey" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="565.8px" height="104.9px" viewBox="0 0 565.8 104.9" style="enable-background:new 0 0 565.8 104.9;" xml:space="preserve">
        <path class="tri-line-left-right" d="M282.9,2v60h264c5.5,0,10,4.5,10,10v30.9"/>
        <polyline class="arrow-heads" points="2,96 8.9,102.9 15.8,96 "/>
        <polyline class="arrow-heads" points="276,96 282.9,102.9 289.8,96 "/>
        <polyline class="arrow-heads" points="550,96 556.9,102.9 563.8,96 "/>
        <path class="tri-line-left-right" d="M282.9,2v60H19c-5.5,0-10,4.5-10,10v30"/>
        <line class="tri-line-center" x1="282.9" y1="2" x2="282.9" y2="102"/>
        </svg>
      </div>

      <div class="Flowchart__grid">
        <div class="Flowchart__grid-item">
          <div class="Flowchart__box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/funnelwide-ourplatform-adgroups-asparagus.jpg">
          </div>
          <p><strong>Cook Gourmet Meals That Will Blow Your Family Away</strong></p>
        </div>
        <div class="Flowchart__grid-item">
          <div class="Flowchart__box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/funnelwide-ourplatform-adgroups-meatandwine.jpg">
          </div>
          <p><strong>Easy Gourmet Meals Delivered Right To You</strong></p>
        </div>
        <div class="Flowchart__grid-item">
          <div class="Flowchart__box">
            <img src="<?php echo get_template_directory_uri(); ?>/img/funnelwide-ourplatform-adgroups-dinnertable.jpg">
          </div>
          <p><strong>How This Startup is Disrupting Gourmet Food</strong></p>
        </div>
      </div>
    </div>
  </div>


   <div  class="Flowchart__item Flowchart__item--no-animation Flowchart__item--small-gap">
     <div class="Flowchart__line Flowchart__line--large-screen">
       <!-- Tri Line (3) -->
       <svg version="1.1" class="animated-lines lines-grey" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="565.8px" height="104.9px" viewBox="0 0 565.8 104.9" style="enable-background:new 0 0 565.8 104.9;" xml:space="preserve"
       >
         <path class="tri-line-left-right" d="M8.7,2.9v30c0,5.5,4.5,10,10,10h264.2v60"/>
         <polyline class="arrow-heads" points="276,96 282.9,102.9 289.8,96 "/>
         <path class="tri-line-left-right" d="M556.8,2.9v30c0,5.5-4.5,10-10,10H282.9v60"/>
         <line class="tri-line-center" x1="282.9" y1="2.9" x2="282.9" y2="102.9"/>
       </svg>
     </div>

     <div class="Flowchart__line Flowchart__line--small-screen">
       <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="213.8px" height="68px" viewBox="0 0 213.8 68" enable-background="new 0 0 213.8 68" xml:space="preserve">
       <g>

          <polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
          100,59.1 106.9,66 113.8,59.1 	"/>
        <path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
          M106.9,66V23.2h-88c-5.5,0-10-4.5-10-10V2"/>
        <path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
          M106.9,66V23.2h88c5.5,0,10-4.5,10-10V2"/>
       </g>
       </svg>
     </div>
    <!-- MidFunnel -->
    <div id="midFunnel" class="Flowchart-menu__anchor">
      <h1 class="Flowchart__title">Integrating Sponsored Content</h1>
      <div class="Flowchart__copy">
        <?php the_field('sponsored_content'); ?>
      </div>
    </div>
   </div>

   <div class="Flowchart__item Flowchart__item--no-animation Flowchart__item--small-gap">
     <div class="Flowchart__line Flowchart__line--small-gap Flowchart__line--large-screen">
       <svg version="1.1" class="animated-lines lines-orange" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="565.8px" height="139.9px" viewBox="0 0 565.8 139.9" style="enable-background:new 0 0 565.8 139.9;" xml:space="preserve"
       >
         <polyline class="arrow-heads" points="2,96 8.9,102.9 15.8,96 "/>
         <polyline class="arrow-heads" points="276,97 282.9,103.9 289.8,97 "/>
         <polyline class="arrow-heads" points="550,131 556.9,137.9 560.8,134 563.8,131 "/>
         <path class="tri-line-left-right" d="M282.9,2v60H19c-5.5,0-10,4.5-10,10v30"/>
         <path class="tri-line-left-right" d="M282.9,37v25H547c5.5,0,10,4.5,10,10v64"/>
         <line class="tri-line-center" x1="282.9" y1="2" x2="282.9" y2="103"/>
       </svg>
     </div>

    <div class=" Flowchart__line Flowchart__line--small-screen">
     <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
       width="213.8px" height="68px" viewBox="0 0 213.8 68" enable-background="new 0 0 213.8 68" xml:space="preserve">
     <g>
      <polyline fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
        198,59.1 204.9,66 211.8,59.1 	"/>
      <path fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
        M106.9,2v42.8h88c5.5,0,10,4.5,10,10V66"/>

        <polyline fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
        2,59.1 8.9,66 15.8,59.1 	"/>
      <path fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
        M106.9,2v42.8h-88c-5.5,0-10,4.5-10,10V66"/>
     </g>
     </svg>
   </div>
      <div class="Flowchart__grid Flowchart__grid--alt">
        <div class="Flowchart__grid-item">
          <p class="Flowchart_bold">Sponsored Content</p>
          <div class="Flowchart__box Flowchart__box--soft">
            <img src="<?php echo get_template_directory_uri(); ?>/img/funnelwide-ourplatform-sponsoredcontent-asparagus.png">

          </div>
        </div>
        <div class="Flowchart__grid-item">
          <p class="Flowchart_bold">Sponsored Content</p>
          <div class="Flowchart__box Flowchart__box--soft">
            <img src="<?php echo get_template_directory_uri(); ?>/img/funnelwide-ourplatform-sponsoredcontent-mealwine.png">
          </div>

        </div>
        <div class="Flowchart__grid-item Flowchart__grid-item--centered">
          <p class="Flowchart__box-title">Skipping Mid-Funnel</p>
          <p class="Flowchart__text-small">Most funnels also test a path that takes consumers directly from the native ad to your home page or shopping cart.</p>
        </div>
      </div>
    </div>

  </div>
  <div id="conversion" class="Flowchart-menu__anchor">
    <div class="Flowchart__item Flowchart__item--no-animation Flowchart__item--small-gap">
      <div class=" Flowchart__line Flowchart__line--small-screen">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           width="213.8px" height="68px" viewBox="0 0 213.8 68" enable-background="new 0 0 213.8 68" xml:space="preserve">
          <g>
              <polyline fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
              100,59.1 106.9,66 113.8,59.1 	"/>
            <path fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
              M106.9,66V23.2h-88c-5.5,0-10-4.5-10-10V2"/>
            <path fill="none" stroke="#FF8403" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
              M106.9,66V23.2h88c5.5,0,10-4.5,10-10V2"/>
          </g>
        </svg>

      </div>
      <div class="Flowchart__line Flowchart__line--large-screen">
        <svg version="1.1" class="animated-lines lines-orange" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          width="552.2px" height="138px" viewBox="0 0 552.2 138" style="enable-background:new 0 0 552.2 138;" xml:space="preserve">
         <polyline class="arrow-heads" points="269.2,129.1 276.1,136 283,129.1 "/>
         <path class="tri-line-left-right" d="M550,2v63.8c0,5.6-4.5,10.2-10.2,10.2H276.1v23.5"/>
         <path class="tri-line-left-right" d="M2,36v30c0,5.5,4.5,10,10,10h264.1v57.3"/>
         <path class="tri-line-center" d="M276.1,35v98.3"/>
       </svg>
     </div>

     <h1 class="Flowchart__title">Managing Conversion Click-Paths</h1>
     <div class="Flowchart__copy">
       <?php the_field('managing_conversions_content'); ?>
     </div>
   </div>

   <div class="Flowchart__item Flowchart__item--no-animation Flowchart__item--small-gap">
     <!-- Dual Line (6) -->
      <div class="Flowchart__line Flowchart__line--large-screen Flowchart__line--small-gap">
       <svg version="1.1" class="animated-lines lines-yellow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="417.8px" height="94.9px" viewBox="0 0 417.8 94.9" style="enable-background:new 0 0 417.8 94.9;" xml:space="preserve">
         <polyline class="arrow-heads" points="402.1,86.1 409,93 415.9,86.1 "/>
         <polyline class="tri-line-center" points="15.9,86.1 9,93 2.1,86.1 "/>
         <path class="dual-line" d="M208.9,4v49H19c-5.5,0-10,4.5-10,10v30"/>
         <path class="dual-line" d="M208.9,4v49h189.9c5.5,0,10,4.5,10,10v30"/>
       </svg>
     </div>

    <div class="Flowchart__grid">
      <div class="Flowchart__line Flowchart__line--small-screen Flowchart__line--offset Flowchart__line--small-gap">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           width="220px" height="420px" viewBox="0 0 192 394.1" enable-background="new 0 0 192 394.1" xml:space="preserve">
        <g>
            <line fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="56.2" y1="2" x2="56.2" y2="42"/>
          <path fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
            M9,71l0-19c0-5.5,4.5-10,10-10h161c5.5,0,10,4.5,10,10v6.3V326c0,5.5-4.5,10-10,10H61c-5.5,0-10,4.5-10,10v46.1"/>

            <polyline fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
            15.8,68 8.9,74.9 2,68 	"/>

            <polyline fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
            57.9,385.3 51,392.1 44.1,385.3 	"/>
        </g>
      </svg>
      </div>

     <div class="Flowchart__grid-left">
       <div class="Flowchart__text-box">
         <div class="Flowchart__icon">
           <?php get_template_part('img/svg/icon', 'coversion.svg'); ?>
         </div>

         <p class="Flowchart__box-title">Direct To Conversion</p>
         <p class="Flowchart__text-small">Whether its your store’s shopping cart or an email newsletter signup, this path drives users directly to your predetermined point of conversion.</p>
       </div>
     </div>

    <div class="Flowchart__grid-right">
      <div class="Flowchart__text-box Flowchart__text-box--right">
        <p class="Flowchart__box-title">Or Drive To Your Homepage</p>
        <p class="Flowchart__text-small">Depending on your site’s design the funnel can determine the best pages to send consumers and help you improve these pages to lift conversion rates.</p>
      </div>

      <div class="Flowchart__line Flowchart__line--small-gap">
        <svg version="1.1" class="animated-lines lines-yellow last-line-offset" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="17.8px" height="34.9px" viewBox="0 0 17.8 34.9" style="enable-background:new 0 0 17.8 34.9;" xml:space="preserve">
          <line class="last-line" x1="8.9" y1="2" x2="8.9" y2="32"/>
          <polyline class="arrow-heads" points="2,26 8.9,32.9 15.8,26 "/>
        </svg>
      </div>

        <div class="Flowchart__text-box Flowchart__text-box--right">
          <div class="Flowchart__icon">
            <?php get_template_part('img/svg/icon', 'coversion.svg'); ?>
          </div>
          <p class="Flowchart__box-title">Generate Conversions</p>
          <p class="Flowchart__text-small">The system optimizes automatically to drive the highest possible amount of quality conversions at the lowest possible cost.</p>
        </div>
      </div>
    </div>
   </div>
  </div>
</div>
