<?php
  $size = get_field('masthead_size');
  $classes = "Masthead Masthead--{$size} ";
  $disabled = get_field('masthead_disable');
  $button_width = get_field('button_fixed_width');

if ($disabled == false): ?>
  <section class="<?php echo $classes; ?>">
    <div class="Masthead__background" style="background-image: url('<?php echo get_field('masthead_image'); ?>')"></div>
    <div class="Masthead__background Masthead__background--mobile" style="background-image: url('<?php echo get_field('background_image_mobile'); ?>')"></div>

    <div class="Masthead__inner">
    <?php if (get_field('masthead_accents') == 'variation_one'): ?>
    <div class="Masthead__accent-top-right">
      <?php get_template_part('img/svg/Homepage', 'Masthead-Accent-Graphic-Right.svg'); ?>
    </div>
  <?php endif; ?>

  <?php if (get_field('masthead_accents') == 'variation_two'): ?>
  <div class="Masthead__accent-two">
    <?php get_template_part('img/svg/ManagedServices', 'Masthead-Accent-Graphic-Center.svg'); ?>
  </div>
<?php endif; ?>

<?php if (get_field('masthead_accents') == 'variation_three'): ?>
<div class="Masthead__accent-three">
  <?php get_template_part('img/svg/OurApproach', 'Masthead-Accent-Graphic-Center.svg'); ?>
</div>
<?php endif; ?>

      <div class="Masthead__content">
        <h1 class="Masthead__title"><?php the_field('masthead_title') ?></h1>
        <div class="Masthead__copy">
          <?php the_field('masthead_copy') ?>
        </div>
        <?php if (have_rows('masthead_buttons')): ?>
          <div class="Masthead__button-group <?php echo ($button_width == true ? ' Masthead__button-group--fixed ' : '') ?>">
            <?php  while ( have_rows('masthead_buttons') ) : the_row();
            include(locate_template('page-builder/components/button.php')); ?>

            <?php endwhile; ?>
          </div>
        <?php endif ?>
      </div>
      <?php if (get_field('masthead_accents') == 'variation_one'): ?>
        <div class="Masthead__accent-bottom-left">
          <?php get_template_part('img/svg/Homepage', 'Masthead-Accent-Graphic-Left.svg'); ?>
        </div>
      <?php endif; ?>

    </div>
  </section>
<?php endif ?>
