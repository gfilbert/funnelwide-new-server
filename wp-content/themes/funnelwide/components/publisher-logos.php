<?php
  $classes = '';
  if ( get_field('background_color') ) {
    $background_color = get_field('background_color');
    $classes .= ' ';
    $classes .= 'Panel--' . $background_color['color'];
  }

  switch (get_sub_field('padding')) {
    case 'none':
      $classes .= '';
    break;
    case 'padding':
      $classes .= ' ';
      $classes .= 'Panel--soft';
    break;
    case 'double':
      $classes .= ' ';
      $classes .= 'Panel--soft-double';
    break;
  }

  $asset_width = 'Asset-grid';
  switch (get_field('asset_width')) {
    case 'normal':
      $asset_width .= '';
    break;
    case 'wide':
      $asset_width .= '--wide';
    break;
  }
?>

<?php if( have_rows('company') ): ?>
<div class="Panel Panel--center<?php echo $classes; ?>">
  <div class="Panel__container">
    <?php if ( get_field('asset_accents') ) { ?>
      <div class="Accent Accent--left">
        <?php get_template_part('img/svg/accent', 'graphic-left-one.svg'); ?>
      </div>
    <?php } ?>
    <div class="<?php echo $asset_width; ?>">
      <?php if ( get_field('asset_heading') ) { ?>
        <div class="Asset-grid__item">
          <p class="text-center"><?php the_field('asset_heading'); ?></p>
        </div>
      <?php }
      while( have_rows('company') ): the_row();  ?>
        <div class="Asset-grid__item">
        <img src=" <?php echo get_sub_field('logo')['url']; ?>" alt="">
        </div>
      <?php endwhile;
      if ( the_field('asset_footer') ) {
        the_field('asset_footer');
      } ?>
      <?php if ( get_field('asset_accents') ) { ?>
        <div class="Accent Accent--right">
          <?php get_template_part('img/svg/accent', 'graphic-right-one.svg'); ?>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php endif; ?>
