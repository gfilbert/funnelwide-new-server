<div class="Pullout">
  <div class="Pullout__inner">
    <div id="menuDrawer" class="Pullout__panel Pullout__panel--nav">
      <div class="Pullout__ctrl">
        <a href="#" class="Pullout__close">
          <svg class="icon icon-close"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#close"></use></svg>
        </a>
      </div>
      <div class="Pullout__content">
        <nav class="Pullout__menu">
          <?php wp_nav_menu( array( 'theme_location' => 'mobile', 'container' => false, 'menu_class' => 'Mobile-menu', 'walker' => new funnelwide_walker_mobile_menu) ); ?>
        </nav>
        <?php if(get_field('dashboard_login', 'option')) { ?>
        <!--<div class="Pullout__dashboard">
           <a href="<?php echo get_field('dashboard_login', 'option') ?>" target="_blank"><svg class="icon icon-user"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#user"></use></svg> Dashboard Login</a>
        </div>-->
        <?php } // END if(get_field('dashboard_login', 'option')) ?>
      </div>
    </div>

    <!--<div id="requestDemo" class="Pullout__panel Pullout__panel--demo" style="background-image: url('<?php echo get_field('demo_background', 'option') ?>')">-->
    <div id="requestDemo" class="Pullout__panel Pullout__panel--demo" style="background-image: url('<?php echo get_field('demo_background', 'option') ?>')">
      <div class="Pullout__ctrl">
        <a href="#menuDrawer" class="Pullout__back open-pullout">Back</a>
        <a href="#" class="Pullout__close">
          <svg class="icon icon-close"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#close"></use></svg>
        </a>
      </div>
      <div class="Pullout__content">
        <?php
        if ( is_active_sidebar( 'pullout' ) ) : 
          dynamic_sidebar('pullout'); 
        endif;
        ?>
      </div>
    </div>
  </div>
</div>
