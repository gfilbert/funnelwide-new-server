<?php

$pre_content_color = get_field('pre_content_color');
$pre_content_copy =  get_field('pre_content_copy');

$post_content_color = get_field('post_content_color');
$post_content_copy = get_field('post_content_copy');

?>

<?php if($pre_content_copy) { ?>
<section class="Panel Panel--soft-double Panel--<?php echo $pre_content_color; ?> Panel--center">
  <div class="Panel__container ">
    <?php echo $pre_content_copy ?>

    <?php
    if (get_field('pre_content_show_button') == true):
      $btnClasses = '';
      $link =  get_field('pre_content_url');
      $new_tab = (get_field('pre_content_type') == 'external' ? 'target="_blank"' : '');
      $btnClasses .= (get_field('pre_content_pullout') ? ' open-pullout ' : '');
      $btnClasses .= get_field('pre_content_style');
      ?>
      <a href="<?php echo $link ?>" class="Button <?php echo $btnClasses ?>" <?php echo $new_tab; ?>>
        <?php echo get_field('pre_content_text') ?>
      </a>
    <?php endif; ?>

  </div>
</section>
<?php } // End if($pre_content_copy) ?>

<?php if( have_rows('step') ): ?>

<section class="Timeline">
  <div class="Timeline__container">
    <div class="Timeline__dot Timeline__dot--first"></div>


<?php $count = 0; while( have_rows('step') ): the_row();
    $count++;

    // vars
		$title = get_sub_field('title');
    $image = get_sub_field('image');
		$headline = get_sub_field('headline');
		$copy = get_sub_field('copy');
    $step_odd = ($count % 2 == 1 ? '' : 'Timeline__step--flipped');
    $step_first = ($count == 1 ? 'Timeline__path--short' : '');
	?>
    <article class="Timeline__step <?php echo $step_odd; ?>">
      <div class="Timeline__path <?php echo $step_first; ?>">
        <div class="Timeline__line"></div>
      </div>
      <div class="Timeline__dot Timeline__dot--hollow"></div>
      <div class="Timeline__title">
        <?php echo $title; ?>
        <?php if( !empty($image) ): ?>
          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="Timeline__content">
        <div class="Timeline__headline"><?php echo $headline; ?></div>
        <div class="Timeline__copy">
          <?php echo $copy; ?>
        </div>
      </div>
    </article>
  <?php endwhile; ?>
    <!-- end  -->
    <div class="Timeline__path">
      <div class="Timeline__line"></div>
    </div>
    <div class="Timeline__dot"></div>
  </div>
</section>

<?php endif; ?>

<?php if($post_content_copy) { ?>
<Section class="Panel Panel--<?php echo $post_content_color; ?> Panel--soft-double Panel--center">
 <div class="Panel__container">
   <?php echo $post_content_copy ?>
   <?php
     if (get_field('post_content_show_button') == true):
       $btnClasses = '';
       $link = get_field('post_content_url');
       $new_tab = (get_field('post_content_type') == 'external' ? 'target="_blank"' : '');
       $btnClasses .= (get_field('post_content_pullout') ? ' open-pullout ' : '');
       $btnClasses .= get_field('post_content_style');
       ?>
       <a href="<?php echo $link ?>" class="Button <?php echo $btnClasses ?>" <?php echo $new_tab; ?>>
         <?php echo get_field('post_content_text') ?>
       </a>
   <?php endif; ?>
 </div>
</Section>
<?php } // End if ($post_content_copy) ?>