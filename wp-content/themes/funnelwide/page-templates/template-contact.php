<?php /* Template Name: Contact Page Template */

get_header(); ?>

  <main id="main" role="main">

    <?php the_content(); ?>

  </main><!-- #main -->

<?php get_footer();
