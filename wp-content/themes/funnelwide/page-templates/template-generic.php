<?php /* Template Name: Generic Template */

get_header('generic'); ?>

<section class="Article Article--single-column">
  <main id="main" class="Article__content" role="main">

  <?php
  while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/content', 'page' );

  endwhile; // End of the loop.
  ?>

  </main><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
