<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package funnelwide
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <section class="Career-single">
      <article class="Career-single__content">
        <?php
        while ( have_posts() ) : the_post();
        the_content();
        ?>
        <hr>
        <a href="<?php echo get_permalink( get_page_by_path( 'careers' ) ) ?>">Back to Careers</a>

        <?php
          endwhile; // End of the loop.
        ?>

      </article>
      <aside class="Career-single__apply">
        <h3>Apply Today</h3>
        <p>Submit your name and resume and we’ll get back to you promptly. Ensure that your first name, last name, and date of submission are in the file name of your resume.</p>
        <form class="Form" action="" method="post">
          <div class="Form__field">
            <input type="text" name="" value="" class="Form__input Form__input--concrete" placeholder="Name*">
          </div>

          <div class="Form__field ">
            <input type="text" name="" value="" class="Form__input Form__input--concrete" placeholder="Email*">
          </div>

          <div class="Form__field ">
            <input type="text" name="" value="" class="Form__input Form__input--concrete" placeholder="Phone*">
          </div>

          <div class="Form__field ">
            <input type="text" name="" value="" class="Form__input Form__input--concrete" placeholder="CV*">
          </div>
          <div class="text-center">
            <button class="Button Button--block">Submit</button>
          </div>
        </form>
      </aside>
    </section>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer('alt');
