<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package funnelwide
 */

?>

	</div><!-- #content -->

  <?php include(locate_template('page-builder/pre-footer-content.php', false, false)); ?>

  <footer class="Footer Footer--alt">
    <div class="Footer__logo">
      <a href="/">
        <?php get_template_part('img/header', 'logo.svg'); ?>
      </a>
    </div>
    <div class="Footer__links">
      <ul class="Footer__social-icons">
        <li class="Footer__social-icons__fb"><a href="#"><svg class="icon icon-facebook"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#facebook"></use></svg></a></li>
        <li class="Footer__social-icons__twitter"><a href="#"><svg class="icon icon-facebook"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#twitter"></use></svg></a></li>
        <li class="Footer__social-icons__linkedin"><a href="#"><svg class="icon icon-facebook"><use xlink:href="/wp-content/themes/funnelwide/dist/symbol-defs.svg#linkedin"></use></svg></a></li>
      </ul>

        <?php wp_nav_menu( array( 'theme_location' => 'footer', 'container' => false, 'menu_class' => 'Footer__link-list', 'walker' => new funnelwide_walker_footer_menu) ); ?>

    </div>
    <div class="Footer__secondary-links">
      <div><?php wp_nav_menu( array( 'theme_location' => 'sub_footer','items_wrap' => '%3$s', 'container' => false, 'walker' => new funnelwide_walker_sub_footer_menu) ); ?></div>
      <?php the_field('copyright', 'option') ?>
    </div>
  </footer>
</div><!-- #page -->
<div class="Pullout__cover-content"></div>
<?php wp_footer(); ?>
<script type='text/javascript'>
(function (d, t) {
  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
  bh.type = 'text/javascript';
  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=ubw4vxylawc7xzu6eonjfw';
  s.parentNode.insertBefore(bh, s);
  })(document, 'script');
</script>
</body>
</html>
