<?php
$query = new WP_Query(array(
  'post_status' => 'publish',
  'post_type' => 'case-studies',
  'posts_per_page' => 2
));

if($query->have_posts()) {
?>

  <section class="Panel Panel--soft-double Panel--center">
    <div class="Panel__container">
      <h1>Case Studies</h1>
      <div class="Grid Grid--inset Grid--two">

<?php

  while ($query->have_posts()): $query->the_post();
    $image = get_field('image');
   ?>

  <div class="Grid__item">
    <a href="<?php echo get_permalink(); ?>" class="Card">
      <div class="Card__pre">
        <img src="<?php echo get_field('brand_logo'); ?>" alt="">
      </div>
      <div class="Card__image">
        <img src="<?php echo $image['sizes']['case_study_card']; ?>" alt="">
      </div>
      <div class="Card__content">
        <p class="Card__title"><?php the_field('headline'); ?></p>
      </div>
    </a>
  </div>


<?php
  endwhile;
  wp_reset_postdata();
  ?>

        </div>
    </div>
  </section>
  <?php } ?>
