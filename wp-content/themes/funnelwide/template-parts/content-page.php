<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package funnelwide
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php the_content(); ?>

	<?php include(locate_template('page-builder/content-builder.php', false, false)); ?>

</article><!-- #post-## -->
