  <section class="Panel Panel--cta" style="background-image: url('wp-content/uploads/2017/01/image_six.jpg')">
    <div class="Panel__container">
      <h1>Get Started</h1>
      <p>Fill out the form below and a Funnelwide professional will contact you promptly.</p>
      <?php
      if ( is_active_sidebar( 'contact' ) ) : 
        dynamic_sidebar('contact'); 
      endif;
      ?>
    </div>
  </section>
