<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package funnelwide
 */

?>

<section class="Panel Panel--soft-double">
  <div class="Panel__container">
    <article id="post-<?php the_ID(); ?>" <?php post_class('Blog-single'); ?>>

    <div class="Blog-single__information">
      <div class="Blog-single__featured-image">
        <?php the_post_thumbnail(); ?>
      </div>

      <?php the_title( '<h1 class="beta Blog-single__title">', '</h1>' ); ?>
    </div>

      <?php the_content(); ?>
		<?php 
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
				'after'  => '</div>',
			)
		);
		?>
    </article><!-- #post-## -->
  </div>
</section>
