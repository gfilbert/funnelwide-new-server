<?php

get_header(); ?>


<?php

$custom_query_args['page'] = get_query_var( 'page' )
  ? get_query_var( 'page' )
  : 1;

  $query = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'post',
    'posts_per_page' => 1,
  )); ?>

  <section class="Blog-posts">
    <main id="main" class="Blog-posts__list" role="main">
      <?php while ($query->have_posts()): $query->the_post(); ?>
        <article class="Blog-posts__item Card" href="<?php the_permalink() ?>">
          <div class="Card__image">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="Card__content">
            <h3><?php the_title(); ?></h3>
            <p>By <?php the_author(); ?> on <?php the_date(); ?></p>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="Button Button--ghost">Read Article</a>
          </div>
        </article>
      <?php endwhile; ?>

      <?php $pagination =  paginate_links( array('total' => $query->max_num_pages, 'type' => 'array', 'prev_next' => false)); ?>

      <ul class="Pagination">
        <li class="Pagination__prev"><?php echo get_previous_posts_link('Previous'); ?></li>

        <li class="Pagination__pages">
          <?php foreach ($pagination as $item): ?>
            <?php echo $item; ?>
          <?php endforeach; ?>
        </li
        >
        <li class="Pagination__prev"><?php echo next_posts_link( 'Next' , $query->max_num_pages ); ?></li>
      </ul>
    </main><!-- #main -->
    <aside class="Blog-posts__categories">
      <h3 class="Blog-posts__categories__heading">Categories</h3>
      <ul class="Vertical-menu">
        <?php $terms = get_terms('category') ?>
        <?php foreach ($terms as $term):?>
          <li class="Vertical-menu__item"><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
        <?php endforeach; ?>
      </ul>
    </aside>
  </section>

<?php
get_footer('alt');
