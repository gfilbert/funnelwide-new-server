<?php
get_header(); ?>

<?php
  $query = new WP_Query(array(
    'post_status' => 'publish',
    'post_type' => 'case-studies',
    'posts_per_page' => -1
  )); ?>
  <div class="Case-studies">
    <?php while ($query->have_posts()): $query->the_post();

      $image = get_field('image');

    ?>

    <a href="<?php the_permalink(); ?>"  class="Horizontal-card" href="<?php the_permalink() ?>">
      <div class="Horizontal-card__image" style="background-image: url('<?php echo $image['url']; ?>')"></div>
      <div class="Horizontal-card__content">
        <div class="Horizontal-card__pre"><img src="<?php echo get_field('brand_logo'); ?>" alt=""></div>
        <p class="Horizontal-card__title"><?php echo get_field('headline'); ?></p>
        <p><?php the_field('strapline'); ?></p>
        <p class="Button Button--block">View case study</p>
      </div>
    </a>

  <?php
    endwhile;
  ?>
  </div>
<?php
  wp_reset_postdata();

get_footer();
