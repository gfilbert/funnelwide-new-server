<?php 

// Register the post types on init
add_action('init', 'cpt_case_studies', 0);

// Function to register a post type (the proper way)
function cpt_case_studies() {
  register_post_type('case-studies', array(
    'labels' => array(
      'name' => __('Case Studies'),
      'singular_name' => __('Case Study'),
      'add_new' => _x('Add new', 'Case Study'),
      'add_new_item' => __('Add New Case Study'),
    ),
    'public' => true,
    'has_archive' => false,
    '_builtin' => false,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'supports' => array(
      'title'
    ),
    'rewrite' => array(
      "slug" => 'case-studies'
    ),
    'menu_icon' => 'dashicons-star-filled'
  ));
}