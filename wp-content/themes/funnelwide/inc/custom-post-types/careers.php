<?php

// Register the post types on init
add_action('init', 'cpt_careers', 0);

// Function to register a post type (the proper way)
function cpt_careers() {
  register_post_type('careers', array(
    'labels' => array(
      'name' => __('Careers'),
      'singular_name' => __('Career'),
      'add_new' => _x('Add new', 'Career'),
      'add_new_item' => __('Add New Career'),
    ),
    'public' => true,
    'has_archive' => false,
    '_builtin' => false,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'supports' => array(
      'title', 'editor'
    ),
    'rewrite' => array(
      "slug" => 'careers'
    ),
    'menu_icon' => 'dashicons-star-filled'
  ));
}
