<?php

function acf_load_svg_field_choices($field) {
  // reset choices
    $field['choices'] = array();
    $choices = array_diff(scandir(get_template_directory() . '/img/svg'), array('..', '.', '.DS_Store'));
    // $choices = get_field('my_select_values', 'option', false);

    // explode the value so that each line is a new array piece
  //  $choices = explode("\n", $choices);

   // remove any unwanted white space
  //  $choices = array_map('trim', $choices);

   // loop through array and add to field 'choices'
    if( is_array($choices) ) {
      foreach( $choices as $choice ) {
        $choice = str_replace(array('icon-','.php'), '', $choice);
        $field['choices'][ $choice ] = $choice;
      }
    }

    // return the field
    return $field;
}

add_filter('acf/load_field/name=svg_icon', 'acf_load_svg_field_choices');

 ?>
