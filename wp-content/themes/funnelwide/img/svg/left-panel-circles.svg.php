<svg width="78px" height="423px" viewBox="0 0 78 423" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 42 (36781) - http://www.bohemiancoding.com/sketch -->
    <title>Group 2</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <circle id="path-1" cx="70" cy="143" r="8"></circle>
        <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-1"></use>
        </mask>
        <circle id="path-3" cx="8" cy="413" r="8"></circle>
        <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-3"></use>
        </mask>
    </defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Case-Study-Hyphen" transform="translate(-171.000000, -596.000000)" stroke-width="4">
            <g id="Group-2" transform="translate(171.000000, 598.000000)">
                <circle id="Oval-Copy" stroke="#FFB502" cx="26" cy="10" r="10"></circle>
                <use id="Oval-Copy-6" stroke="#FFD100" mask="url(#mask-2)" fill="#FFFFFF" xlink:href="#path-1"></use>
                <use id="Oval-Copy-7" stroke="#FF8403" mask="url(#mask-4)" fill="#FFFFFF" xlink:href="#path-3"></use>
            </g>
        </g>
    </g>
</svg>
