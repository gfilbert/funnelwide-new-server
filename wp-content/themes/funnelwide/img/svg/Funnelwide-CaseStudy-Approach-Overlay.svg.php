<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="550px" height="524px" viewBox="0 0 550 524" enable-background="new 0 0 550 524" xml:space="preserve">
<g>
	<circle fill="none" stroke="#FFB502" stroke-width="2" stroke-miterlimit="10" cx="512.5" cy="78.5" r="7"/>
	<circle fill="none" stroke="#FFB502" stroke-width="2" stroke-miterlimit="10" cx="39.5" cy="463.3" r="5"/>
	<circle fill="none" stroke="#FF8403" stroke-width="4" stroke-miterlimit="10" cx="480.2" cy="438.3" r="10"/>
	<circle fill="none" stroke="#FFD100" stroke-width="4" stroke-miterlimit="10" cx="44.5" cy="10" r="10"/>
</g>
</svg>
