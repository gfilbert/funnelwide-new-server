<svg width="192px" height="401px" viewBox="0 0 192 401" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 42 (36781) - http://www.bohemiancoding.com/sketch -->
    <title>Group 3</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <circle id="path-1" cx="8" cy="393" r="8"></circle>
        <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-1"></use>
        </mask>
        <circle id="path-3" cx="108" cy="8" r="8"></circle>
        <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-3"></use>
        </mask>
    </defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Case-Study-Hyphen" transform="translate(-1117.000000, -610.000000)" stroke-width="4">
            <g id="Group-3" transform="translate(1117.000000, 610.000000)">
                <use id="Oval-Copy-8" stroke="#FFB502" mask="url(#mask-2)" fill="#FFFFFF" xlink:href="#path-1"></use>
                <use id="Oval-Copy-9" stroke="#FF8403" mask="url(#mask-4)" fill="#FFFFFF" xlink:href="#path-3"></use>
                <circle id="Oval-Copy-10" stroke="#FFB502" cx="180" cy="289" r="10"></circle>
            </g>
        </g>
    </g>
</svg>
