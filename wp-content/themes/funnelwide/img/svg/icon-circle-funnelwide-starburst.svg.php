<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 215 215" enable-background="new 0 0 215 215" xml:space="preserve">
<g>
	<defs>
		<path id="FunnelwideStarburstID_1_" d="M102.5,36c0-2.2,1.8-4,4-4s4,1.8,4,4s-1.8,4-4,4S102.5,38.2,102.5,36"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_2_">
		<use xlink:href="#FunnelwideStarburstID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="97.5" y="27" clip-path="url(#FunnelwideStarburstID_2_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_3_" d="M102.5,20c0-2.2,1.8-4,4-4s4,1.8,4,4s-1.8,4-4,4S102.5,22.2,102.5,20"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_4_">
		<use xlink:href="#FunnelwideStarburstID_3_"  overflow="visible"/>
	</clipPath>
	<rect x="97.5" y="11" clip-path="url(#FunnelwideStarburstID_4_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_5_" d="M102.5,4c0-2.2,1.8-4,4-4s4,1.8,4,4s-1.8,4-4,4S102.5,6.2,102.5,4"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_6_">
		<use xlink:href="#FunnelwideStarburstID_5_"  overflow="visible"/>
	</clipPath>
	<rect x="97.5" y="-5" clip-path="url(#FunnelwideStarburstID_6_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_7_" d="M53.5,60.3c-1.6-1.6-1.6-4.1,0-5.7s4.1-1.6,5.7,0s1.6,4.1,0,5.7C57.6,61.8,55.1,61.8,53.5,60.3"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_8_">
		<use xlink:href="#FunnelwideStarburstID_7_"  overflow="visible"/>
	</clipPath>
	<rect x="47.3" y="48.4" clip-path="url(#FunnelwideStarburstID_8_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_9_" d="M42.2,48.9c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7
			C46.3,50.5,43.8,50.5,42.2,48.9"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_10_">
		<use xlink:href="#FunnelwideStarburstID_9_"  overflow="visible"/>
	</clipPath>
	<rect x="36" y="37.1" clip-path="url(#FunnelwideStarburstID_10_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_11_" d="M30.9,37.6c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7
			C35,39.2,32.4,39.2,30.9,37.6"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_12_">
		<use xlink:href="#FunnelwideStarburstID_11_"  overflow="visible"/>
	</clipPath>
	<rect x="24.7" y="25.8" clip-path="url(#FunnelwideStarburstID_12_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_13_" d="M36,112.1c-2.2,0-4-1.8-4-4s1.8-4,4-4s4,1.8,4,4S38.2,112.1,36,112.1"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_14_">
		<use xlink:href="#FunnelwideStarburstID_13_"  overflow="visible"/>
	</clipPath>
	<rect x="27" y="99.1" clip-path="url(#FunnelwideStarburstID_14_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_15_" d="M20,112.1c-2.2,0-4-1.8-4-4s1.8-4,4-4s4,1.8,4,4S22.2,112.1,20,112.1"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_16_">
		<use xlink:href="#FunnelwideStarburstID_15_"  overflow="visible"/>
	</clipPath>
	<rect x="11" y="99.1" clip-path="url(#FunnelwideStarburstID_16_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_17_" d="M4,112.1c-2.2,0-4-1.8-4-4s1.8-4,4-4s4,1.8,4,4S6.2,112.1,4,112.1"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_18_">
		<use xlink:href="#FunnelwideStarburstID_17_"  overflow="visible"/>
	</clipPath>
	<rect x="-5" y="99.1" clip-path="url(#FunnelwideStarburstID_18_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_19_" d="M60.3,161.1c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0
			C61.8,157,61.8,159.5,60.3,161.1"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_20_">
		<use xlink:href="#FunnelwideStarburstID_19_"  overflow="visible"/>
	</clipPath>
	<rect x="48.4" y="149.2" clip-path="url(#FunnelwideStarburstID_20_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_21_" d="M48.9,172.4c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0
			C50.5,168.3,50.5,170.8,48.9,172.4"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_22_">
		<use xlink:href="#FunnelwideStarburstID_21_"  overflow="visible"/>
	</clipPath>
	<rect x="37.1" y="160.6" clip-path="url(#FunnelwideStarburstID_22_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_23_" d="M37.6,183.7c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0
			C39.2,179.6,39.2,182.1,37.6,183.7"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_24_">
		<use xlink:href="#FunnelwideStarburstID_23_"  overflow="visible"/>
	</clipPath>
	<rect x="25.8" y="171.9" clip-path="url(#FunnelwideStarburstID_24_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_25_" d="M112.1,178.6c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-2.2,1.8-4,4-4S112.1,176.4,112.1,178.6"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_26_">
		<use xlink:href="#FunnelwideStarburstID_25_"  overflow="visible"/>
	</clipPath>
	<rect x="99.1" y="169.6" clip-path="url(#FunnelwideStarburstID_26_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_27_" d="M112.1,194.6c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-2.2,1.8-4,4-4S112.1,192.4,112.1,194.6"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_28_">
		<use xlink:href="#FunnelwideStarburstID_27_"  overflow="visible"/>
	</clipPath>
	<rect x="99.1" y="185.6" clip-path="url(#FunnelwideStarburstID_28_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<rect id="FunnelwideStarburstID_29_" x="104.1" y="206.6" width="8" height="8"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_30_">
		<use xlink:href="#FunnelwideStarburstID_29_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#FunnelwideStarburstID_30_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="FunnelwideStarburstID_31_" x="104" y="206" width="9" height="9"/>
			</defs>
			<clipPath id="FunnelwideStarburstID_32_">
				<use xlink:href="#FunnelwideStarburstID_31_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#FunnelwideStarburstID_32_)">
				<defs>
					<path id="FunnelwideStarburstID_33_" d="M112.1,210.6c0,2.2-1.8,4-4,4s-4-1.8-4-4c0-2.2,1.8-4,4-4S112.1,208.4,112.1,210.6"/>
				</defs>
				<clipPath id="FunnelwideStarburstID_34_">
					<use xlink:href="#FunnelwideStarburstID_33_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#FunnelwideStarburstID_34_)">
					<defs>
						<rect id="FunnelwideStarburstID_35_" x="104" y="206" width="9" height="9"/>
					</defs>
					<clipPath id="FunnelwideStarburstID_36_">
						<use xlink:href="#FunnelwideStarburstID_35_"  overflow="visible"/>
					</clipPath>
					<rect x="99.1" y="201.6" clip-path="url(#FunnelwideStarburstID_36_)" fill="#FCCF06" width="18" height="18"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_37_" d="M164.1,157.3c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7
			C160,155.8,162.5,155.8,164.1,157.3"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_38_">
		<use xlink:href="#FunnelwideStarburstID_37_"  overflow="visible"/>
	</clipPath>
	<rect x="152.2" y="151.2" clip-path="url(#FunnelwideStarburstID_38_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_39_" d="M175.4,168.6c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7
			C171.3,167.1,173.8,167.1,175.4,168.6"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_40_">
		<use xlink:href="#FunnelwideStarburstID_39_"  overflow="visible"/>
	</clipPath>
	<rect x="163.6" y="162.5" clip-path="url(#FunnelwideStarburstID_40_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_41_" d="M186.7,180c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0c-1.6-1.6-1.6-4.1,0-5.7
			C182.6,178.4,185.1,178.4,186.7,180"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_42_">
		<use xlink:href="#FunnelwideStarburstID_41_"  overflow="visible"/>
	</clipPath>
	<rect x="174.9" y="173.8" clip-path="url(#FunnelwideStarburstID_42_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_43_" d="M178.6,102.5c2.2,0,4,1.8,4,4c0,2.2-1.8,4-4,4s-4-1.8-4-4C174.6,104.3,176.4,102.5,178.6,102.5"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_44_">
		<use xlink:href="#FunnelwideStarburstID_43_"  overflow="visible"/>
	</clipPath>
	<rect x="169.6" y="97.5" clip-path="url(#FunnelwideStarburstID_44_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_45_" d="M194.6,102.5c2.2,0,4,1.8,4,4c0,2.2-1.8,4-4,4s-4-1.8-4-4C190.6,104.3,192.4,102.5,194.6,102.5"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_46_">
		<use xlink:href="#FunnelwideStarburstID_45_"  overflow="visible"/>
	</clipPath>
	<rect x="185.6" y="97.5" clip-path="url(#FunnelwideStarburstID_46_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_47_" d="M210.6,102.5c2.2,0,4,1.8,4,4c0,2.2-1.8,4-4,4s-4-1.8-4-4C206.6,104.3,208.4,102.5,210.6,102.5"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_48_">
		<use xlink:href="#FunnelwideStarburstID_47_"  overflow="visible"/>
	</clipPath>
	<rect x="201.6" y="97.5" clip-path="url(#FunnelwideStarburstID_48_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_49_" d="M154.3,53.5c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0S152.8,55.1,154.3,53.5"
			/>
	</defs>
	<clipPath id="FunnelwideStarburstID_50_">
		<use xlink:href="#FunnelwideStarburstID_49_"  overflow="visible"/>
	</clipPath>
	<rect x="148.2" y="47.3" clip-path="url(#FunnelwideStarburstID_50_)" fill="#F48220" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_51_" d="M165.6,42.2c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0
			C164.1,46.3,164.1,43.8,165.6,42.2"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_52_">
		<use xlink:href="#FunnelwideStarburstID_51_"  overflow="visible"/>
	</clipPath>
	<rect x="159.5" y="36" clip-path="url(#FunnelwideStarburstID_52_)" fill="#FAB216" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_53_" d="M177,30.9c1.6-1.6,4.1-1.6,5.7,0s1.6,4.1,0,5.7s-4.1,1.6-5.7,0S175.4,32.4,177,30.9"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_54_">
		<use xlink:href="#FunnelwideStarburstID_53_"  overflow="visible"/>
	</clipPath>
	<rect x="170.8" y="24.7" clip-path="url(#FunnelwideStarburstID_54_)" fill="#FCCF06" width="18" height="18"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_55_" d="M126.3,110.4c1.5,1.1,2.3,2.4,2.3,3.8c0,1.4-0.8,2.7-2.3,3.8c-1.2,0.9-2.9,1.7-5.1,2.3
			c-4.3,1.3-11.4,1.8-15.9,2c-2.6,0.1-6.6,0.4-9,1.1c-2.5,0.7-3.3,1.5-3.5,1.7c0.1,0.3,1,1,3.5,1.7c2.4,0.7,5.6,1.1,9,1.1
			c3.4,0,6.6-0.4,9-1.1c2.5-0.7,3.4-1.6,3.7-2.3c0.6-1.4,2.6-0.9,2.6,0.6v0c0,0.7-0.3,1.8-1.7,2.8c-0.8,0.6-2,1.1-3.4,1.6
			c-2.8,0.8-6.4,1.3-10.2,1.3c-3.8,0-7.5-0.5-10.2-1.3c-1.4-0.4-2.6-1-3.4-1.6c-1.4-1-1.7-2.1-1.7-2.8c0-0.7,0.3-1.8,1.7-2.8
			c0.8-0.6,2-1.1,3.4-1.6c2.8-0.8,6.2-1.2,10.2-1.3c4.5-0.1,11.1-0.7,15.1-1.9c3.7-1.1,5.4-2.5,5.4-3.3c0-0.4-0.8-1.3-1.9-2
			c-0.6-0.4-0.9-1.2-0.5-1.9c0.4-0.8,1.3-1,2-0.6C125.6,109.9,126.2,110.3,126.3,110.4"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_56_">
		<use xlink:href="#FunnelwideStarburstID_55_"  overflow="visible"/>
	</clipPath>
	<rect x="85" y="104.5" clip-path="url(#FunnelwideStarburstID_56_)" fill="#FFFFFF" width="48.6" height="31.2"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_57_" d="M85.4,106.5c0.7,0.2,1.5-0.2,1.7-0.9c0.3-0.8-0.1-1.7-0.9-1.9c-0.1,0-0.1,0-0.2-0.1
			c-5.3-1.6-7.2-3.5-7.2-4.6s1.9-3,7.2-4.6c3.2-1,7.2-1.7,11.5-2.1c-0.1-0.5-0.1-1-0.1-1.5c0-0.5,0-0.9,0.1-1.4
			c-4.6,0.4-8.8,1.2-12.3,2.2C79.2,93.4,76,96,76,99s3.2,5.6,9.2,7.4C85.3,106.4,85.3,106.4,85.4,106.5"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_58_">
		<use xlink:href="#FunnelwideStarburstID_57_"  overflow="visible"/>
	</clipPath>
	<rect x="71" y="84.4" clip-path="url(#FunnelwideStarburstID_58_)" fill="#FFFFFF" width="31.5" height="27.1"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_59_" d="M125.5,91.6c-3.5-1.1-7.7-1.8-12.3-2.2c0.1,0.4,0.1,0.9,0.1,1.4c0,0.5-0.1,1-0.1,1.5
			c4.3,0.4,8.2,1.1,11.5,2.1c5.3,1.6,7.2,3.5,7.2,4.6c0,1.1-1.9,3-7.2,4.6c-5.1,1.6-12.9,2.3-19.3,2.4c-5.7,0.1-11.6,0.7-15.9,2
			c-2.2,0.7-3.9,1.4-5.1,2.3c-1.5,1.1-2.3,2.4-2.3,3.8c0,1.4,0.8,2.7,2.3,3.8c1,0.7,2.3,1.4,3.9,1.9c0.1,0,0.2,0.1,0.2,0.1
			c0.8,0.3,1.6-0.2,1.8-1c0.2-0.8-0.2-1.6-0.9-1.8c-0.2-0.1-0.4-0.1-0.6-0.2c-2.7-1-4-2.2-4-2.9c0-0.8,1.7-2.2,5.4-3.3
			c4-1.2,9.4-1.8,15.1-1.9c7.6-0.2,14.7-0.9,20.2-2.6c6-1.8,9.2-4.4,9.2-7.4S131.5,93.4,125.5,91.6"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_60_">
		<use xlink:href="#FunnelwideStarburstID_59_"  overflow="visible"/>
	</clipPath>
	<rect x="77" y="84.4" clip-path="url(#FunnelwideStarburstID_60_)" fill="#FFFFFF" width="62.7" height="40.7"/>
</g>
<g>
	<defs>
		<path id="FunnelwideStarburstID_61_" d="M105.3,100.5c-5.3,0-9.6-4.4-9.6-9.7c0-5.4,4.3-9.7,9.6-9.7c5.3,0,9.6,4.4,9.6,9.7
			C114.9,96.1,110.6,100.5,105.3,100.5 M105.3,84.2c-3.6,0-6.5,2.9-6.5,6.6c0,3.6,2.9,6.6,6.5,6.6c3.6,0,6.5-2.9,6.5-6.6
			C111.8,87.1,108.9,84.2,105.3,84.2"/>
	</defs>
	<clipPath id="FunnelwideStarburstID_62_">
		<use xlink:href="#FunnelwideStarburstID_61_"  overflow="visible"/>
	</clipPath>
	<rect x="90.8" y="76" clip-path="url(#FunnelwideStarburstID_62_)" fill="#FFFFFF" width="29.1" height="29.5"/>
</g>
<circle fill="none" stroke="#FFFFFF" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" cx="105" cy="107.4" r="49.7"/>
</svg>
