<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 270 270" enable-background="new 0 0 270 270" xml:space="preserve">
<circle fill="#464646" cx="135" cy="135" r="135"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M230.4,176.7
	c-10.7,24.3-30.3,43.7-54.7,54.2"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="134.5" y1="108.9" x2="134.5" y2="95.3"/>
<path fill="none" stroke="#FCCF06" stroke-width="4" stroke-miterlimit="10" d="M147,94h-25c-1.6,0-3-1.3-3-3V66c0-1.6,1.3-3,3-3h25
	c1.6,0,3,1.3,3,3v25C149.9,92.7,148.6,94,147,94z"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="108.2" y1="135.3" x2="94.5" y2="135.3"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" d="M93.2,122.8v25c0,1.6-1.3,3-3,3h-25
	c-1.6,0-3-1.3-3-3v-25c0-1.6,1.3-3,3-3h25C91.8,119.8,93.2,121.1,93.2,122.8z"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="134.5" y1="161.6" x2="134.5" y2="175.2"/>
<path fill="none" stroke="#FAB216" stroke-width="4" stroke-miterlimit="10" d="M122,176.6h25c1.6,0,3,1.3,3,3v25c0,1.6-1.3,3-3,3
	h-25c-1.6,0-3-1.3-3-3v-25C119,177.9,120.3,176.6,122,176.6z"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="160.8" y1="135.3" x2="174.4" y2="135.3"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-miterlimit="10" d="M175.8,147.8v-25c0-1.6,1.3-3,3-3h25
	c1.6,0,3,1.3,3,3v25c0,1.6-1.3,3-3,3h-25C177.1,150.7,175.8,149.4,175.8,147.8z"/>
<path fill="#FAB216" d="M150.4,155h-31.9c-2.1,0-3.8-1.7-3.8-3.8v-31.9c0-2.1,1.7-3.8,3.8-3.8h31.9c2.1,0,3.8,1.7,3.8,3.8v31.9
	C154.2,153.3,152.5,155,150.4,155z"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="121.7" y1="125.8" x2="147.2" y2="125.8"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="121.7" y1="135.3" x2="147.2" y2="135.3"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="121.7" y1="144.8" x2="140.4" y2="144.8"/>
<circle fill="#FFFFFF" cx="146.4" cy="144.8" r="1.9"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M37.6,94.2
	C48.1,69,68.1,48.9,93.2,38.1"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M93.2,230.9
	c-24.4-10.5-44-29.8-54.7-54.1"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M230.4,176.7
	c-10.7,24.3-30.3,43.7-54.7,54.2"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M175.2,37.9
	c25.3,10.7,45.4,30.9,56,56.3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 90.9051 163.9131)" fill="#FAB216" cx="142.5" cy="28.1" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 103.6089 190.2727)" fill="#F48220" cx="164.5" cy="33.8" rx="4.4" ry="4.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 23.4493 116.9711)" fill="#FCCF06" cx="152.9" cy="30.2" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -102.2897 133.8901)" fill="#FAB216" cx="28.1" cy="127.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -75.9301 121.1863)" fill="#F48220" cx="33.8" cy="105.6" rx="4.5" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -73.9473 55.6307)" fill="#FCCF06" cx="30.2" cy="117.1" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -132.3127 327.0849)" fill="#FAB216" cx="127.5" cy="241.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -145.0164 300.7253)" fill="#F48220" cx="105.5" cy="236.2" rx="4.4" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -135.2877 153.0273)" fill="#FCCF06" cx="117.1" cy="239.8" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 60.8821 357.1079)" fill="#FAB216" cx="241.9" cy="142.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 34.5225 369.8116)" fill="#F48220" cx="236.2" cy="164.5" rx="4.4" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -37.8911 214.3677)" fill="#FCCF06" cx="239.8" cy="152.9" rx="3" ry="3"/>
</svg>
