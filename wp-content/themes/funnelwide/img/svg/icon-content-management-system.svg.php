<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="#F48220" d="M91.6,78.9h79.9c1.4,0,2.6,1.1,2.6,2.6v28.9c0,1.4-1.1,2.6-2.6,2.6H91.6c-1.4,0-2.6-1.1-2.6-2.6V81.5
	C89,80,90.2,78.9,91.6,78.9z"/>
<path fill="#FAB216" d="M91.6,118h34.8c1.4,0,2.6,1.1,2.6,2.6v20.4c0,1.4-1.1,2.6-2.6,2.6H91.6c-1.4,0-2.6-1.1-2.6-2.6v-20.4
	C89,119.1,90.2,118,91.6,118z"/>
<path fill="#FCCF06" d="M136.6,118h34.8c1.4,0,2.6,1.1,2.6,2.6v20.4c0,1.4-1.1,2.6-2.6,2.6h-34.8c-1.4,0-2.6-1.1-2.6-2.6v-20.4
	C134.1,119.1,135.2,118,136.6,118z"/>
<circle fill="#FAB216" cx="129" cy="71.2" r="2.6"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" points="81.2,163.7
	81.2,71.2 58.2,71.2 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="81.2" y1="127.8" x2="58.2" y2="127.8"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="91" y1="151" x2="126.9" y2="151"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="91" y1="159.7" x2="126.9" y2="159.7"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="136.1" y1="151" x2="172" y2="151"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="136.1" y1="159.7" x2="172" y2="159.7"/>
<circle fill="#FCCF06" cx="68.4" cy="84" r="3.8"/>
<circle fill="#FAB216" cx="68.4" cy="99.3" r="3.8"/>
<circle fill="#FAB216" cx="68" cy="140.5" r="3.8"/>
<circle fill="#F48220" cx="68" cy="156.1" r="3.8"/>
<circle fill="#F48220" cx="68.4" cy="115" r="3.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M112.3,213
	c-52.8-1.2-94.5-45-93.3-97.7c0.2-8.1,1.4-16.1,3.6-23.9"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="217.5,138.4
	205.9,144.9 199.4,133.3 "/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="11.3,96.5
	22.9,90.1 29.4,101.7 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M116.6,22
	c52.8,1.2,94.5,45,93.3,97.7c-0.2,8.1-1.4,16.1-3.6,23.9"/>
<line fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="91.7" y1="71.2" x2="118.9" y2="71.2"/>
</svg>
