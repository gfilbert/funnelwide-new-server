<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<ellipse transform="matrix(0.8155 -0.5787 0.5787 0.8155 -13.9082 103.945)" fill="#FCCF06" cx="156.1" cy="73.8" rx="3.4" ry="3.4"/>
<ellipse transform="matrix(0.8164 -0.5775 0.5775 0.8164 -1.068 39.9718)" fill="#FAB216" cx="62.3" cy="21.7" rx="3.4" ry="3.4"/>
<ellipse transform="matrix(0.8155 -0.5787 0.5787 0.8155 21.3522 98.6567)" fill="#F48220" cx="165.4" cy="15.8" rx="5" ry="5"/>
<circle fill="#FCCF06" cx="115" cy="37" r="36.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M106.1,46c0,4.9,4,9,9,9
	c4.9,0,8.9-4,8.9-9l0,0c0-6.4-5.5-8.3-8.9-9s-8.9-2.7-8.9-9c0-4.9,4-9,9-9c4.9,0,8.9,4,8.9,9"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="115" y1="19.1" x2="115" y2="12.7"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="115" y1="61.4" x2="115" y2="54.9"/>
<ellipse transform="matrix(0.2803 -0.9599 0.9599 0.2803 -133.4023 272.453)" fill="#F48220" cx="115" cy="225.2" rx="4.8" ry="4.8"/>
<ellipse transform="matrix(0.9599 -0.2803 0.2803 0.9599 -59.6439 14.2452)" fill="#F48220" cx="20" cy="215.6" rx="4.8" ry="4.8"/>
<ellipse transform="matrix(0.2803 -0.9599 0.9599 0.2803 -55.8278 356.8102)" fill="#F48220" cx="210" cy="215.6" rx="4.8" ry="4.8"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="105.5,92.8
	115,83.3 124.5,92.8 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="115" y1="213.9" x2="115" y2="85.3"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M115,86.9
	c0,54.7-32.8,104.1-83.2,125.5"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M174.1,179.1
	c9.1,7.6,19.4,13.9,30.3,18.5"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M85.5,142.2
	c-7.3,14.1-17.4,26.7-29.6,36.9"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M115,86.9
	c0,54.7,32.7,104.1,83.2,125.5"/>
</svg>
