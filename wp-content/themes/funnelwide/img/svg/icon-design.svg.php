<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M141.3,148.1
	c-10.4-2.2-18.5-10.3-20.7-20.7"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M173.1,127.8
	c-2.3,10.2-10.4,18.2-20.6,20.3"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M99,95.6
	c10.3,2.3,18.3,10.4,20.4,20.7"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M66.8,116.4
	c2.1-10.4,10.2-18.5,20.5-20.8"/>
<rect x="114.4" y="116.3" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.2" height="11.2"/>
<rect x="168.2" y="116.3" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.1" height="11.2"/>
<rect x="60.7" y="116.3" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.2" height="11.2"/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="152.4,148.1
	152.4,154.3 141.3,154.3 141.3,143.2 152.4,143.2 152.4,148.1 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="98.7" y1="95" x2="130" y2="95"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="56.3" y1="95" x2="87.6" y2="95"/>
<rect x="87.6" y="89.4" fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.2" height="11.2"/>
<path fill="#FCCF06" d="M130,89.4L130,89.4c3.1,0,5.6,2.5,5.6,5.6v0c0,3.1-2.5,5.6-5.6,5.6l0,0c-3.1,0-5.6-2.5-5.6-5.6v0
	C124.4,91.9,126.9,89.4,130,89.4z"/>
<path fill="#FCCF06" d="M56.3,89.4L56.3,89.4c3.1,0,5.6,2.5,5.6,5.6v0c0,3.1-2.5,5.6-5.6,5.6l0,0c-3.1,0-5.6-2.5-5.6-5.6v0
	C50.7,91.9,53.2,89.4,56.3,89.4z"/>
<circle fill="#343434" cx="93.1" cy="121.9" r="3.3"/>
<circle fill="#343434" cx="146.9" cy="121.9" r="3.4"/>
<path fill="#343434" d="M111.1,149.3l-6.1,1.9c-0.8,0.2-1.4,0.8-1.6,1.6l-1.9,6.1c-0.4,1.3-1.7,2-3,1.6c-0.8-0.2-1.3-0.8-1.6-1.6
	l-5.6-16.7c-0.4-1.3,0.3-2.7,1.5-3.1c0.5-0.2,1.1-0.2,1.6,0l16.7,5.6c1.3,0.4,2,1.8,1.6,3.1C112.4,148.5,111.8,149.1,111.1,149.3z"
	/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M23.8,115.4
	c1.2-50.1,42.7-89.8,92.8-88.6c7.7,0.2,15.3,1.3,22.7,3.4"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="94.5,215.4
	88.4,204.4 99.4,198.2 "/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="134.3,19.6
	140.5,30.6 129.4,36.8 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M205.1,119.6
	c-1.2,50.1-42.7,89.7-92.8,88.6c-7.7-0.2-15.3-1.3-22.6-3.4"/>
</svg>
