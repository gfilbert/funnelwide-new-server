<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 270 270" enable-background="new 0 0 270 270" xml:space="preserve">
<circle fill="#464646" cx="135" cy="135" r="135"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M161.9,165.7
	c-10.4-2.2-18.5-10.3-20.7-20.7"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M193.6,145.3
	c-2.3,10.2-10.4,18.2-20.6,20.3"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M119.5,113.1
	c10.3,2.3,18.3,10.4,20.5,20.7"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M87.4,134
	c2.1-10.4,10.2-18.5,20.5-20.8"/>
<rect x="135" y="133.8" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.1" height="11.1"/>
<rect x="188.7" y="133.8" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.1" height="11.1"/>
<rect x="81.2" y="133.8" fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.2" height="11.1"/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="173,165.7
	173,171.8 161.9,171.8 161.9,160.7 173,160.7 173,165.7 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="119.3" y1="112.5" x2="150.6" y2="112.5"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="76.8" y1="112.5" x2="108.1" y2="112.5"/>
<rect x="108.1" y="106.9" fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" width="11.2" height="11.2"/>
<path fill="#FCCF06" d="M150.6,106.9L150.6,106.9c3.1,0,5.6,2.5,5.6,5.6v0c0,3.1-2.5,5.6-5.6,5.6l0,0c-3.1,0-5.6-2.5-5.6-5.6v0
	C145,109.4,147.5,106.9,150.6,106.9z"/>
<path fill="#FCCF06" d="M76.8,106.9L76.8,106.9c3.1,0,5.6,2.5,5.6,5.6v0c0,3.1-2.5,5.6-5.6,5.6h0c-3.1,0-5.6-2.5-5.6-5.6v0
	C71.2,109.4,73.7,106.9,76.8,106.9z"/>
<circle fill="#FFFFFF" cx="113.7" cy="139.4" r="3.4"/>
<circle fill="#FFFFFF" cx="167.4" cy="139.4" r="3.4"/>
<path fill="#FFFFFF" d="M131.7,166.8l-6.1,1.9c-0.8,0.2-1.4,0.8-1.6,1.6l-1.9,6.1c-0.4,1.3-1.7,2-3,1.6c-0.8-0.2-1.3-0.8-1.6-1.6
	l-5.6-16.7c-0.4-1.3,0.3-2.7,1.5-3.1c0.5-0.2,1.1-0.2,1.6,0l16.7,5.6c1.3,0.4,2,1.8,1.5,3.1C133,166,132.4,166.6,131.7,166.8z"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M44.3,133
	c1.1-50.1,42.6-89.8,92.7-88.6c7.7,0.2,15.3,1.3,22.7,3.4"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="115.1,232.9
	108.9,221.9 120,215.7 "/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="154.9,37.1
	161.1,48.1 150,54.3 "/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M225.7,137
	c-1.2,50.1-42.7,89.7-92.8,88.6c-7.7-0.2-15.3-1.3-22.6-3.4"/>
</svg>
