<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 130 130" enable-background="new 0 0 130 130" xml:space="preserve">
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M80.6,82.9H51.4
	c-1.1,0-2.1-0.7-2.4-1.8L44.1,65c-0.4-1.3,0.4-2.8,1.7-3.2c0.2-0.1,0.5-0.1,0.7-0.1H87L80.6,82.9z"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" points="50.6,89 78.8,89
	88.8,55.7 94.5,55.7 "/>
<circle fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" cx="50.6" cy="92.2" r="3.2"/>
<circle fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" cx="78.5" cy="92.2" r="3.2"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="65.1" y1="68.9" x2="65.1" y2="75.7"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="54.7" y1="68.9" x2="55.5" y2="75.7"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="75.5" y1="68.9" x2="74.7" y2="75.7"/>
<circle fill="#FCCF06" cx="43.5" cy="31.7" r="2"/>
<circle fill="#FAB216" cx="86.7" cy="43.3" r="2"/>
<path fill="none" stroke="#F48220" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M59.6,45
	c-0.1,2.4,1.8,4.5,4.2,4.6c2.4,0.1,4.5-1.8,4.6-4.2c0-0.1,0-0.3,0-0.4c0-3.2-2.7-4.1-4.4-4.4s-4.4-1.3-4.4-4.4
	c-0.1-2.4,1.8-4.5,4.2-4.6c2.4-0.1,4.5,1.8,4.6,4.2c0,0.1,0,0.3,0,0.4"/>
<line fill="none" stroke="#F48220" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="64" y1="31.7" x2="64" y2="28.5"/>
<line fill="none" stroke="#F48220" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="64" y1="52.6" x2="64" y2="49.4"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="18.8,101.4
	28.6,101.4 28.6,111.2 "/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="15.5" y1="114.5" x2="28.6" y2="101.4"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M35.3,22.5
	C53,10.1,76.7,10,94.5,22.4"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M22.5,94.7
	C10,76.9,10,53.1,22.5,35.3"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M94.7,107.5
	c-17.9,12.5-41.6,12.5-59.5,0"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M107.5,35.3
	c12.5,17.9,12.5,41.6,0,59.5"/>
<circle fill="#F48220" cx="11.7" cy="118.3" r="2"/>
<circle fill="#FCCF06" cx="7.9" cy="122.1" r="1.5"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="101.4,111.2
	101.4,101.4 111.2,101.4 "/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="114.5" y1="114.5" x2="101.4" y2="101.4"/>
<circle fill="#FAB216" cx="118.3" cy="118.3" r="2"/>
<circle fill="#F48220" cx="122.1" cy="122.1" r="1.5"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="111.2,28.6
	101.4,28.6 101.4,18.8 "/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="18.8,28.6
	28.6,28.6 28.6,18.8 "/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="114.5" y1="15.5" x2="101.4" y2="28.6"/>
<circle fill="#FAB216" cx="118.3" cy="11.7" r="2"/>
<circle fill="#FCCF06" cx="122.1" cy="7.9" r="1.5"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="15.5" y1="15.5" x2="28.6" y2="28.6"/>
<circle fill="#FCCF06" cx="11.7" cy="11.7" r="2"/>
<circle fill="#F48220" cx="7.9" cy="7.9" r="1.5"/>
</svg>
