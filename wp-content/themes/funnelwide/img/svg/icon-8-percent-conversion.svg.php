<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<circle fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" cx="107.4" cy="115.9" r="12.5"/>
<circle fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" cx="107.4" cy="142.8" r="14.3"/>
<circle fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" cx="132.4" cy="110.2" r="4.1"/>
<circle fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" cx="149" cy="123.9" r="4.1"/>
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="149.8" y1="106.3" x2="131.5" y2="128.7"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M105.6,84.5V36.3
	c0-5.9-4.8-10.7-10.7-10.7c-5.9,0-10.7,4.8-10.7,10.7v48.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="62.6,108.5
	84.1,162 84.1,178.5 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="84.1" y1="84.5" x2="84.1" y2="98.7"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="160.4" y1="212.4" x2="160.4" y2="221.8"/>
<polyline fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="81,212.3
	81,188.6 160.4,188.6 "/>
<circle fill="#FAB216" cx="93.1" cy="200.3" r="3.2"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M127.1,91V66.7
	c0-5.9-4.8-10.7-10.7-10.7c-5.9,0-10.7,4.8-10.7,10.7v17.7"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M148.6,89.7V74.4
	c0-5.9-4.8-10.7-10.7-10.7s-10.7,4.8-10.7,10.7V91"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M170.1,89.7v-1.3
	c0-5.9-4.8-10.7-10.7-10.7c-5.9,0-10.7,4.8-10.7,10.7V91"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="170.1,89.7
	170.1,114.8 160.4,162 160.4,188.6 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M71.6,71.4
	c-5.2,0.9-9,5.3-9,10.6v26.4"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="94.8" y1="13.8" x2="94.8" y2="1.3"/>
<line fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="81.2" y1="18.2" x2="74" y2="8.1"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="72.9" y1="29.7" x2="61.1" y2="25.9"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="116.8" y1="29.7" x2="128.6" y2="25.9"/>
<line fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="108.4" y1="18.2" x2="115.7" y2="8.1"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="square" stroke-linejoin="round" x1="160.4" y1="188.6" x2="160.4" y2="212.4"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M25.2,173.1
	c-11.6-17.4-17.8-37.8-17.9-58.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M202.7,54.9
	c11.7,17.5,17.9,38.1,17.9,59.1"/>
<path fill="#F48220" d="M186.6,35.6c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0c-0.8-0.7-1.2-1.8-1.2-2.8
	C185.5,37.4,185.9,36.4,186.6,35.6z"/>
<path fill="#F48220" d="M35.6,186.6c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-1.6,1.6-4.1,1.6-5.7,0c-0.8-0.7-1.2-1.8-1.2-2.8
	C34.4,188.4,34.9,187.4,35.6,186.6z"/>
</svg>
