<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 195 199" enable-background="new 0 0 195 199" xml:space="preserve">
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	63.3,196.6 58.2,186.4 68.5,181.3 "/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	131.3,2 136.3,12.2 126.1,17.3 "/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M20.2,155.1
	C8.7,139.5,2,120.2,2,99.3c0-52.5,42.7-95,95.3-95c13.4,0,26.1,2.7,37.6,7.7"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M174.4,43.5
	c11.4,15.7,18.2,35,18.2,55.8c0,52.5-42.7,95-95.3,95c-13.4,0-26.1-2.7-37.6-7.7"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-miterlimit="10" points="20.2,155.1 75.8,86.4 110.9,123.3 
	174.4,43.5 "/>
<g>
	<defs>
		<path id="GoalsChartID_1_" d="M85.3,86.4c0,5.3-4.3,9.6-9.6,9.6c-5.3,0-9.6-4.3-9.6-9.6c0-5.3,4.3-9.6,9.6-9.6
			C81.1,76.8,85.3,81.1,85.3,86.4"/>
	</defs>
	<clipPath id="GoalsChartID_2_">
		<use xlink:href="#GoalsChartID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="61.2" y="71.8" clip-path="url(#GoalsChartID_2_)" fill="#FCCF06" width="29.2" height="29.1"/>
</g>
<g>
	<defs>
		<path id="GoalsChartID_3_" d="M120.5,123.3c0,5.3-4.3,9.6-9.6,9.6c-5.3,0-9.6-4.3-9.6-9.6c0-5.3,4.3-9.6,9.6-9.6
			C116.2,113.8,120.5,118,120.5,123.3"/>
	</defs>
	<clipPath id="GoalsChartID_4_">
		<use xlink:href="#GoalsChartID_3_"  overflow="visible"/>
	</clipPath>
	<rect x="96.3" y="108.8" clip-path="url(#GoalsChartID_4_)" fill="#FAB216" width="29.2" height="29.1"/>
</g>
<g>
	<defs>
		<path id="GoalsChartID_5_" d="M28,149.5c3.1,4.3,2.2,10.3-2.1,13.4c-4.3,3.1-10.3,2.2-13.4-2.1c-3.1-4.3-2.2-10.3,2.1-13.4
			C18.8,144.3,24.8,145.2,28,149.5"/>
	</defs>
	<clipPath id="GoalsChartID_6_">
		<use xlink:href="#GoalsChartID_5_"  overflow="visible"/>
	</clipPath>
	<rect x="5.6" y="140.6" clip-path="url(#GoalsChartID_6_)" fill="#FAB216" width="29.2" height="29.1"/>
</g>
<g>
	<defs>
		<path id="GoalsChartID_7_" d="M166.6,49.1c-3.1-4.3-2.2-10.3,2.1-13.4c4.3-3.1,10.3-2.2,13.4,2.1c3.1,4.3,2.2,10.3-2.1,13.4
			C175.7,54.3,169.7,53.4,166.6,49.1"/>
	</defs>
	<clipPath id="GoalsChartID_8_">
		<use xlink:href="#GoalsChartID_7_"  overflow="visible"/>
	</clipPath>
	<rect x="159.8" y="28.9" clip-path="url(#GoalsChartID_8_)" fill="#F48220" width="29.2" height="29.1"/>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="56.2" y1="86.4" x2="32.4" y2="86.4"/>
<g>
	<defs>
		<path id="GoalsChartID_9_" d="M25.4,86.4c0,1.3-1.1,2.4-2.4,2.4s-2.4-1.1-2.4-2.4c0-1.3,1.1-2.4,2.4-2.4S25.4,85.1,25.4,86.4"/>
	</defs>
	<clipPath id="GoalsChartID_10_">
		<use xlink:href="#GoalsChartID_9_"  overflow="visible"/>
	</clipPath>
	<rect x="15.6" y="79" clip-path="url(#GoalsChartID_10_)" fill="#FFFFFF" width="14.8" height="14.8"/>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="123.7" y1="143" x2="99.9" y2="143"/>
<g>
	<defs>
		<path id="GoalsChartID_11_" d="M92.9,143c0,1.3-1.1,2.4-2.4,2.4s-2.4-1.1-2.4-2.4s1.1-2.4,2.4-2.4S92.9,141.7,92.9,143"/>
	</defs>
	<clipPath id="GoalsChartID_12_">
		<use xlink:href="#GoalsChartID_11_"  overflow="visible"/>
	</clipPath>
	<rect x="83.1" y="135.6" clip-path="url(#GoalsChartID_12_)" fill="#FFFFFF" width="14.8" height="14.8"/>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="154.1" y1="43.5" x2="130.3" y2="43.5"/>
<g>
	<defs>
		<path id="GoalsChartID_13_" d="M123.3,43.5c0,1.3-1.1,2.4-2.4,2.4c-1.3,0-2.4-1.1-2.4-2.4c0-1.3,1.1-2.4,2.4-2.4
			C122.2,41.1,123.3,42.2,123.3,43.5"/>
	</defs>
	<clipPath id="GoalsChartID_14_">
		<use xlink:href="#GoalsChartID_13_"  overflow="visible"/>
	</clipPath>
	<rect x="113.5" y="36.1" clip-path="url(#GoalsChartID_14_)" fill="#FFFFFF" width="14.8" height="14.8"/>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="123.7" y1="154.2" x2="99.9" y2="154.2"/>
<g>
	<defs>
		<path id="GoalsChartID_15_" d="M92.9,154.2c0,1.3-1.1,2.4-2.4,2.4s-2.4-1.1-2.4-2.4s1.1-2.4,2.4-2.4S92.9,152.9,92.9,154.2"/>
	</defs>
	<clipPath id="GoalsChartID_16_">
		<use xlink:href="#GoalsChartID_15_"  overflow="visible"/>
	</clipPath>
	<rect x="83.1" y="146.8" clip-path="url(#GoalsChartID_16_)" fill="#FFFFFF" width="14.8" height="14.8"/>
</g>
<g>
	<defs>
		<path id="GoalsChartID_17_" d="M48.9,179.7c0,2.6-2.1,4.8-4.8,4.8c-2.6,0-4.8-2.1-4.8-4.8c0-2.6,2.1-4.8,4.8-4.8
			C46.8,174.9,48.9,177.1,48.9,179.7"/>
	</defs>
	<clipPath id="GoalsChartID_18_">
		<use xlink:href="#GoalsChartID_17_"  overflow="visible"/>
	</clipPath>
	<rect x="34.3" y="169.9" clip-path="url(#GoalsChartID_18_)" fill="#F48220" width="19.6" height="19.6"/>
</g>
<g>
	<defs>
		<path id="GoalsChartID_19_" d="M145.6,18.9c0-2.6,2.1-4.8,4.8-4.8c2.6,0,4.8,2.1,4.8,4.8c0,2.6-2.1,4.8-4.8,4.8
			C147.8,23.7,145.6,21.6,145.6,18.9"/>
	</defs>
	<clipPath id="GoalsChartID_20_">
		<use xlink:href="#GoalsChartID_19_"  overflow="visible"/>
	</clipPath>
	<rect x="140.6" y="9.1" clip-path="url(#GoalsChartID_20_)" fill="#FCCF06" width="19.6" height="19.6"/>
</g>
</svg>
