<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M59.5,196.5
	C15,167.2,2.8,107.4,32,63c12.7-19.2,31.8-33.3,54-39.6"/>
<path fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M51,212.3h61.4
	c53.2,0,96.3-43.1,96.4-96.3c0-53.2-43.1-96.3-96.3-96.4c0,0,0,0,0,0"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="211.9" y1="212.3" x2="163.5" y2="212.3"/>
<polyline fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="206.6,201.9
	217.1,212.3 206.6,222.8 "/>
<polyline fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="117.7,30.2
	107.2,19.7 117.7,9.2 "/>
<ellipse transform="matrix(0.9853 -0.1709 0.1709 0.9853 -36.0432 5.9698)" fill="#FAB216" cx="16.7" cy="212.3" rx="3.8" ry="3.8"/>
<ellipse transform="matrix(0.4308 -0.9024 0.9024 0.4308 -172.3976 151.3334)" fill="#F48220" cx="33.8" cy="212.3" rx="5.8" ry="5.8"/>
<ellipse transform="matrix(0.262 -0.9651 0.9651 0.262 8.027 203.3618)" fill="#FAB216" cx="137" cy="96.4" rx="11.4" ry="11.3"/>
<ellipse transform="matrix(0.9297 -0.3683 0.3683 0.9297 -34.9706 61.3213)" fill="#FCCF06" cx="143.2" cy="122.3" rx="7.6" ry="7.6"/>
<ellipse transform="matrix(0.3049 -0.9524 0.9524 0.3049 -41.7554 223.7579)" fill="#F48220" cx="132.4" cy="140.5" rx="5.7" ry="5.7"/>
<ellipse transform="matrix(0.737 -0.6758 0.6758 0.737 -68.549 118.3232)" fill="#FAB216" cx="117.8" cy="147.3" rx="3.8" ry="3.8"/>
<ellipse transform="matrix(0.9369 -0.3497 0.3497 0.9369 -44.7467 46.2741)" fill="#FCCF06" cx="105.8" cy="147" rx="1.9" ry="1.9"/>
<ellipse transform="matrix(0.2745 -0.9616 0.9616 0.2745 -67.2754 182.6476)" fill="#FCCF06" cx="87.4" cy="135.9" rx="11.3" ry="11.4"/>
<ellipse transform="matrix(0.9344 -0.3562 0.3562 0.9344 -33.8337 36.2697)" fill="#FAB216" cx="81.6" cy="110" rx="7.6" ry="7.6"/>
<ellipse transform="matrix(0.3171 -0.9484 0.9484 0.3171 -23.965 150.5282)" fill="#F48220" cx="92.5" cy="91.9" rx="5.7" ry="5.7"/>
<ellipse transform="matrix(0.7457 -0.6663 0.6663 0.7457 -29.5638 93.1585)" fill="#FAB216" cx="107.3" cy="85.3" rx="3.8" ry="3.8"/>
<ellipse transform="matrix(0.9413 -0.3376 0.3376 0.9413 -21.9241 45.2964)" fill="#FCCF06" cx="119.3" cy="85.7" rx="1.9" ry="1.9"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="51" y1="212.3" x2="112.4" y2="212.3"/>
<circle fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" cx="112.4" cy="116" r="66.2"/>
</svg>
