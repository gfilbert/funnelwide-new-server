<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 220 224" enable-background="new 0 0 220 224" xml:space="preserve">
<g>
	<defs>
		<path id="GearDollarSignID_1_" d="M145.6,112.3c0-20.1-16.3-36.3-36.3-36.3C89.3,76,73,92.3,73,112.3s16.3,36.3,36.3,36.3
			C129.4,148.6,145.6,132.4,145.6,112.3z M121.2,175.8l-9.9-15.5c-0.7-1.1-1.8-1.7-3.1-1.8c-1.3,0-2.5,0.5-3.2,1.5l-11,14.8
			c-4.3-1.1-8.6-2.6-12.6-4.6l1.2-18.4c0.1-1.3-0.5-2.5-1.5-3.3c-1-0.8-2.3-1-3.5-0.6l-17.5,5.6c-2.9-3.4-5.4-7.2-7.5-11.1
			l11.7-14.2c0.8-1,1.1-2.3,0.7-3.5c-0.3-1.2-1.3-2.2-2.5-2.6l-17.5-5.8c-0.3-4.5-0.2-9,0.5-13.4l17.8-4.6c1.2-0.4,2.2-1.2,2.6-2.4
			c0.4-1.2,0.3-2.5-0.5-3.5l-10.7-15c2.4-3.8,5.1-7.4,8.2-10.6l17.2,6.8c1.2,0.5,2.5,0.3,3.5-0.4c1-0.7,1.7-1.9,1.7-3.1l0.1-18.4
			c4.6-1.9,8-2.9,12.9-3.7l9.9,15.5c0.7,1.1,1.8,1.7,3.1,1.8c1.3,0,2.5-0.5,3.2-1.5l11-14.8c4.3,1.1,8.6,2.6,12.6,4.6l-1.2,18.4
			c-0.1,1.3,0.5,2.5,1.5,3.2c1,0.8,2.3,1,3.5,0.6l17.5-5.6c2.9,3.4,5.4,7.2,7.5,11.1l-11.7,14.2c-0.8,1-1.1,2.3-0.7,3.5
			c0.3,1.2,1.3,2.2,2.5,2.6l17.5,5.8c0.3,4.5,0.2,9-0.5,13.4l-17.8,4.6c-1.2,0.4-2.2,1.2-2.6,2.4c-0.4,1.2-0.3,2.5,0.5,3.5l10.7,15
			c-2.4,3.8-5.1,7.4-8.2,10.6l-17.2-6.8c-1.2-0.5-2.5-0.3-3.5,0.4c-1,0.7-1.7,1.9-1.7,3.1l-0.1,18.4
			C129.6,174,126.1,175,121.2,175.8z"/>
	</defs>
	<clipPath id="GearDollarSignID_2_">
		<use xlink:href="#GearDollarSignID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="40" y="43" clip-path="url(#GearDollarSignID_2_)" fill="#FCCF06" width="139.8" height="137.8"/>
</g>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M209.8,104
	c2.5,33.8-12.3,68-41.9,89.1c-17.6,12.5-37.9,18.5-57.9,18.5"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M10.3,119.6
	c-2.5-33.8,12.3-68,41.9-89.1C69.8,18,90.1,12,110.1,12"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M101,120.7
	c0,5.1,4.1,9.2,9.2,9.2c5.1,0,9.2-4.1,9.2-9.2c0-6.6-5.7-8.6-9.2-9.2c-3.5-0.7-9.2-2.8-9.2-9.2c0-5.1,4.1-9.2,9.2-9.2
	c5.1,0,9.2,4.1,9.2,9.2"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="110" y1="92.7" x2="110" y2="86"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="110" y1="136.7" x2="110" y2="130"/>
<g>
	<defs>
		<path id="GearDollarSignID_3_" d="M27.6,171.4c1.1,1.5,3.2,1.9,4.7,0.8c1.5-1.1,1.9-3.2,0.8-4.7c-1.1-1.5-3.2-1.9-4.7-0.8
			C26.9,167.7,26.5,169.8,27.6,171.4"/>
	</defs>
	<clipPath id="GearDollarSignID_4_">
		<use xlink:href="#GearDollarSignID_3_"  overflow="visible"/>
	</clipPath>
	<rect x="22" y="161" clip-path="url(#GearDollarSignID_4_)" fill="#FAB216" width="16.8" height="16.8"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_5_" d="M185.6,55.4c1.1,1.5,3.2,1.9,4.7,0.8c1.5-1.1,1.9-3.2,0.8-4.7c-1.1-1.5-3.2-1.9-4.7-0.8
			C184.9,51.7,184.5,53.8,185.6,55.4"/>
	</defs>
	<clipPath id="GearDollarSignID_6_">
		<use xlink:href="#GearDollarSignID_5_"  overflow="visible"/>
	</clipPath>
	<rect x="180" y="45" clip-path="url(#GearDollarSignID_6_)" fill="#FAB216" width="16.8" height="16.8"/>
</g>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	101,2 110.9,11.9 101,21.7 "/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	117.9,221.7 108,211.9 117.9,202 "/>
<g>
	<defs>
		<path id="GearDollarSignID_7_" d="M206.2,96.4c-3.3,0-6.4-1.6-8.3-4.3c-3.3-4.6-2.2-11,2.4-14.2c1.7-1.2,3.8-1.9,5.9-1.9
			c3.3,0,6.4,1.6,8.3,4.3c1.6,2.2,2.2,4.9,1.7,7.6c-0.5,2.7-1.9,5-4.2,6.6C210.4,95.8,208.3,96.4,206.2,96.4"/>
	</defs>
	<clipPath id="GearDollarSignID_8_">
		<use xlink:href="#GearDollarSignID_7_"  overflow="visible"/>
	</clipPath>
	<rect x="191" y="71" clip-path="url(#GearDollarSignID_8_)" fill="#F48220" width="30.4" height="30.4"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_9_" d="M206.7,79.1c2.4,0,4.8,1.1,6.2,3.2c2.4,3.4,1.6,8.2-1.8,10.7c-1.3,1-2.9,1.4-4.4,1.4
			c-2.4,0-4.8-1.1-6.2-3.2c-2.4-3.4-1.6-8.2,1.8-10.7C203.7,79.6,205.2,79.1,206.7,79.1 M206.7,74L206.7,74c-2.7,0-5.2,0.8-7.4,2.4
			c-2.8,2-4.6,4.9-5.2,8.3c-0.6,3.4,0.2,6.7,2.2,9.5c2.4,3.4,6.3,5.4,10.4,5.4c2.7,0,5.2-0.8,7.4-2.4c2.8-2,4.6-4.9,5.2-8.3
			c0.6-3.4-0.2-6.7-2.2-9.5C214.8,76,210.9,74,206.7,74"/>
	</defs>
	<clipPath id="GearDollarSignID_10_">
		<use xlink:href="#GearDollarSignID_9_"  overflow="visible"/>
	</clipPath>
	<rect x="189" y="69" clip-path="url(#GearDollarSignID_10_)" fill="#FFFFFF" width="35.5" height="35.5"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_11_" d="M13.2,147.4c-3.3,0-6.4-1.6-8.3-4.3c-3.3-4.6-2.2-11,2.4-14.2c1.7-1.2,3.8-1.9,5.9-1.9
			c3.3,0,6.4,1.6,8.3,4.3c3.3,4.6,2.2,11-2.4,14.2C17.4,146.8,15.3,147.4,13.2,147.4"/>
	</defs>
	<clipPath id="GearDollarSignID_12_">
		<use xlink:href="#GearDollarSignID_11_"  overflow="visible"/>
	</clipPath>
	<rect x="-2" y="122" clip-path="url(#GearDollarSignID_12_)" fill="#F48220" width="30.4" height="30.4"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_13_" d="M12.7,129.1c2.4,0,4.8,1.1,6.2,3.2c2.4,3.4,1.6,8.2-1.8,10.7c-1.3,1-2.9,1.4-4.4,1.4
			c-2.4,0-4.8-1.1-6.2-3.2c-2.4-3.4-1.6-8.2,1.8-10.7C9.7,129.6,11.2,129.1,12.7,129.1 M12.7,124L12.7,124c-2.7,0-5.2,0.8-7.4,2.4
			c-5.7,4.1-7.1,12-3,17.8c2.4,3.4,6.3,5.4,10.4,5.4c2.7,0,5.2-0.8,7.4-2.4c5.7-4.1,7.1-12,3-17.8C20.8,126,16.9,124,12.7,124"/>
	</defs>
	<clipPath id="GearDollarSignID_14_">
		<use xlink:href="#GearDollarSignID_13_"  overflow="visible"/>
	</clipPath>
	<rect x="-5" y="119" clip-path="url(#GearDollarSignID_14_)" fill="#FFFFFF" width="35.5" height="35.5"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_15_" d="M17,155.2c0-2.9,2.3-5.2,5.2-5.2c2.9,0,5.2,2.3,5.2,5.2c0,2.9-2.3,5.2-5.2,5.2C19.3,160.3,17,158,17,155.2
			"/>
	</defs>
	<clipPath id="GearDollarSignID_16_">
		<use xlink:href="#GearDollarSignID_15_"  overflow="visible"/>
	</clipPath>
	<rect x="12" y="145" clip-path="url(#GearDollarSignID_16_)" fill="#FAB216" width="20.3" height="20.3"/>
</g>
<g>
	<defs>
		<path id="GearDollarSignID_17_" d="M202.3,68.2c0,2.9-2.3,5.2-5.2,5.2c-2.9,0-5.2-2.3-5.2-5.2c0-2.9,2.3-5.2,5.2-5.2
			C200,63,202.3,65.3,202.3,68.2"/>
	</defs>
	<clipPath id="GearDollarSignID_18_">
		<use xlink:href="#GearDollarSignID_17_"  overflow="visible"/>
	</clipPath>
	<rect x="187" y="58" clip-path="url(#GearDollarSignID_18_)" fill="#FAB216" width="20.3" height="20.3"/>
</g>
</svg>
