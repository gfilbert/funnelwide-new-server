<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M79.7,143.1h-26
	c0,0,22.2-33.7,23.7-36c1.4-2.1,2.2-4.6,2.2-7.2c-0.1-7.2-5.9-12.9-13.1-12.9c-4.2,0-8.2,2.1-10.6,5.6"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M87.6,130.1
	c0,7.2,5.8,13,13,13s13-5.8,13-13V99.9c0-7.2-5.8-13-13-13s-13,5.8-13,13V130.1z"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M121.4,130.1
	c0,7.2,5.8,13,13,13c7.2,0,13-5.8,13-13V99.9c0-7.2-5.8-13-13-13s-13,5.8-13,13L121.4,130.1z"/>
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="157" y1="86.9" x2="157" y2="143.1"/>
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="157" y1="130.5" x2="179.6" y2="108.6"/>
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="162.9" y1="125.5" x2="180.2" y2="143.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="126.5,35
	115,46.6 103.5,35 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="115" y1="4" x2="115" y2="44.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="25,103.5
	36.6,115 25,126.5 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="4" y1="115" x2="34.1" y2="115"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="103.5,194.9
	115,183.4 126.5,194.9 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="115" y1="226" x2="115" y2="185.9"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="204.9,126.5
	193.4,115 204.9,103.5 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="226" y1="115" x2="195.9" y2="115"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M4,115
	c0-35.5,17-68.9,45.8-89.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M115,226
	c-35.5,0-68.9-17-89.8-45.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M226,115
	c0,35.5-17,68.9-45.8,89.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M115,4
	c35.5,0,68.9,17,89.8,45.7"/>
<circle fill="#FCCF06" cx="68.3" cy="13.3" r="8.8"/>
<path fill="#FAB216" d="M62.8,20.1c3.2,2.6,7.8,2.6,11,0l0.2,0.2l4.1,4.6c1.1,1.3,1.7,2.9,1.7,4.6V32H56.8v-2.5
	c0-1.7,0.6-3.3,1.7-4.6L62.8,20.1z"/>
<circle fill="#FAB216" cx="212.4" cy="63.7" r="8.8"/>
<path fill="#FCCF06" d="M206.9,70.6c3.2,2.6,7.8,2.6,11,0l0.2,0.2l4.1,4.6c1.1,1.3,1.7,2.9,1.7,4.6v2.5H201v-2.5
	c0-1.7,0.6-3.3,1.7-4.6L206.9,70.6z"/>
<circle fill="#F48220" cx="164.4" cy="203.7" r="8.8"/>
<path fill="#FAB216" d="M158.9,210.6c3.2,2.6,7.8,2.6,11,0l0.2,0.2l4.1,4.6c1.1,1.3,1.7,2.9,1.7,4.6v2.5H153v-2.5
	c0-1.7,0.6-3.3,1.7-4.6L158.9,210.6z"/>
<circle fill="#FCCF06" cx="17.1" cy="155.1" r="8.8"/>
<path fill="#F48220" d="M11.6,162c3.2,2.6,7.8,2.6,11,0l0.2,0.2l4.1,4.6c1.1,1.3,1.7,2.9,1.7,4.6v2.5H5.6v-2.5
	c0-1.7,0.6-3.3,1.7-4.6L11.6,162z"/>
</svg>
