<svg width="122px" height="449px" viewBox="0 0 122 449" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <ellipse id="path-1" cx="8" cy="259" rx="8" ry="8"></ellipse>
        <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-1"></use>
        </mask>
        <circle id="path-3" cx="69" cy="8" r="8"></circle>
        <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-3"></use>
        </mask>
        <ellipse id="path-5" cx="114" cy="441" rx="8" ry="8"></ellipse>
        <mask id="mask-6" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-5"></use>
        </mask>
    </defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Publisher-Network" transform="translate(-1246.000000, -533.000000)" stroke-width="4" fill="#FFFFFF">
            <g id="Group-3" transform="translate(1246.000000, 533.000000)">
                <use id="Oval-Copy-13" stroke="#FFD100" mask="url(#mask-2)" xlink:href="#path-1"></use>
                <use id="Oval-Copy-11" stroke="#FF8403" mask="url(#mask-4)" xlink:href="#path-3"></use>
                <use id="Oval-Copy-12" stroke="#FF8403" mask="url(#mask-6)" xlink:href="#path-5"></use>
            </g>
        </g>
    </g>
</svg>
