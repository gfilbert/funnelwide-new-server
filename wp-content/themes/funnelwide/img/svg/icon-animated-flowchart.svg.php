<!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="click-path-graphic" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 334 318" style="enable-background:new 0 0 334 318;" xml:space="preserve">
<style type="text/css">
	.clickpath0{fill-rule:evenodd;clip-rule:evenodd;fill:#FFEEA5;}
	.clickpath1{fill-rule:evenodd;clip-rule:evenodd;fill:#FAB216;}
	.clickpath2{fill:none;stroke:#464646;stroke-width:3.9;stroke-linecap:round;stroke-miterlimit:10;}
	.clickpath3{fill-rule:evenodd;clip-rule:evenodd;fill:#FFD100;}
	.clickpath4{fill-rule:evenodd;clip-rule:evenodd;fill:#F48220;}
	.clickpath5{fill:none;stroke:#FFEEA5;stroke-width:3.9;stroke-linecap:round;stroke-miterlimit:10;}
	.clickpath6{fill:none;stroke:#464646;stroke-width:3;stroke-linecap:round;stroke-miterlimit:10;}
	.clickpath7{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
	.clickpath8{fill:none;stroke:#FF8403;stroke-width:2;stroke-miterlimit:10;}
	.clickpath9{fill:none;stroke:#FFD100;stroke-width:2;stroke-miterlimit:10;}
	.clickpath10{fill:none;stroke:#FFB502;stroke-width:2;stroke-miterlimit:10;}
	.clickpath12{fill:none;stroke:#464646;stroke-width:6;stroke-linecap:round;stroke-miterlimit:10;}
	.clickpath13{fill:none;stroke:#FF8403;stroke-width:3.1873;stroke-miterlimit:10;}
	.clickpath14{fill:none;stroke:#FFD100;stroke-width:3.1873;stroke-miterlimit:10;}
	.clickpath15{fill:none;stroke:#FFB502;stroke-width:3.1873;stroke-miterlimit:10;}
	.clickpath16{fill:none;stroke-width:3.9;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
</style>
<circle class="clickpath10" cx="314" cy="253.6" r="5"/>
<circle class="clickpath9" cx="34" cy="306.6" r="5"/>
<circle class="clickpath9" cx="23" cy="159.6" r="5"/>
<path class="clickpath4" d="M309.4,39.2h-70.7c-3.1,0-5.7-2.6-5.7-5.7V8.3c0-3.1,2.6-5.7,5.7-5.7h70.7c3.1,0,5.7,2.5,5.7,5.7v25.2
	C315.1,36.6,312.6,39.2,309.4,39.2"/>
<line class="clickpath2" x1="236" y1="47.6" x2="310.8" y2="47.6"/>
<line class="clickpath2" x1="236" y1="56.6" x2="256.6" y2="56.6"/>
<g class="clickpath11 opacity">
	<line class="clickpath2" x1="22.5" y1="47.9" x2="95.6" y2="47.9"/>
	<line class="clickpath2" x1="22.5" y1="57" x2="76.2" y2="57"/>
	<path class="clickpath3" d="M201.7,39.2H131c-3.1,0-5.7-2.6-5.7-5.7V8.3c0-3.1,2.6-5.7,5.7-5.7h70.7c3.1,0,5.7,2.6,5.7,5.7v25.2
		C207.4,36.6,204.9,39.2,201.7,39.2"/>
	<path class="clickpath1" d="M96.7,39.2H26c-3.1,0-5.7-2.6-5.7-5.7V8.3c0-3.1,2.6-5.7,5.7-5.7h70.7c3.1,0,5.7,2.6,5.7,5.7v25.2
		C102.4,36.6,99.9,39.2,96.7,39.2"/>
	<line class="clickpath2" x1="127.8" y1="47.9" x2="192.6" y2="47.9"/>
	<line class="clickpath2" x1="127.8" y1="57" x2="162.2" y2="57"/>
	<path class="clickpath2" d="M59,74.8v8.4c0,3.3,2.6,5.9,5.9,5.9h203.4c3.3,0,5.9-2.6,5.9-5.9v-8.4"/>
	<path class="clickpath2" d="M226.7,121.4V113c0-3.3-2.6-5.9-5.9-5.9H112.5c-3.3,0-5.9,2.6-5.9,5.9v8.4"/>
	<path class="clickpath2" d="M207.7,235.4V227c0-3.3-2.6-5.9-5.9-5.9h-70.4c-3.3,0-5.9,2.6-5.9,5.9v8.4"/>
	<path class="clickpath2" d="M106.6,188.8v8.4c0,3.3,2.6,5.9,5.9,5.9h108.4c3.3,0,5.9-2.6,5.9-5.9v-8.4v-68.3"/>
	<line class="clickpath2" x1="166" y1="74.6" x2="166" y2="121.6"/>
	<line class="clickpath2" x1="167" y1="189.1" x2="167" y2="220.8"/>
	<g>
		<path class="clickpath1" d="M127.3,179.9H85.9c-3,0-5.4-2.4-5.4-5.4v-38.8c0-3,2.4-5.4,5.4-5.4h41.4c3,0,5.4,2.4,5.4,5.4v38.8
			C132.7,177.5,130.3,179.9,127.3,179.9"/>
		<line class="clickpath2" x1="86" y1="157.1" x2="106.6" y2="157.1"/>
		<line class="clickpath2" x1="86" y1="164.6" x2="106.6" y2="164.6"/>
		<line class="clickpath2" x1="86" y1="172.1" x2="106.6" y2="172.1"/>
		<path class="clickpath7" d="M126.9,150.6H86.3c-1.3,0-2.3-1-2.3-2.3v-12.2c0-1.3,1-2.3,2.3-2.3h40.6c1.3,0,2.3,1,2.3,2.3v12.2
			C129.1,149.6,128.1,150.6,126.9,150.6"/>
		<path class="clickpath7" d="M127.7,173.9h-13.8c-0.8,0-1.5-0.7-1.5-1.5v-15.8c0-0.8,0.7-1.5,1.5-1.5h13.8c0.8,0,1.5,0.7,1.5,1.5v15.8
			C129.1,173.3,128.5,173.9,127.7,173.9"/>
	</g>
	<line class="clickpath2" x1="207.7" y1="268.9" x2="207.7" y2="279.3"/>
	<path class="clickpath3" d="M125.1,260.4l-4.6,2.4c-0.7,0.4-1.6-0.2-1.5-1.1l0.9-5.1c0.1-0.3-0.1-0.7-0.3-0.9l-3.7-3.6
		c-0.6-0.6-0.3-1.6,0.6-1.7l5.1-0.7c0.3,0,0.6-0.3,0.8-0.6l2.3-4.6c0.4-0.8,1.5-0.8,1.8,0l2.3,4.6c0.1,0.3,0.4,0.5,0.8,0.6l5.1,0.7
		c0.8,0.1,1.2,1.1,0.6,1.7l-3.7,3.6c-0.2,0.2-0.4,0.6-0.3,0.9l0.9,5.1c0.1,0.8-0.7,1.5-1.5,1.1l-4.6-2.4
		C125.7,260.2,125.4,260.2,125.1,260.4"/>
</g>
<path class="clickpath1" d="M145.9,179.9h41.4c3,0,5.4-2.4,5.4-5.4v-38.8c0-3-2.4-5.4-5.4-5.4h-41.4c-3,0-5.4,2.4-5.4,5.4v38.8
	C140.4,177.5,142.9,179.9,145.9,179.9"/>
<line class="clickpath2" x1="187.2" y1="157.1" x2="166.6" y2="157.1"/>
<line class="clickpath2" x1="187.2" y1="164.6" x2="166.6" y2="164.6"/>
<line class="clickpath2" x1="187.2" y1="172.1" x2="166.6" y2="172.1"/>
<path class="clickpath7" d="M146.3,150.6h40.6c1.3,0,2.3-1,2.3-2.3v-12.2c0-1.3-1-2.3-2.3-2.3h-40.6c-1.3,0-2.3,1-2.3,2.3v12.2
	C144,149.6,145,150.6,146.3,150.6"/>
<path class="clickpath7" d="M145.5,173.9h13.8c0.8,0,1.5-0.7,1.5-1.5v-15.8c0-0.8-0.7-1.5-1.5-1.5h-13.8c-0.8,0-1.5,0.7-1.5,1.5v15.8
	C144,173.3,144.6,173.9,145.5,173.9"/>
<path class="clickpath3" d="M207.3,303.5l-4.6,2.4c-0.7,0.4-1.6-0.2-1.5-1.1l0.9-5.1c0.1-0.3-0.1-0.7-0.3-0.9l-3.7-3.6
	c-0.6-0.6-0.3-1.6,0.6-1.7l5.1-0.7c0.3,0,0.6-0.3,0.8-0.6l2.3-4.6c0.4-0.8,1.5-0.8,1.8,0l2.3,4.6c0.1,0.3,0.4,0.5,0.8,0.6l5.1,0.7
	c0.8,0.1,1.2,1.1,0.6,1.7l-3.7,3.6c-0.2,0.2-0.3,0.6-0.3,0.9l0.9,5.1c0.1,0.8-0.7,1.5-1.5,1.1l-4.6-2.4
	C207.9,303.3,207.6,303.3,207.3,303.5"/>
<path class="clickpath4" d="M184.9,249.1c0-0.1-0.1-0.3-0.1-0.4c0-0.6,0.5-1,1.1-1c0.5,0,0.9,0.3,1,0.7l1.8,5.7l1.9-5.7
	c0.2-0.5,0.5-0.8,1-0.8h0.2c0.5,0,0.9,0.3,1,0.8l1.9,5.7l1.8-5.7c0.1-0.4,0.5-0.7,1-0.7c0.6,0,1,0.4,1,1c0,0.1,0,0.3-0.1,0.4
	l-2.6,7.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2c-0.5,0-0.9-0.3-1-0.8l-1.8-5.3l-1.8,5.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2
	c-0.5,0-0.9-0.3-1.1-0.8L184.9,249.1z"/>
<path class="clickpath4" d="M199.4,249.1c0-0.1-0.1-0.3-0.1-0.4c0-0.6,0.5-1,1.1-1c0.5,0,0.9,0.3,1,0.7l1.8,5.7l1.9-5.7
	c0.2-0.5,0.5-0.8,1-0.8h0.2c0.5,0,0.9,0.3,1,0.8l1.9,5.7l1.8-5.7c0.1-0.4,0.5-0.7,1-0.7c0.6,0,1,0.4,1,1c0,0.1,0,0.3-0.1,0.4
	l-2.6,7.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2c-0.5,0-0.9-0.3-1.1-0.8l-1.8-5.3l-1.8,5.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2
	c-0.5,0-0.9-0.3-1.1-0.8L199.4,249.1z"/>
<path class="clickpath4" d="M213.9,249.1c0-0.1-0.1-0.3-0.1-0.4c0-0.6,0.5-1,1.1-1c0.5,0,0.9,0.3,1,0.7l1.8,5.7l1.9-5.7
	c0.2-0.5,0.5-0.8,1-0.8h0.2c0.5,0,0.9,0.3,1,0.8l1.9,5.7l1.8-5.7c0.1-0.4,0.5-0.7,1-0.7c0.6,0,1,0.4,1,1c0,0.1,0,0.3-0.1,0.4
	l-2.6,7.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2c-0.5,0-0.9-0.3-1.1-0.8l-1.8-5.3l-1.8,5.3c-0.2,0.5-0.6,0.8-1.1,0.8h-0.2
	c-0.5,0-0.9-0.3-1.1-0.8L213.9,249.1z"/>
<path class="clickpath4" d="M228.5,254.9c0.7,0,1.2,0.5,1.2,1.1v0c0,0.6-0.5,1.1-1.2,1.1c-0.7,0-1.2-0.5-1.2-1.1v0
	C227.4,255.4,227.9,254.9,228.5,254.9"/>
<path class="clickpath16 line1" d="M274.3,74.8v8.4c0,3.3-2.6,5.9-5.9,5.9H166c0,0,0,32.5,0,32.5"/>
<path class="clickpath16 line2" d="M167,189.1v31.8l0,0.3h34.8c3.3,0,5.9,2.6,5.9,5.9v8.4"/>
<line class="clickpath16 line3" x1="207.7" y1="268.9" x2="207.7" y2="279.3"/>
</svg>
