<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="127.1" y1="96.7" x2="148" y2="96.7"/>
<polyline fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="47.3,96.7
	82.7,96.7 90.2,96.7 116.8,96.7 "/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="67.8,114.3
	93.9,114.3 101.4,114.3 146.8,114.3 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="47.3" y1="114.3" x2="57.5" y2="114.3"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="113.3" y1="79" x2="157.4" y2="79"/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="47.3,79 69.4,79
	77,79 103.1,79 "/>
<polyline fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="47.3,131.9
	53.7,131.9 61.3,131.9 71.1,131.9 78.6,131.9 99.3,131.9 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="109.4" y1="131.9" x2="120.9" y2="131.9"/>
<polyline fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="68.5,149.6
	99.2,149.6 107,149.6 113.5,149.6 "/>
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="47.3" y1="149.6" x2="58.2" y2="149.6"/>
<circle fill="#F48220" cx="157.4" cy="96.7" r="2.4"/>
<circle fill="#FAB216" cx="129.1" cy="131.9" r="2.4"/>
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="123.8" y1="149.6" x2="137.8" y2="149.6"/>
<path fill="#F48220" d="M196.1,79.5L196.1,79.5c2.7,1.9,3.4,5.6,1.5,8.4l-2.1,2.9l0,0l-9.8-6.9l0,0l2-2.9
	C189.6,78.2,193.4,77.6,196.1,79.5L196.1,79.5z"/>
<path fill="#343434" d="M145.9,145.3l5.3,3.7l-2.8,2.1c-1,0.7-2.4,0.5-3.1-0.5c-0.4-0.6-0.5-1.3-0.3-2L145.9,145.3z"/>
<rect x="186" y="83.7" transform="matrix(0.5746 -0.8185 0.8185 0.5746 6.9373 192.8087)" fill="#8C8E90" width="5.8" height="12.1"/>
<rect x="140" y="110.7" transform="matrix(0.5746 -0.8185 0.8185 0.5746 -23.1705 188.7856)" fill="#FAB216" width="60" height="12.1"/>
<rect x="142.4" y="115.4" transform="matrix(0.5746 -0.8185 0.8185 0.5746 -23.5438 191.531)" fill="#F48220" width="60" height="6"/>
<polygon fill="#FCCF06" points="147.8,137.8 145.9,145.3 151.2,149.1 157.7,144.7 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M173.1,202.8
	c-35.8,23.8-82.4,23.8-118.2,0"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M54.9,25.3
	c35.8-23.8,82.4-23.8,118.2,0"/>
<path fill="#F48220" d="M35.6,41.3c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C37.4,42.5,36.4,42.1,35.6,41.3z"/>
<path fill="#FCCF06" d="M186.7,41.3c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C188.5,42.5,187.5,42.1,186.7,41.3z"/>
<path fill="#FAB216" d="M35.7,192.4c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C37.4,193.5,36.4,193.1,35.7,192.4z"/>
<path fill="#F48220" d="M186.6,192.4c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C188.4,193.5,187.4,193.1,186.6,192.4z"/>
</svg>
