<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 187 189" enable-background="new 0 0 187 189" xml:space="preserve">
<g>
	<defs>
		<path id="DeployID_1_" d="M21.3,145.8L43.4,168c1.1,1.1,1.1,3,0,4.1l-16.3,16.3c-1.1,1.1-3,1.1-4.1,0L0.9,166.2c-1.1-1.1-1.1-3,0-4.1
			l16.3-16.3C18.4,144.7,20.2,144.7,21.3,145.8"/>
	</defs>
	<clipPath id="DeployID_2_">
		<use xlink:href="#DeployID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="-4.9" y="140" clip-path="url(#DeployID_2_)" fill="#F48220" width="54.2" height="54.2"/>
</g>
<g>
	<defs>
		<rect id="DeployID_3_" x="142.2" y="144.2" width="44.2" height="44.2"/>
	</defs>
	<clipPath id="DeployID_4_">
		<use xlink:href="#DeployID_3_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#DeployID_4_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="DeployID_5_" x="142" y="144" width="45" height="45"/>
			</defs>
			<clipPath id="DeployID_6_">
				<use xlink:href="#DeployID_5_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#DeployID_6_)">
				<defs>
					<path id="DeployID_7_" d="M143.1,167.1l22.1-22.1c1.1-1.1,3-1.1,4.1,0l16.3,16.3c1.1,1.1,1.1,3,0,4.1l-22.1,22.1
						c-1.1,1.1-3,1.1-4.1,0l-16.3-16.3C142,170.1,142,168.3,143.1,167.1"/>
				</defs>
				<clipPath id="DeployID_8_">
					<use xlink:href="#DeployID_7_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#DeployID_8_)">
					<defs>
						<rect id="DeployID_9_" x="142" y="144" width="45" height="45"/>
					</defs>
					<clipPath id="DeployID_10_">
						<use xlink:href="#DeployID_9_"  overflow="visible"/>
					</clipPath>
					<rect x="137.2" y="139.2" clip-path="url(#DeployID_10_)" fill="#FCCF06" width="54.2" height="54.2"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<rect id="DeployID_11_" x="141.6" y="1.2" width="44.2" height="44.2"/>
	</defs>
	<clipPath id="DeployID_12_">
		<use xlink:href="#DeployID_11_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#DeployID_12_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="DeployID_13_" x="141" y="1" width="45" height="45"/>
			</defs>
			<clipPath id="DeployID_14_">
				<use xlink:href="#DeployID_13_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#DeployID_14_)">
				<defs>
					<path id="DeployID_15_" d="M164.5,44.6l-22.1-22.1c-1.1-1.1-1.1-3,0-4.1l16.3-16.3c1.1-1.1,3-1.1,4.1,0l22.1,22.1
						c1.1,1.1,1.1,3,0,4.1l-16.3,16.3C167.5,45.7,165.6,45.7,164.5,44.6"/>
				</defs>
				<clipPath id="DeployID_16_">
					<use xlink:href="#DeployID_15_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#DeployID_16_)">
					<defs>
						<rect id="DeployID_17_" x="141" y="1" width="45" height="45"/>
					</defs>
					<clipPath id="DeployID_18_">
						<use xlink:href="#DeployID_17_"  overflow="visible"/>
					</clipPath>
					<rect x="136.6" y="-3.8" clip-path="url(#DeployID_18_)" fill="#FCCF06" width="54.2" height="54.2"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<path id="DeployID_19_" d="M43.4,21.2L21.2,43.4c-1.1,1.1-3,1.1-4.1,0L0.8,27.1c-1.1-1.1-1.1-3,0-4.1L23,0.8c1.1-1.1,3-1.1,4.1,0
			l16.3,16.3C44.5,18.3,44.5,20.1,43.4,21.2"/>
	</defs>
	<clipPath id="DeployID_20_">
		<use xlink:href="#DeployID_19_"  overflow="visible"/>
	</clipPath>
	<rect x="-5" y="-5" clip-path="url(#DeployID_20_)" fill="#FAB216" width="54.2" height="54.2"/>
</g>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	30.9,48.8 30.9,32.6 47.2,32.6 "/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M46.1,124.8
	c-11.3-18.2-11.3-41.4,0-59.5"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M123.6,143
	c-18.1,11.3-41.2,11.3-59.3,0.1"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M141.8,65.4
	c11.2,18.1,11.2,41.2,0,59.3"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M64.3,47.1
	c18.1-11.3,41.3-11.3,59.4,0.1"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="59.1" y1="60.8" x2="30.9" y2="32.6"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	47.3,156.9 31,156.9 31,140.7 "/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="59.2" y1="128.7" x2="31" y2="156.9"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	155.4,140.6 155.4,156.9 139.1,156.9 "/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="127.1" y1="128.6" x2="155.4" y2="156.9"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	139,32.5 155.3,32.5 155.3,48.8 "/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="127.1" y1="60.7" x2="155.3" y2="32.5"/>
<g>
	<defs>
		<rect id="DeployID_21_" x="66.7" y="74.4" width="53.7" height="23.8"/>
	</defs>
	<clipPath id="DeployID_22_">
		<use xlink:href="#DeployID_21_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#DeployID_22_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="DeployID_23_" x="66" y="74" width="55" height="25"/>
			</defs>
			<clipPath id="DeployID_24_">
				<use xlink:href="#DeployID_23_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#DeployID_24_)">
				<defs>
					<path id="DeployID_25_" d="M116.7,98.2H70.5c-2.1,0-3.7-1.7-3.7-3.7V78.1c0-2,1.7-3.7,3.7-3.7h46.2c2.1,0,3.7,1.7,3.7,3.7v16.4
						C120.4,96.5,118.8,98.2,116.7,98.2"/>
				</defs>
				<clipPath id="DeployID_26_">
					<use xlink:href="#DeployID_25_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#DeployID_26_)">
					<defs>
						<rect id="DeployID_27_" x="66" y="74" width="55" height="25"/>
					</defs>
					<clipPath id="DeployID_28_">
						<use xlink:href="#DeployID_27_"  overflow="visible"/>
					</clipPath>
					<rect x="61.7" y="69.4" clip-path="url(#DeployID_28_)" fill="#F48220" width="63.7" height="33.8"/>
				</g>
			</g>
		</g>
	</g>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="68.6" y1="105" x2="117.4" y2="105"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="68.6" y1="112" x2="82" y2="112"/>
</svg>
