<svg width="115px" height="422px" viewBox="0 0 115 422" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 41.2 (35397) - http://www.bohemiancoding.com/sketch -->
    <title>Group 2</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <circle id="path-1" cx="107" cy="8" r="8"></circle>
        <mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-1"></use>
        </mask>
        <ellipse id="path-3" cx="8" cy="279" rx="8" ry="8"></ellipse>
        <mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-3"></use>
        </mask>
        <ellipse id="path-5" cx="91" cy="414" rx="8" ry="8"></ellipse>
        <mask id="mask-6" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="16" height="16" fill="white">
            <use xlink:href="#path-5"></use>
        </mask>
    </defs>
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Publisher-Network" transform="translate(-50.000000, -580.000000)" stroke-width="4" fill="#FFFFFF">
            <g id="Group-2" transform="translate(50.000000, 580.000000)">
                <use id="Oval-Copy-6" stroke="#FFD100" mask="url(#mask-2)" xlink:href="#path-1"></use>
                <use id="Oval-Copy-8" stroke="#FFD100" mask="url(#mask-4)" xlink:href="#path-3"></use>
                <use id="Oval-Copy-16" stroke="#FF8403" mask="url(#mask-6)" xlink:href="#path-5"></use>
            </g>
        </g>
    </g>
</svg>
