<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<circle fill="#FAB216" cx="91.8" cy="111.2" r="16.7"/>
<path fill="#F48220" d="M108.4,134.4l1.4-1.6l-7.3-8.3l-0.3-0.4c-6.1,4.9-14.7,4.9-20.8,0l-8,9.1c-2.1,2.4-3.3,5.5-3.4,8.7v4.8h33.5
	C103.7,142.2,105.4,137.8,108.4,134.4z"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M121.3,69.8
	c-3.9-2-8.6-0.8-11.1,2.8c-2.6-4-8-5-11.9-2.4c-3.7,2.4-4.9,7.3-2.8,11.2c2,4,7.8,8.4,12.1,11.4c1,0.7,1.9,1.3,2.7,1.8
	c0.8-0.5,1.7-1.1,2.7-1.8c4.3-3,10-7.4,12.1-11.4C127.2,77.1,125.6,71.9,121.3,69.8C121.3,69.8,121.3,69.8,121.3,69.8z"/>
<circle fill="#FCCF06" cx="133.6" cy="110.2" r="20.2"/>
<path fill="#FAB216" d="M121,125.9c7.4,5.9,17.9,5.9,25.2,0l0.4,0.5l9.4,10.6c2.6,2.9,4,6.6,4,10.5v5.8h-52.7v-5.8
	c0-3.9,1.4-7.6,4-10.5L121,125.9z"/>
<circle fill="#F48220" cx="114.2" cy="43.7" r="4"/>
<circle fill="#FAB216" cx="114.2" cy="27.7" r="4"/>
<circle fill="#FCCF06" cx="114.2" cy="11.7" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -27.2984 64.3632)" fill="#F48220" cx="64" cy="65.1" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -22.6121 53.051)" fill="#FAB216" cx="52.7" cy="53.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -17.9287 41.7377)" fill="#FCCF06" cx="41.4" cy="42.5" rx="4" ry="4"/>
<circle fill="#F48220" cx="43.7" cy="115.8" r="4"/>
<circle fill="#FAB216" cx="27.7" cy="115.8" r="4"/>
<circle fill="#FCCF06" cx="11.7" cy="115.8" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -98.2691 94.6584)" fill="#F48220" cx="65.1" cy="166" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -109.5854 89.9821)" fill="#FAB216" cx="53.8" cy="177.3" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -120.8946 85.2887)" fill="#FCCF06" cx="42.5" cy="188.6" rx="4" ry="4"/>
<circle fill="#F48220" cx="115.8" cy="186.3" r="4"/>
<circle fill="#FAB216" cx="115.8" cy="202.3" r="4"/>
<circle fill="#FCCF06" cx="115.8" cy="218.3" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -69.2129 168.638)" fill="#F48220" cx="169" cy="167.9" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -73.8991 179.9502)" fill="#FAB216" cx="180.3" cy="179.2" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -78.5854 191.2565)" fill="#FCCF06" cx="191.6" cy="190.5" rx="4" ry="4"/>
<circle fill="#F48220" cx="186.3" cy="114.2" r="4"/>
<circle fill="#FAB216" cx="202.3" cy="114.2" r="4"/>
<circle fill="#FCCF06" cx="218.3" cy="114.2" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 2.9997 135.341)" fill="#F48220" cx="164.9" cy="64" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 14.316 140.0173)" fill="#FAB216" cx="176.2" cy="52.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 25.6252 144.7106)" fill="#FCCF06" cx="187.5" cy="41.4" rx="4" ry="4"/>
</svg>
