<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 270 270" enable-background="new 0 0 270 270" xml:space="preserve">
<circle fill="#464646" cx="135" cy="135" r="135"/>
<circle fill="#FAB216" cx="111.8" cy="131.2" r="16.6"/>
<path fill="#F48220" d="M128.4,154.4l1.4-1.6l-7.3-8.3l-0.3-0.4c-6.1,4.9-14.7,4.9-20.8,0l-8,9.1c-2.1,2.4-3.3,5.5-3.4,8.7v4.8h33.5
	C123.7,162.2,125.4,157.8,128.4,154.4z"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M141.3,89.8
	c-3.9-2-8.6-0.8-11.1,2.8c-2.7-3.9-8.1-4.9-12-2.2c-3.6,2.4-4.8,7.1-2.8,11c2,4,7.8,8.4,12.1,11.4c1,0.7,1.9,1.3,2.7,1.8
	c0.8-0.5,1.7-1.1,2.7-1.8c4.3-3,10-7.4,12.1-11.4C147.2,97.1,145.6,91.9,141.3,89.8z"/>
<circle fill="#FCCF06" cx="153.6" cy="130.2" r="20.2"/>
<path fill="#FAB216" d="M141,145.9c7.4,5.9,17.9,5.9,25.2,0l0.4,0.5l9.4,10.6c2.6,2.9,4,6.6,4,10.5v5.8h-52.7v-5.8
	c0-3.9,1.4-7.6,4-10.5L141,145.9z"/>
<circle fill="#F48220" cx="134.2" cy="63.7" r="4"/>
<circle fill="#FAB216" cx="134.2" cy="47.7" r="4"/>
<circle fill="#FCCF06" cx="134.2" cy="31.7" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -35.5814 84.3662)" fill="#F48220" cx="84" cy="85.1" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -30.8981 73.047)" fill="#FAB216" cx="72.7" cy="73.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -26.2118 61.7407)" fill="#FCCF06" cx="61.4" cy="62.5" rx="4" ry="4"/>
<circle fill="#F48220" cx="63.7" cy="135.8" r="4"/>
<circle fill="#FAB216" cx="47.7" cy="135.8" r="4"/>
<circle fill="#FCCF06" cx="31.7" cy="135.8" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.5521 114.6614)" fill="#F48220" cx="85.1" cy="186" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -117.8714 109.9781)" fill="#FAB216" cx="73.8" cy="197.3" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -129.1776 105.2918)" fill="#FCCF06" cx="62.5" cy="208.6" rx="4" ry="4"/>
<circle fill="#F48220" cx="135.8" cy="206.3" r="4"/>
<circle fill="#FAB216" cx="135.8" cy="222.3" r="4"/>
<circle fill="#FCCF06" cx="135.8" cy="238.3" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -77.4988 188.634)" fill="#F48220" cx="189" cy="187.9" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -82.1822 199.9532)" fill="#FAB216" cx="200.3" cy="199.2" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -86.8685 211.2595)" fill="#FCCF06" cx="211.6" cy="210.5" rx="4" ry="4"/>
<circle fill="#F48220" cx="206.3" cy="134.2" r="4"/>
<circle fill="#FAB216" cx="222.3" cy="134.2" r="4"/>
<circle fill="#FCCF06" cx="238.3" cy="134.2" r="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -5.2833 155.344)" fill="#F48220" cx="184.9" cy="84" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 6.033 160.0203)" fill="#FAB216" cx="196.2" cy="72.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 17.3393 164.7066)" fill="#FCCF06" cx="207.5" cy="61.4" rx="4" ry="4"/>
</svg>
