<svg width="91px" height="183px" viewBox="0 0 91 183" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Case-Study-Teensafe" transform="translate(-1282.000000, -2431.000000)" stroke="#FFFFFF">
            <g id="Group-3" transform="translate(1283.000000, 2433.000000)">
                <circle id="Oval-Copy-2" stroke-width="2" cx="84" cy="118" r="5"></circle>
                <circle id="Oval-Copy-4" stroke-width="2" cx="5" cy="175" r="5"></circle>
                <circle id="Oval-Copy-5" stroke-width="4" cx="34" cy="10" r="10"></circle>
            </g>
        </g>
    </g>
</svg>
