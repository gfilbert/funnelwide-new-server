<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="#FFFFFF" d="M116.6,157.4c-23.4,0-42.4-19-42.4-42.4c0-23.4,19-42.4,42.4-42.4c23.4,0,42.3,18.9,42.4,42.3
	C158.9,138.4,140,157.4,116.6,157.4z"/>
<path fill="#FCCF06" d="M116.6,77.7c20.6,0,37.4,16.7,37.4,37.4s-16.7,37.4-37.4,37.4S79.2,135.7,79.2,115c0,0,0,0,0,0
	C79.2,94.4,96,77.7,116.6,77.7 M116.6,67.6c-26.2,0-47.4,21.2-47.4,47.3s21.2,47.4,47.3,47.4s47.4-21.2,47.4-47.3c0,0,0,0,0,0
	C163.9,88.8,142.7,67.6,116.6,67.6L116.6,67.6z"/>
<path fill="#F48220" d="M30.4,225.7L5.8,201c-1.3-1.3-1.3-3.3,0-4.6l18.2-18.2c1.3-1.3,3.3-1.3,4.6,0l24.6,24.6
	c1.3,1.3,1.3,3.3,0,4.6L35,225.7C33.7,226.9,31.7,226.9,30.4,225.7z"/>
<path fill="#FCCF06" d="M227.4,200.7l-24.6,24.6c-1.3,1.3-3.3,1.3-4.6,0L180,207.2c-1.3-1.3-1.3-3.3,0-4.6l24.6-24.6
	c1.3-1.3,3.3-1.3,4.6,0l18.2,18.2C228.7,197.4,228.7,199.5,227.4,200.7z"/>
<path fill="#FCCF06" d="M202.5,3.8l24.6,24.6c1.3,1.3,1.3,3.3,0,4.6L209,51.1c-1.3,1.3-3.3,1.3-4.6,0l-24.6-24.6
	c-1.3-1.3-1.3-3.3,0-4.6l18.2-18.2C199.2,2.5,201.2,2.5,202.5,3.8z"/>
<path fill="#FAB216" d="M5.5,28.7L30.1,4c1.3-1.3,3.3-1.3,4.6,0l18.2,18.2c1.3,1.3,1.3,3.3,0,4.6L28.2,51.4c-1.3,1.3-3.3,1.3-4.6,0
	L5.5,33.2C4.2,32,4.2,30,5.5,28.7z"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="36.1,52.8
	36.1,34.7 54.2,34.7 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M48,157.7
	c-16.2-26.2-16.2-59.3,0-85.5"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M159.6,183.8
	c-26.1,16.2-59.2,16.2-85.3,0.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M185.8,72.3
	c16.1,26.1,16.1,59.1-0.1,85.3"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M74.1,46
	c26.2-16.2,59.4-16.2,85.5,0.1"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="67.6" y1="66.1" x2="36.1" y2="34.7"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="54.3,195.5
	36.2,195.5 36.2,177.4 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="67.6" y1="164.1" x2="36.2" y2="195.5"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="197.1,177.3
	197.1,195.4 178.9,195.4 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="165.6" y1="164" x2="197.1" y2="195.4"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="178.8,34.6
	197,34.6 197,52.7 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="165.5" y1="66" x2="197" y2="34.6"/>
<circle fill="#FCCF06" cx="12" cy="114.8" r="5.7"/>
<circle fill="#FAB216" cx="221.7" cy="115" r="5.7"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M108.3,123.3
	c0,4.6,3.7,8.3,8.3,8.3s8.3-3.7,8.3-8.3c0-5.9-5.1-7.7-8.3-8.3s-8.3-2.5-8.3-8.3c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="116.6" y1="98.5" x2="116.6" y2="92.5"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="116.6" y1="137.6" x2="116.6" y2="131.6"/>
</svg>
