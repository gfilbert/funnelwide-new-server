<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<path fill="#F48220" d="M39.2,214.3l-22.1-22.1c-1.1-1.1-1.1-3,0-4.1l16.3-16.3c1.1-1.1,3-1.1,4.1,0l22.1,22.1c1.1,1.1,1.1,3,0,4.1
	l-16.3,16.3C42.2,215.4,40.4,215.4,39.2,214.3z"/>
<path fill="#FCCF06" d="M216,191.9L193.9,214c-1.1,1.1-3,1.1-4.1,0l-16.3-16.3c-1.1-1.1-1.1-3,0-4.1l22.1-22.1c1.1-1.1,3-1.1,4.1,0
	l16.3,16.3C217.1,189,217.1,190.8,216,191.9z"/>
<path fill="#FCCF06" d="M193.7,15.1l22.1,22.1c1.1,1.1,1.1,3,0,4.1l-16.3,16.3c-1.1,1.1-3,1.1-4.1,0l-22.1-22.1
	c-1.1-1.1-1.1-3,0-4.1l16.3-16.3C190.7,14,192.5,14,193.7,15.1z"/>
<path fill="#FAB216" d="M16.9,37.5L39,15.4c1.1-1.1,3-1.1,4.1,0l16.3,16.3c1.1,1.1,1.1,3,0,4.1L37.3,57.9c-1.1,1.1-3,1.1-4.1,0
	L16.9,41.6C15.7,40.5,15.7,38.6,16.9,37.5z"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="44.3,59.1
	44.3,42.8 60.6,42.8 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M55,153.3
	c-14.6-23.5-14.6-53.2,0-76.8"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M155.1,176.7
	c-23.4,14.5-53.1,14.6-76.6,0.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M178.7,76.7
	c14.5,23.5,14.4,53.1-0.1,76.5"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M78.5,53
	c23.5-14.5,53.3-14.5,76.8,0.1"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="72.6" y1="71.1" x2="44.3" y2="42.8"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="60.7,187.2
	44.4,187.2 44.4,170.9 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="72.6" y1="159" x2="44.4" y2="187.2"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="188.8,170.9
	188.8,187.2 172.5,187.2 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="160.6" y1="158.9" x2="188.8" y2="187.2"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="172.4,42.8
	188.7,42.8 188.7,59.1 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="160.5" y1="71" x2="188.7" y2="42.8"/>
<circle fill="#FCCF06" cx="22.7" cy="114.8" r="5.1"/>
<circle fill="#FAB216" cx="210.9" cy="115" r="5.1"/>
<path fill="#C2C4C6" d="M81.9,105c0.3-1.7,0.7-3.4,1.2-5.1c3.2-8.5,9.3-13.4,18.3-14.7c10.9-1.4,21.2,5.5,24,16.1
	c0.5,2.3,0.7,4.6,0.4,6.9c-0.2,3.1-0.6,6.2-1.2,9.3c-0.9,4.7-2,9.3-3.2,13.9c-1,3.8-2.2,7.5-3.3,11.2c-0.8,2.6-1.5,5.2-2.5,7.6h-0.2
	c-0.3-0.3-0.5-0.6-0.8-0.8c-3.4-2.9-6.7-6-10-9.1c-3.9-3.7-7.6-7.5-11.2-11.5c-2.4-2.6-4.7-5.4-6.8-8.3c-1.9-2.7-3.6-5.4-4.3-8.7
	c-0.2-0.9-0.3-1.8-0.5-2.7C81.7,107.9,81.7,106.4,81.9,105L81.9,105z M113.9,107.5c0.1-5.1-3.9-9.3-9-9.4c-0.1,0-0.3,0-0.4,0
	c-4.9-0.1-9,3.7-9.1,8.6c0,0.2,0,0.4,0,0.6c0.1,5.1,4.4,9.2,9.5,9C109.7,116.2,113.7,112.3,113.9,107.5L113.9,107.5z"/>
<path fill="#404040" d="M127.3,84.7c1,0,2,0.1,2.9,0.3c0.7,0.2,1.4,0.4,2.1,0.6c9,3,14.2,9.3,15.6,18.7c0.8,5.5-0.8,10.3-3.9,14.9
	c-4,5.7-8.4,11.1-13.2,16.1c-2.7,2.9-5.4,5.7-8.3,8.4c-0.2,0.3-0.5,0.5-0.8,0.6c0.7-2.5,1.4-4.9,2.1-7.3c2.2-7.2,3.9-14.6,5.1-22.1
	c0.6-4.1,1.2-8.2,0.5-12.3c-1-6.4-4.5-12.1-9.8-16L119,86c0,0,0-0.1-0.1-0.1c1.1-0.6,2.3-0.9,3.5-1.1c0.4-0.1,0.7-0.1,1.1-0.1
	L127.3,84.7z"/>
</svg>
