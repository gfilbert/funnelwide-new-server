<svg width="56px" height="245px" viewBox="0 0 56 245" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Case-Study-Teensafe" transform="translate(-78.000000, -2386.000000)" stroke="#FFFFFF">
            <g id="Group-2" transform="translate(79.000000, 2388.000000)">
                <circle id="Oval-Copy-3" stroke-width="2" cx="49" cy="237" r="5"></circle>
                <circle id="Oval-Copy-7" stroke-width="2" cx="5" cy="97" r="5"></circle>
                <circle id="Oval-Copy-6" stroke-width="4" cx="39" cy="10" r="10"></circle>
            </g>
        </g>
    </g>
</svg>
