<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="115" y1="88.7" x2="115" y2="75.1"/>
<path fill="none" stroke="#FCCF06" stroke-width="4" stroke-miterlimit="10" d="M127.5,73.7h-25c-1.6,0-3-1.3-3-3v-25
	c0-1.6,1.3-3,3-3h25c1.6,0,3,1.3,3,3v25C130.5,72.4,129.2,73.7,127.5,73.7z"/>
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="88.7" y1="115" x2="75.1" y2="115"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-miterlimit="10" d="M73.7,102.5v25c0,1.6-1.3,3-3,3h-25
	c-1.6,0-3-1.3-3-3v-25c0-1.6,1.3-3,3-3h25C72.4,99.5,73.7,100.9,73.7,102.5z"/>
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="115" y1="141.3" x2="115" y2="154.9"/>
<path fill="none" stroke="#FAB216" stroke-width="4" stroke-miterlimit="10" d="M102.5,156.3h25c1.6,0,3,1.3,3,3v25c0,1.6-1.3,3-3,3
	h-25c-1.6,0-3-1.3-3-3v-25C99.5,157.6,100.8,156.3,102.5,156.3z"/>
<line fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="141.3" y1="115" x2="154.9" y2="115"/>
<path fill="none" stroke="#F48220" stroke-width="4" stroke-miterlimit="10" d="M156.3,127.5v-25c0-1.6,1.3-3,3-3h25
	c1.6,0,3,1.3,3,3v25c0,1.6-1.3,3-3,3h-25C157.6,130.5,156.3,129.2,156.3,127.5z"/>
<path fill="#FAB216" d="M130.9,134.7H99.1c-2.1,0-3.8-1.7-3.8-3.8V99.1c0-2.1,1.7-3.8,3.8-3.8h31.9c2.1,0,3.8,1.7,3.8,3.8v31.9
	C134.7,133,133,134.7,130.9,134.7z"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="102.2" y1="105.5" x2="127.8" y2="105.5"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="102.2" y1="115" x2="127.8" y2="115"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="102.2" y1="124.5" x2="120.9" y2="124.5"/>
<circle fill="#FFFFFF" cx="127" cy="124.5" r="1.9"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M18.1,74
	c10.5-25.2,30.4-45.4,55.6-56.2"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M73.7,210.6
	c-24.4-10.5-44-29.8-54.7-54.1"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M210.9,156.5
	c-10.7,24.3-30.3,43.7-54.7,54.2"/>
<path fill="none" stroke="#343434" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M155.7,17.6
	c25.3,10.7,45.4,30.9,56,56.3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 94.6784 127.8195)" fill="#FAB216" cx="123" cy="7.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 107.3872 154.2083)" fill="#F48220" cx="145" cy="13.5" rx="4.4" ry="4.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 32.0795 97.2607)" fill="#FCCF06" cx="133.4" cy="9.9" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -98.5197 97.8159)" fill="#FAB216" cx="8.7" cy="107.2" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -72.1419 85.1136)" fill="#F48220" cx="14.3" cy="85.3" rx="4.5" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -65.3071 35.9244)" fill="#FCCF06" cx="10.7" cy="96.8" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -128.5344 291.0205)" fill="#FAB216" cx="108" cy="221.6" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 -141.2348 264.6415)" fill="#F48220" cx="86.1" cy="215.9" rx="4.4" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -126.6534 133.321)" fill="#FCCF06" cx="97.6" cy="219.5" rx="3" ry="3"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 64.6703 321.0352)" fill="#FAB216" cx="222.4" cy="122.2" rx="2" ry="2"/>
<ellipse transform="matrix(0.1675 -0.9859 0.9859 0.1675 38.2859 333.7263)" fill="#F48220" cx="216.7" cy="144.2" rx="4.4" ry="4.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -29.2568 194.6614)" fill="#FCCF06" cx="220.3" cy="132.6" rx="3" ry="3"/>
</svg>
