<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 268 268" enable-background="new 0 0 268 268" xml:space="preserve">
<g>
	<defs>
		<path id="DeliverID_1_" d="M133.7,87.8c25.3,0,45.9,20.6,45.9,45.8c0,25.3-20.6,45.8-45.9,45.8c-25.3,0-45.9-20.5-45.9-45.8
			C87.8,108.4,108.4,87.8,133.7,87.8 M133.7,78C102.9,78,78,102.9,78,133.6c0,30.7,24.9,55.6,55.7,55.6c30.8,0,55.7-24.9,55.7-55.6
			C189.4,102.9,164.4,78,133.7,78"/>
	</defs>
	<clipPath id="DeliverID_2_">
		<use xlink:href="#DeliverID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="73" y="73" clip-path="url(#DeliverID_2_)" fill="#FCCF06" width="121.4" height="121.3"/>
</g>
<g>
	<defs>
		<path id="DeliverID_3_" d="M27.8,211.2l29,28.9c1.5,1.5,1.5,3.9,0,5.3l-21.3,21.3c-1.5,1.5-3.9,1.5-5.4,0l-29-28.9
			c-1.5-1.5-1.5-3.9,0-5.3l21.3-21.3C23.9,209.7,26.3,209.7,27.8,211.2"/>
	</defs>
	<clipPath id="DeliverID_4_">
		<use xlink:href="#DeliverID_3_"  overflow="visible"/>
	</clipPath>
	<rect x="-5" y="205.1" clip-path="url(#DeliverID_4_)" fill="#F48220" width="67.9" height="67.8"/>
</g>
<g>
	<defs>
		<rect id="DeliverID_5_" x="209.9" y="209.5" width="57.9" height="57.9"/>
	</defs>
	<clipPath id="DeliverID_6_">
		<use xlink:href="#DeliverID_5_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#DeliverID_6_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="DeliverID_7_" x="209" y="209" width="59" height="59"/>
			</defs>
			<clipPath id="DeliverID_8_">
				<use xlink:href="#DeliverID_7_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#DeliverID_8_)">
				<defs>
					<path id="DeliverID_9_" d="M211,239.6l29-29c1.5-1.5,3.9-1.5,5.4,0l21.4,21.3c1.5,1.5,1.5,3.9,0,5.3l-29,29c-1.5,1.5-3.9,1.5-5.4,0
						L211,244.9C209.5,243.5,209.5,241.1,211,239.6"/>
				</defs>
				<clipPath id="DeliverID_10_">
					<use xlink:href="#DeliverID_9_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#DeliverID_10_)">
					<defs>
						<rect id="DeliverID_11_" x="209" y="209" width="59" height="59"/>
					</defs>
					<clipPath id="DeliverID_12_">
						<use xlink:href="#DeliverID_11_"  overflow="visible"/>
					</clipPath>
					<rect x="204.9" y="204.5" clip-path="url(#DeliverID_12_)" fill="#FCCF06" width="67.9" height="67.9"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<rect id="DeliverID_13_" x="209.6" y="0.2" width="57.9" height="57.9"/>
	</defs>
	<clipPath id="DeliverID_14_">
		<use xlink:href="#DeliverID_13_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#DeliverID_14_)" enable-background="new    ">
		<g>
			<defs>
				<rect id="DeliverID_15_" x="209" width="59" height="59"/>
			</defs>
			<clipPath id="DeliverID_16_">
				<use xlink:href="#DeliverID_15_"  overflow="visible"/>
			</clipPath>
			<g clip-path="url(#DeliverID_16_)">
				<defs>
					<path id="DeliverID_17_" d="M239.7,57l-29-29c-1.5-1.5-1.5-3.9,0-5.3L232,1.3c1.5-1.5,3.9-1.5,5.4,0l29,29c1.5,1.5,1.5,3.9,0,5.3
						L245,57C243.5,58.4,241.1,58.4,239.7,57"/>
				</defs>
				<clipPath id="DeliverID_18_">
					<use xlink:href="#DeliverID_17_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#DeliverID_18_)">
					<defs>
						<rect id="DeliverID_19_" x="209" width="59" height="59"/>
					</defs>
					<clipPath id="DeliverID_20_">
						<use xlink:href="#DeliverID_19_"  overflow="visible"/>
					</clipPath>
					<rect x="204.6" y="-4.8" clip-path="url(#DeliverID_20_)" fill="#FCCF06" width="67.9" height="67.9"/>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<path id="DeliverID_21_" d="M57.8,27.8l-29,29c-1.5,1.5-3.9,1.5-5.4,0L2.1,35.4c-1.5-1.5-1.5-3.9,0-5.3l29-29c1.5-1.5,3.9-1.5,5.4,0
			l21.4,21.3C59.3,23.9,59.3,26.3,57.8,27.8"/>
	</defs>
	<clipPath id="DeliverID_22_">
		<use xlink:href="#DeliverID_21_"  overflow="visible"/>
	</clipPath>
	<rect x="-4" y="-5" clip-path="url(#DeliverID_22_)" fill="#FAB216" width="67.9" height="67.9"/>
</g>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	39,61.6 39,40.3 60.3,40.3 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M53.3,183.9
	c-19.1-30.6-19.1-69.8,0-100.4"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M185.2,214.3
	c-30.6,19-69.7,19.1-100.3,0.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M216,83.5
	c18.9,30.6,18.9,69.6-0.1,100.2"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M83.6,53.2
	c30.7-19,69.9-19,100.6,0.1"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="76" y1="77.2" x2="39" y2="40.3"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	60.3,229.1 39,229.1 39,207.8 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="76" y1="192.1" x2="39" y2="229"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	229.4,207.8 229.4,229.1 208,229.1 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="192.3" y1="192.1" x2="229.3" y2="229"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	206.7,39 228.1,39 228.1,60.3 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="191" y1="75.9" x2="228" y2="39"/>
<g>
	<defs>
		<path id="DeliverID_23_" d="M152,121.6c0,9.5-7.7,17.2-17.2,17.2c-9.5,0-17.2-7.7-17.2-17.2c0-9.5,7.7-17.2,17.2-17.2
			C144.3,104.4,152,112.1,152,121.6"/>
	</defs>
	<clipPath id="DeliverID_24_">
		<use xlink:href="#DeliverID_23_"  overflow="visible"/>
	</clipPath>
	<rect x="112.6" y="99.4" clip-path="url(#DeliverID_24_)" fill="#FAB216" width="44.4" height="44.4"/>
</g>
<g>
	<defs>
		<path id="DeliverID_25_" d="M124.1,134.5c2.9,2.4,6.7,3.8,10.8,3.8c4.1,0,7.8-1.4,10.8-3.8l0.3,0.4l8,9c2.2,2.5,3.4,5.6,3.4,8.9v4.9
			h-44.9v-4.9c0-3.3,1.2-6.5,3.4-8.9L124.1,134.5z"/>
	</defs>
	<clipPath id="DeliverID_26_">
		<use xlink:href="#DeliverID_25_"  overflow="visible"/>
	</clipPath>
	<rect x="107.4" y="129.5" clip-path="url(#DeliverID_26_)" fill="#F48220" width="54.9" height="33.3"/>
</g>
</svg>
