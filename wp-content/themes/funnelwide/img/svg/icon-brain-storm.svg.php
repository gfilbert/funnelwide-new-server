<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 151 187" enable-background="new 0 0 151 187" xml:space="preserve">
<g>
	<defs>
		<path id="brainstormID_1_" d="M68.6,32.8c0-2,1.6-3.6,3.6-3.6c2,0,3.6,1.6,3.6,3.6c0,2-1.6,3.6-3.6,3.6C70.2,36.4,68.6,34.8,68.6,32.8"/>
	</defs>
	<clipPath id="brainstormID_2_">
		<use xlink:href="#brainstormID_1_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_2_)">
		<defs>
			<rect id="brainstormID_3_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_4_">
			<use xlink:href="#brainstormID_3_"  overflow="visible"/>
		</clipPath>
		<rect x="63.6" y="24.2" clip-path="url(#brainstormID_4_)" fill="#F48220" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_5_" d="M68.6,18.4c0-2,1.6-3.6,3.6-3.6c2,0,3.6,1.6,3.6,3.6c0,2-1.6,3.6-3.6,3.6C70.2,22,68.6,20.4,68.6,18.4"/>
	</defs>
	<clipPath id="brainstormID_6_">
		<use xlink:href="#brainstormID_5_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_6_)">
		<defs>
			<rect id="brainstormID_7_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_8_">
			<use xlink:href="#brainstormID_7_"  overflow="visible"/>
		</clipPath>
		<rect x="63.6" y="9.8" clip-path="url(#brainstormID_8_)" fill="#FAB216" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_9_" d="M68.6,4c0-2,1.6-3.6,3.6-3.6c2,0,3.6,1.6,3.6,3.6c0,2-1.6,3.6-3.6,3.6C70.2,7.6,68.6,6,68.6,4"/>
	</defs>
	<clipPath id="brainstormID_10_">
		<use xlink:href="#brainstormID_9_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_10_)">
		<defs>
			<rect id="brainstormID_11_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_12_">
			<use xlink:href="#brainstormID_11_"  overflow="visible"/>
		</clipPath>
		<rect x="63.6" y="-4.6" clip-path="url(#brainstormID_12_)" fill="#FCCF06" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_13_" d="M98,39.1c1.1-1.7,3.3-2.2,5-1.1c1.7,1.1,2.2,3.3,1.1,5c-1.1,1.7-3.3,2.2-5,1.1
			C97.4,43.1,96.9,40.8,98,39.1"/>
	</defs>
	<clipPath id="brainstormID_14_">
		<use xlink:href="#brainstormID_13_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_14_)">
		<defs>
			<rect id="brainstormID_15_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_16_">
			<use xlink:href="#brainstormID_15_"  overflow="visible"/>
		</clipPath>
		<rect x="92.4" y="32.5" clip-path="url(#brainstormID_16_)" fill="#FCCF06" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_17_" d="M105.6,26.9c1.1-1.7,3.3-2.2,5-1.1c1.7,1.1,2.2,3.3,1.1,5c-1.1,1.7-3.3,2.2-5,1.1
			C105,30.8,104.5,28.6,105.6,26.9"/>
	</defs>
	<clipPath id="brainstormID_18_">
		<use xlink:href="#brainstormID_17_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_18_)">
		<defs>
			<rect id="brainstormID_19_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_20_">
			<use xlink:href="#brainstormID_19_"  overflow="visible"/>
		</clipPath>
		<rect x="100" y="20.2" clip-path="url(#brainstormID_20_)" fill="#F48220" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_21_" d="M113.2,14.7c1.1-1.7,3.3-2.2,5-1.1c1.7,1.1,2.2,3.3,1.1,5c-1.1,1.7-3.3,2.2-5,1.1
			C112.7,18.6,112.2,16.4,113.2,14.7"/>
	</defs>
	<clipPath id="brainstormID_22_">
		<use xlink:href="#brainstormID_21_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_22_)">
		<defs>
			<rect id="brainstormID_23_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_24_">
			<use xlink:href="#brainstormID_23_"  overflow="visible"/>
		</clipPath>
		<rect x="107.7" y="8" clip-path="url(#brainstormID_24_)" fill="#FAB216" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_25_" d="M119.5,60.1c1.8-0.9,3.9-0.1,4.8,1.7c0.9,1.8,0.1,3.9-1.7,4.8c-1.8,0.9-3.9,0.1-4.8-1.7
			C116.9,63.1,117.7,60.9,119.5,60.1"/>
	</defs>
	<clipPath id="brainstormID_26_">
		<use xlink:href="#brainstormID_25_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_26_)">
		<defs>
			<rect id="brainstormID_27_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_28_">
			<use xlink:href="#brainstormID_27_"  overflow="visible"/>
		</clipPath>
		<rect x="112.4" y="54.7" clip-path="url(#brainstormID_28_)" fill="#FCCF06" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_29_" d="M132.4,53.8c1.8-0.9,3.9-0.1,4.8,1.7c0.9,1.8,0.1,3.9-1.7,4.8c-1.8,0.9-3.9,0.1-4.8-1.7
			C129.9,56.8,130.6,54.6,132.4,53.8"/>
	</defs>
	<clipPath id="brainstormID_30_">
		<use xlink:href="#brainstormID_29_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_30_)">
		<defs>
			<rect id="brainstormID_31_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_32_">
			<use xlink:href="#brainstormID_31_"  overflow="visible"/>
		</clipPath>
		<rect x="125.4" y="48.4" clip-path="url(#brainstormID_32_)" fill="#FAB216" width="17.2" height="17.2"/>
	</g>
</g>
<g>
	<defs>
		<rect id="brainstormID_33_" x="143.3" y="47.1" width="7.2" height="7.2"/>
	</defs>
	<clipPath id="brainstormID_34_">
		<use xlink:href="#brainstormID_33_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_34_)">
		<defs>
			<rect id="brainstormID_35_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_36_">
			<use xlink:href="#brainstormID_35_"  overflow="visible"/>
		</clipPath>
		<g clip-path="url(#brainstormID_36_)" enable-background="new    ">
			<g>
				<defs>
					<rect id="brainstormID_37_" x="143" y="47" width="8" height="8"/>
				</defs>
				<clipPath id="brainstormID_38_">
					<use xlink:href="#brainstormID_37_"  overflow="visible"/>
				</clipPath>
				<g clip-path="url(#brainstormID_38_)">
					<defs>
						<path id="brainstormID_39_" d="M145.4,47.4c1.8-0.9,3.9-0.1,4.8,1.7c0.9,1.8,0.1,3.9-1.7,4.8c-1.8,0.9-3.9,0.1-4.8-1.7
							C142.8,50.5,143.6,48.3,145.4,47.4"/>
					</defs>
					<clipPath id="brainstormID_40_">
						<use xlink:href="#brainstormID_39_"  overflow="visible"/>
					</clipPath>
					<g clip-path="url(#brainstormID_40_)">
						<defs>
							<rect id="brainstormID_41_" x="143" y="47" width="8" height="8"/>
						</defs>
						<clipPath id="brainstormID_42_">
							<use xlink:href="#brainstormID_41_"  overflow="visible"/>
						</clipPath>
						<rect x="138.3" y="42.1" clip-path="url(#brainstormID_42_)" fill="#F48220" width="17.2" height="17.2"/>
					</g>
				</g>
			</g>
		</g>
	</g>
</g>
<g>
	<defs>
		<rect id="brainstormID_43_" x="-28" y="-5" width="207" height="207"/>
	</defs>
	<clipPath id="brainstormID_44_">
		<use xlink:href="#brainstormID_43_"  overflow="visible"/>
	</clipPath>
	
		<line clip-path="url(#brainstormID_44_)" fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="39.2" y1="94.3" x2="39.2" y2="118.8"/>
	
		<line clip-path="url(#brainstormID_44_)" fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="95.9" y1="163.2" x2="96.2" y2="184.4"/>
	
		<path clip-path="url(#brainstormID_44_)" fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
		M57,46.9c3.6-1.1,7.5-1.8,11.4-2c24.5-1,44.9,17.5,46.9,41.4c0.2,2.4,0.7,4.7,1.5,6.9l10,26.9c0.6,1.5-0.5,3.1-2.1,3.1l-8.8,0.1
		l0.4,26.4c0.1,7.2-5.6,13.1-12.8,13.2l-18.9,0.2"/>
	
		<path clip-path="url(#brainstormID_44_)" fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
		M49,184.9l-0.2-17.5c-0.1-5.2-1.3-10.4-3.7-15"/>
</g>
<g>
	<defs>
		<path id="brainstormID_45_" d="M70.1,95.4H8.3c-2.3,0-4.2-1.9-4.2-4.2V64.5c0-2.3,1.9-4.2,4.2-4.2h61.8c2.3,0,4.2,1.9,4.2,4.2v26.7
			C74.3,93.5,72.4,95.4,70.1,95.4"/>
	</defs>
	<clipPath id="brainstormID_46_">
		<use xlink:href="#brainstormID_45_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_46_)">
		<defs>
			<rect id="brainstormID_47_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_48_">
			<use xlink:href="#brainstormID_47_"  overflow="visible"/>
		</clipPath>
		<rect x="-0.9" y="55.3" clip-path="url(#brainstormID_48_)" fill="#FCCF06" width="80.2" height="45.1"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_49_" d="M43.4,77.8c0,2.3-1.9,4.2-4.2,4.2c-2.3,0-4.2-1.9-4.2-4.2c0-2.3,1.9-4.2,4.2-4.2
			C41.5,73.6,43.4,75.5,43.4,77.8"/>
	</defs>
	<clipPath id="brainstormID_50_">
		<use xlink:href="#brainstormID_49_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_50_)">
		<defs>
			<rect id="brainstormID_51_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_52_">
			<use xlink:href="#brainstormID_51_"  overflow="visible"/>
		</clipPath>
		<rect x="30" y="68.6" clip-path="url(#brainstormID_52_)" fill="#464646" width="18.4" height="18.4"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_53_" d="M60.3,77.8c0,2.3-1.9,4.2-4.2,4.2c-2.3,0-4.2-1.9-4.2-4.2c0-2.3,1.9-4.2,4.2-4.2
			C58.4,73.6,60.3,75.5,60.3,77.8"/>
	</defs>
	<clipPath id="brainstormID_54_">
		<use xlink:href="#brainstormID_53_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_54_)">
		<defs>
			<rect id="brainstormID_55_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_56_">
			<use xlink:href="#brainstormID_55_"  overflow="visible"/>
		</clipPath>
		<rect x="46.8" y="68.6" clip-path="url(#brainstormID_56_)" fill="#464646" width="18.4" height="18.4"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_57_" d="M26.6,77.8c0,2.3-1.9,4.2-4.2,4.2s-4.2-1.9-4.2-4.2c0-2.3,1.9-4.2,4.2-4.2S26.6,75.5,26.6,77.8"/>
	</defs>
	<clipPath id="brainstormID_58_">
		<use xlink:href="#brainstormID_57_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_58_)">
		<defs>
			<rect id="brainstormID_59_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_60_">
			<use xlink:href="#brainstormID_59_"  overflow="visible"/>
		</clipPath>
		<rect x="13.2" y="68.6" clip-path="url(#brainstormID_60_)" fill="#464646" width="18.4" height="18.4"/>
	</g>
</g>
<g>
	<defs>
		<rect id="brainstormID_61_" x="-28" y="-5" width="207" height="207"/>
	</defs>
	<clipPath id="brainstormID_62_">
		<use xlink:href="#brainstormID_61_"  overflow="visible"/>
	</clipPath>
	
		<polyline clip-path="url(#brainstormID_62_)" fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
		70.6,118.8 70.6,110.4 7.8,110.4 7.8,118.8 	"/>
</g>
<g>
	<defs>
		<path id="brainstormID_63_" d="M14.8,133.6c0,3.9-3.1,7-7,7c-3.9,0-7-3.1-7-7s3.1-7,7-7C11.7,126.6,14.8,129.7,14.8,133.6"/>
	</defs>
	<clipPath id="brainstormID_64_">
		<use xlink:href="#brainstormID_63_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_64_)">
		<defs>
			<rect id="brainstormID_65_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_66_">
			<use xlink:href="#brainstormID_65_"  overflow="visible"/>
		</clipPath>
		<rect x="-4.2" y="121.6" clip-path="url(#brainstormID_66_)" fill="#FAB216" width="24" height="24"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_67_" d="M46.2,133.6c0,3.9-3.1,7-7,7c-3.9,0-7-3.1-7-7s3.1-7,7-7C43.1,126.6,46.2,129.7,46.2,133.6"/>
	</defs>
	<clipPath id="brainstormID_68_">
		<use xlink:href="#brainstormID_67_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_68_)">
		<defs>
			<rect id="brainstormID_69_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_70_">
			<use xlink:href="#brainstormID_69_"  overflow="visible"/>
		</clipPath>
		<rect x="27.2" y="121.6" clip-path="url(#brainstormID_70_)" fill="#FCCF06" width="24" height="24"/>
	</g>
</g>
<g>
	<defs>
		<path id="brainstormID_71_" d="M77.6,133.6c0,3.9-3.1,7-7,7c-3.9,0-7-3.1-7-7s3.1-7,7-7C74.5,126.6,77.6,129.7,77.6,133.6"/>
	</defs>
	<clipPath id="brainstormID_72_">
		<use xlink:href="#brainstormID_71_"  overflow="visible"/>
	</clipPath>
	<g clip-path="url(#brainstormID_72_)">
		<defs>
			<rect id="brainstormID_73_" x="-28" y="-5" width="207" height="207"/>
		</defs>
		<clipPath id="brainstormID_74_">
			<use xlink:href="#brainstormID_73_"  overflow="visible"/>
		</clipPath>
		<rect x="58.6" y="121.6" clip-path="url(#brainstormID_74_)" fill="#F48220" width="24" height="24"/>
	</g>
</g>
</svg>
