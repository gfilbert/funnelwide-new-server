<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 130 130" enable-background="new 0 0 130 130" xml:space="preserve">
<path fill="#FCCF06" d="M63.7,74.8l-7,3.7c-0.8,0.4-1.7,0.1-2.1-0.6c-0.2-0.3-0.2-0.7-0.2-1l1.3-7.8c0.1-0.5-0.1-1-0.5-1.4l-5.7-5.5
	c-0.6-0.6-0.6-1.6,0-2.2c0.2-0.3,0.6-0.4,0.9-0.5l7.8-1.1c0.5-0.1,1-0.4,1.2-0.9l3.5-7.1c0.4-0.8,1.3-1.1,2.1-0.7
	c0.3,0.2,0.6,0.4,0.7,0.7l3.5,7.1c0.2,0.5,0.7,0.8,1.2,0.9l7.8,1.1c0.9,0.1,1.5,0.9,1.3,1.8c0,0.3-0.2,0.7-0.5,0.9l-5.7,5.5
	C73.2,68,73,68.5,73.1,69l1.3,7.8c0.2,0.8-0.4,1.7-1.3,1.8c-0.3,0.1-0.7,0-1-0.2l-7-3.7C64.7,74.5,64.2,74.5,63.7,74.8z"/>
<circle fill="#F48220" cx="65" cy="43.4" r="1.5"/>
<circle fill="#FCCF06" cx="65" cy="37.5" r="1.5"/>
<circle fill="#FAB216" cx="65" cy="31.7" r="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -20.716 49.1124)" fill="#F48220" cx="48.9" cy="49.6" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -18.9955 44.9719)" fill="#FCCF06" cx="44.8" cy="45.4" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -17.2891 40.8314)" fill="#FAB216" cx="40.6" cy="41.3" rx="1.5" ry="1.5"/>
<circle fill="#F48220" cx="41.9" cy="65.3" r="1.5"/>
<circle fill="#FCCF06" cx="36.1" cy="65.3" r="1.5"/>
<circle fill="#FAB216" cx="30.2" cy="65.3" r="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -43.4462 57.8426)" fill="#F48220" cx="48.1" cy="81.4" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -47.5896 56.1292)" fill="#FCCF06" cx="44" cy="85.5" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -51.7201 54.4187)" fill="#FAB216" cx="39.8" cy="89.6" rx="1.5" ry="1.5"/>
<circle fill="#F48220" cx="63.8" cy="88.4" r="1.5"/>
<circle fill="#FCCF06" cx="63.8" cy="94.2" r="1.5"/>
<circle fill="#FAB216" cx="63.8" cy="100.1" r="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -34.713 80.5799)" fill="#F48220" cx="79.9" cy="82.2" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -36.4293 84.7162)" fill="#FCCF06" cx="84" cy="86.3" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -38.1428 88.8538)" fill="#FAB216" cx="88.2" cy="90.5" rx="1.5" ry="1.5"/>
<circle fill="#F48220" cx="86.9" cy="66.5" r="1.5"/>
<circle fill="#FCCF06" cx="92.8" cy="66.5" r="1.5"/>
<circle fill="#FAB216" cx="98.6" cy="66.5" r="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -11.9786 71.8455)" fill="#F48220" cx="80.7" cy="50.4" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -7.8452 73.563)" fill="#FCCF06" cx="84.9" cy="46.3" rx="1.5" ry="1.5"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -3.7047 75.2694)" fill="#FAB216" cx="89" cy="42.1" rx="1.5" ry="1.5"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M37,107.5
	C13.5,92.1,7,60.5,22.4,37C29.1,26.8,39.3,19.3,51,16"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M32.5,115.9H65
	c28.1,0,50.9-22.8,50.9-50.9S93.1,14.1,65,14.1"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="117.5" y1="115.9" x2="92" y2="115.9"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="114.8,110.4
	120.3,115.9 114.8,121.4 "/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="67.8,19.6
	62.2,14.1 67.8,8.6 "/>
<ellipse transform="matrix(0.9853 -0.1709 0.1709 0.9853 -19.5968 4.1622)" fill="#FAB216" cx="14.4" cy="115.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.4308 -0.9024 0.9024 0.4308 -91.2723 87.1102)" fill="#F48220" cx="23.4" cy="115.9" rx="3" ry="3"/>
</svg>
