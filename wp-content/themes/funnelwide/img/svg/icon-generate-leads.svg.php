<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="180.5,66.8
	164.2,66.8 164.2,50.5 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="205.2" y1="25.8" x2="165.9" y2="65.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="66.3,50.5
	66.3,66.8 50,66.8 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="25.3" y1="25.8" x2="64.6" y2="65.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="50,164.7
	66.3,164.7 66.3,181 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="25.3" y1="205.7" x2="64.6" y2="166.4"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="164.2,181
	164.2,164.7 180.5,164.7 "/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="205.2" y1="205.7" x2="165.9" y2="166.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -48.0996 115.3928)" fill="#FCCF06" cx="115.2" cy="115.8" rx="55" ry="55"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -48.0996 115.3928)" fill="#FAB216" cx="115.2" cy="115.8" rx="35" ry="35"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -48.0996 115.3928)" fill="#F48220" cx="115.2" cy="115.8" rx="15" ry="15"/>
<path fill="#FAB216" d="M87.4,34.1c-2.2,0-4-1.8-4-4s1.8-4,4-4c2.2,0,4,1.8,4,4c0,1.7-1.1,3.3-2.8,3.8C88.2,34,87.8,34.1,87.4,34.1z
	"/>
<path fill="#FCCF06" d="M29.5,91.9c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4c2.2,0,4,1.8,4,4c0,0.4-0.1,0.8-0.2,1.2
	C32.8,90.8,31.3,91.9,29.5,91.9z"/>
<circle fill="#F48220" cx="25.1" cy="115.8" r="4"/>
<path fill="#FAB216" d="M29.5,147.6c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4c2.2,0,4,1.8,4,4c0,1.7-1.1,3.3-2.8,3.8
	C30.4,147.5,30,147.6,29.5,147.6z"/>
<path fill="#F48220" d="M87.4,205.5c-2.2,0-4-1.8-4-4s1.8-4,4-4c2.2,0,4,1.8,4,4c0,0.4-0.1,0.8-0.2,1.2
	C90.7,204.3,89.1,205.5,87.4,205.5z"/>
<circle fill="#FCCF06" cx="115.2" cy="205.9" r="4"/>
<path fill="#FAB216" d="M143.1,205.5c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4c2.2,0,4,1.8,4,4c0,1.7-1.1,3.3-2.8,3.8
	C143.9,205.4,143.5,205.5,143.1,205.5z"/>
<path fill="#FCCF06" d="M200.9,147.6c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4c2.2,0,4,1.8,4,4c0,0.4-0.1,0.8-0.2,1.2
	C204.2,146.5,202.7,147.6,200.9,147.6z"/>
<circle fill="#F48220" cx="205.4" cy="115.8" r="4"/>
<path fill="#FAB216" d="M200.9,91.9c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4c2.2,0,4,1.8,4,4c0,1.7-1.1,3.3-2.8,3.8
	C201.8,91.9,201.4,91.9,200.9,91.9z"/>
<path fill="#F48220" d="M143.1,34.1c-2.2,0-4-1.8-4-4c0-2.2,1.8-4,4-4s4,1.8,4,4c0,0.4-0.1,0.8-0.2,1.2
	C146.4,32.9,144.8,34.1,143.1,34.1z"/>
<circle fill="#FCCF06" cx="115.2" cy="25.6" r="4"/>
</svg>
