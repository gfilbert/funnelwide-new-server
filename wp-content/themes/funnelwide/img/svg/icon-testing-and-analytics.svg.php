<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="84,218.8
	78.6,207.8 89.5,202.5 "/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="156.2,11.2
	161.6,22.1 150.7,27.5 "/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M38.1,174.5
	C5.3,129.2,15.4,65.9,60.7,33c28.9-20.9,66.6-25.2,99.4-11.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M202,55.5
	c32.8,45.3,22.7,108.6-22.6,141.5c-28.9,20.9-66.6,25.1-99.3,11.1"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-miterlimit="10" points="38.2,174.5 97.2,101.2 134.5,140.6
	202,55.5 "/>
<circle fill="#FCCF06" cx="97.2" cy="101.2" r="10.2"/>
<circle fill="#FAB216" cx="134.5" cy="140.6" r="10.2"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -95.2909 55.756)" fill="#FAB216" cx="38.2" cy="174.5" rx="10.2" ry="10.2"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 5.9868 129.3329)" fill="#F48220" cx="202" cy="55.5" rx="10.2" ry="10.2"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="76.4" y1="101.2" x2="51.1" y2="101.2"/>
<circle fill="#464646" cx="41.1" cy="101.2" r="2.6"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="148.2" y1="161.6" x2="122.8" y2="161.6"/>
<circle fill="#464646" cx="112.9" cy="161.6" r="2.6"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="180.5" y1="55.5" x2="155.1" y2="55.5"/>
<circle fill="#464646" cx="145.2" cy="55.5" r="2.6"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="148.2" y1="173.5" x2="122.8" y2="173.5"/>
<circle fill="#464646" cx="112.9" cy="173.5" r="2.6"/>
<circle fill="#F48220" cx="63.6" cy="200.7" r="5.1"/>
<circle fill="#FCCF06" cx="176.6" cy="29.3" r="5.1"/>
</svg>
