<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 311 275" enable-background="new 0 0 311 275" xml:space="preserve">
<path fill="none" stroke="#464646" stroke-width="3.9" stroke-linecap="round" stroke-miterlimit="10" d="M64,84.9
	C77,68.3,94.6,55.4,114.8,48"/>
<path fill="none" stroke="#464646" stroke-width="3.9" stroke-linecap="round" stroke-miterlimit="10" d="M59.4,218.8
	C47.8,201.6,40.8,181.1,40,159"/>
<path fill="none" stroke="#464646" stroke-width="3.9" stroke-linecap="round" stroke-miterlimit="10" d="M184.9,265
	c-10,2.9-20.5,4.4-31.4,4.4c-10.9,0-21.4-1.5-31.4-4.4"/>
<path fill="none" stroke="#464646" stroke-width="3.9" stroke-linecap="round" stroke-miterlimit="10" d="M267.4,159
	c-0.8,22.1-7.8,42.6-19.4,59.8"/>
<path fill="none" stroke="#464646" stroke-width="3.9" stroke-linecap="round" stroke-miterlimit="10" d="M193,48
	c20.2,7.4,37.8,20.3,50.8,36.9"/>
<line fill="none" stroke="#464646" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="143" y1="24" x2="143" y2="35.2"/>
<line fill="none" stroke="#464646" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="169.1" y1="55.5" x2="169.2" y2="65.3"/>
<path fill="none" stroke="#464646" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M151.2,2.2c1.7-0.5,3.4-0.8,5.3-0.9c11.3-0.4,20.7,8,21.6,19c0.1,1.1,0.3,2.2,0.7,3.2l4.6,12.3c0.3,0.7-0.2,1.4-1,1.4l-4,0.1
	l0.2,12.1c0,3.3-2.6,6-5.9,6l-8.7,0.1"/>
<path fill="none" stroke="#464646" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M147.5,65.5l-0.1-8c0-2.4-0.6-4.8-1.7-6.9"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_1_" d="M157.2,24.5h-28.4c-1.1,0-1.9-0.9-1.9-1.9V10.3c0-1.1,0.9-1.9,1.9-1.9h28.4c1.1,0,1.9,0.9,1.9,1.9v12.2
			C159.1,23.6,158.3,24.5,157.2,24.5"/>
	</defs>
	<clipPath id="FunnelwideServicesID_2_">
		<use xlink:href="#FunnelwideServicesID_1_"  overflow="visible"/>
	</clipPath>
	<rect x="121.8" y="3.4" clip-path="url(#FunnelwideServicesID_2_)" fill="#FCCF06" width="42.3" height="26.1"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_3_" d="M144.9,16.4c0,1.1-0.9,1.9-1.9,1.9c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9
			C144.1,14.5,144.9,15.3,144.9,16.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_4_">
		<use xlink:href="#FunnelwideServicesID_3_"  overflow="visible"/>
	</clipPath>
	<rect x="136" y="9.5" clip-path="url(#FunnelwideServicesID_4_)" fill="#464646" width="13.9" height="13.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_5_" d="M152.7,16.4c0,1.1-0.9,1.9-1.9,1.9c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9
			C151.8,14.5,152.7,15.3,152.7,16.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_6_">
		<use xlink:href="#FunnelwideServicesID_5_"  overflow="visible"/>
	</clipPath>
	<rect x="143.8" y="9.5" clip-path="url(#FunnelwideServicesID_6_)" fill="#464646" width="13.9" height="13.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_7_" d="M137.2,16.4c0,1.1-0.9,1.9-1.9,1.9c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9
			C136.3,14.5,137.2,15.3,137.2,16.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_8_">
		<use xlink:href="#FunnelwideServicesID_7_"  overflow="visible"/>
	</clipPath>
	<rect x="128.3" y="9.5" clip-path="url(#FunnelwideServicesID_8_)" fill="#464646" width="13.9" height="13.9"/>
</g>
<polyline fill="none" stroke="#464646" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	157.4,35.2 157.4,31.3 128.5,31.3 128.5,35.2 "/>
<g>
	<defs>
		<path id="FunnelwideServicesID_9_" d="M131.8,42c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2c0-1.8,1.4-3.2,3.2-3.2
			C130.3,38.8,131.8,40.2,131.8,42"/>
	</defs>
	<clipPath id="FunnelwideServicesID_10_">
		<use xlink:href="#FunnelwideServicesID_9_"  overflow="visible"/>
	</clipPath>
	<rect x="120.3" y="33.8" clip-path="url(#FunnelwideServicesID_10_)" fill="#FAB216" width="16.5" height="16.4"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_11_" d="M146.2,42c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2c0-1.8,1.4-3.2,3.2-3.2
			C144.8,38.8,146.2,40.2,146.2,42"/>
	</defs>
	<clipPath id="FunnelwideServicesID_12_">
		<use xlink:href="#FunnelwideServicesID_11_"  overflow="visible"/>
	</clipPath>
	<rect x="134.8" y="33.8" clip-path="url(#FunnelwideServicesID_12_)" fill="#FCCF06" width="16.5" height="16.4"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_13_" d="M160.6,42c0,1.8-1.4,3.2-3.2,3.2c-1.8,0-3.2-1.4-3.2-3.2c0-1.8,1.4-3.2,3.2-3.2
			C159.2,38.8,160.6,40.2,160.6,42"/>
	</defs>
	<clipPath id="FunnelwideServicesID_14_">
		<use xlink:href="#FunnelwideServicesID_13_"  overflow="visible"/>
	</clipPath>
	<rect x="149.2" y="33.8" clip-path="url(#FunnelwideServicesID_14_)" fill="#F48220" width="16.5" height="16.4"/>
</g>
<path fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M282.4,140.4c-7.3-1.5-13-7.3-14.5-14.6"/>
<path fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M304.7,126c-1.6,7.2-7.3,12.8-14.5,14.3"/>
<path fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M252.7,103.3c7.2,1.6,12.8,7.3,14.4,14.6"/>
<path fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M230.2,118c1.5-7.3,7.2-13.1,14.4-14.7"/>
<rect x="263.6" y="117.9" fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" width="7.8" height="7.9"/>
<rect x="301.3" y="117.9" fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" width="7.8" height="7.9"/>
<rect x="225.9" y="117.9" fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" width="7.8" height="7.9"/>
<polyline fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	290.3,140.4 290.3,144.7 282.4,144.7 282.4,136.9 290.3,136.9 290.3,140.4 "/>
<line fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="252.5" y1="102.9" x2="274.5" y2="102.9"/>
<line fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="222.8" y1="102.9" x2="244.7" y2="102.9"/>
<rect x="244.7" y="98.9" fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" width="7.8" height="7.9"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_15_" d="M274.5,106.8L274.5,106.8c-2.2,0-3.9-1.8-3.9-3.9c0-2.2,1.8-3.9,3.9-3.9c2.2,0,3.9,1.8,3.9,3.9
			C278.4,105,276.7,106.8,274.5,106.8"/>
	</defs>
	<clipPath id="FunnelwideServicesID_16_">
		<use xlink:href="#FunnelwideServicesID_15_"  overflow="visible"/>
	</clipPath>
	<rect x="265.6" y="93.9" clip-path="url(#FunnelwideServicesID_16_)" fill="#FCCF06" width="17.8" height="17.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_17_" d="M222.8,106.8L222.8,106.8c-2.2,0-3.9-1.8-3.9-3.9c0-2.2,1.8-3.9,3.9-3.9c2.2,0,3.9,1.8,3.9,3.9
			C226.7,105,224.9,106.8,222.8,106.8"/>
	</defs>
	<clipPath id="FunnelwideServicesID_18_">
		<use xlink:href="#FunnelwideServicesID_17_"  overflow="visible"/>
	</clipPath>
	<rect x="213.8" y="93.9" clip-path="url(#FunnelwideServicesID_18_)" fill="#FCCF06" width="17.8" height="17.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_19_" d="M251,121.8c0,1.3-1.1,2.4-2.3,2.4c-1.3,0-2.3-1.1-2.3-2.4c0-1.3,1.1-2.4,2.3-2.4
			C249.9,119.5,251,120.5,251,121.8"/>
	</defs>
	<clipPath id="FunnelwideServicesID_20_">
		<use xlink:href="#FunnelwideServicesID_19_"  overflow="visible"/>
	</clipPath>
	<rect x="241.3" y="114.5" clip-path="url(#FunnelwideServicesID_20_)" fill="#343434" width="14.7" height="14.7"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_21_" d="M288.7,121.8c0,1.3-1.1,2.4-2.3,2.4c-1.3,0-2.3-1.1-2.3-2.4c0-1.3,1.1-2.4,2.3-2.4
			C287.6,119.5,288.7,120.5,288.7,121.8"/>
	</defs>
	<clipPath id="FunnelwideServicesID_22_">
		<use xlink:href="#FunnelwideServicesID_21_"  overflow="visible"/>
	</clipPath>
	<rect x="279" y="114.5" clip-path="url(#FunnelwideServicesID_22_)" fill="#343434" width="14.7" height="14.7"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_23_" d="M261.2,141.2l-4.3,1.3c-0.5,0.2-1,0.6-1.1,1.1l-1.3,4.3c-0.5,1.6-2.7,1.6-3.3,0l-3.9-11.8
			c-0.4-1.3,0.8-2.6,2.2-2.2l11.7,3.9C262.8,138.5,262.8,140.7,261.2,141.2"/>
	</defs>
	<clipPath id="FunnelwideServicesID_24_">
		<use xlink:href="#FunnelwideServicesID_23_"  overflow="visible"/>
	</clipPath>
	<rect x="242.3" y="128.9" clip-path="url(#FunnelwideServicesID_24_)" fill="#343434" width="25.2" height="25.2"/>
</g>
<line fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="241.5" y1="240.3" x2="253.1" y2="240.3"/>
<polyline fill="none" stroke="#FCCF06" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	197.4,240.3 217,240.3 221.1,240.3 235.8,240.3 "/>
<polyline fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	208.7,250.2 223.2,250.2 227.3,250.2 252.4,250.2 "/>
<line fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="197.4" y1="250.2" x2="203.1" y2="250.2"/>
<line fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="233.9" y1="230.4" x2="258.3" y2="230.4"/>
<polyline fill="none" stroke="#F48220" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	197.4,230.4 209.7,230.4 213.8,230.4 228.2,230.4 "/>
<polyline fill="none" stroke="#FCCF06" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	197.4,260 201,260 205.1,260 210.6,260 214.7,260 226.2,260 "/>
<line fill="none" stroke="#FAB216" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="231.7" y1="260" x2="238.1" y2="260"/>
<polyline fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
	209.1,269.9 226.1,269.9 230.4,269.9 234,269.9 "/>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="197.4" y1="269.9" x2="203.4" y2="269.9"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_25_" d="M259.6,240.3c0,0.7-0.6,1.3-1.3,1.3c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3
			C259,239,259.6,239.6,259.6,240.3"/>
	</defs>
	<clipPath id="FunnelwideServicesID_26_">
		<use xlink:href="#FunnelwideServicesID_25_"  overflow="visible"/>
	</clipPath>
	<rect x="252" y="234" clip-path="url(#FunnelwideServicesID_26_)" fill="#F48220" width="12.6" height="12.6"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_27_" d="M243.9,260c0,0.7-0.6,1.3-1.3,1.3s-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3S243.9,259.3,243.9,260"/>
	</defs>
	<clipPath id="FunnelwideServicesID_28_">
		<use xlink:href="#FunnelwideServicesID_27_"  overflow="visible"/>
	</clipPath>
	<rect x="236.3" y="253.7" clip-path="url(#FunnelwideServicesID_28_)" fill="#FAB216" width="12.6" height="12.6"/>
</g>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="239.7" y1="269.9" x2="247.4" y2="269.9"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_29_" d="M279.7,230.7L279.7,230.7c-1.5-1.1-3.6-0.7-4.6,0.8l-1.1,1.6l5.5,3.9l1.1-1.6
			C281.5,233.9,281.2,231.8,279.7,230.7"/>
	</defs>
	<clipPath id="FunnelwideServicesID_30_">
		<use xlink:href="#FunnelwideServicesID_29_"  overflow="visible"/>
	</clipPath>
	<rect x="268.9" y="225.1" clip-path="url(#FunnelwideServicesID_30_)" fill="#F48220" width="17.2" height="16.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_31_" d="M251.9,267.5l2.9,2.1l-1.6,1.2c-1,0.7-2.3-0.2-1.9-1.4L251.9,267.5z"/>
	</defs>
	<clipPath id="FunnelwideServicesID_32_">
		<use xlink:href="#FunnelwideServicesID_31_"  overflow="visible"/>
	</clipPath>
	<rect x="246.3" y="262.5" clip-path="url(#FunnelwideServicesID_32_)" fill="#343434" width="13.6" height="13.5"/>
</g>
<g>
	<defs>
		<polygon id="FunnelwideServicesID_33_" points="279.3,237 273.9,233.2 272,235.8 277.5,239.7 		"/>
	</defs>
	<clipPath id="FunnelwideServicesID_34_">
		<use xlink:href="#FunnelwideServicesID_33_"  overflow="visible"/>
	</clipPath>
	<rect x="267" y="228.2" clip-path="url(#FunnelwideServicesID_34_)" fill="#8B8E90" width="17.3" height="16.5"/>
</g>
<g>
	<defs>
		<polygon id="FunnelwideServicesID_35_" points="272,235.8 253,263.3 258.4,267.2 277.5,239.7 		"/>
	</defs>
	<clipPath id="FunnelwideServicesID_36_">
		<use xlink:href="#FunnelwideServicesID_35_"  overflow="visible"/>
	</clipPath>
	<rect x="248" y="230.8" clip-path="url(#FunnelwideServicesID_36_)" fill="#FAB216" width="34.5" height="41.4"/>
</g>
<g>
	<defs>
		<polygon id="FunnelwideServicesID_37_" points="258.4,267.2 255.7,265.2 274.8,237.8 277.5,239.7 		"/>
	</defs>
	<clipPath id="FunnelwideServicesID_38_">
		<use xlink:href="#FunnelwideServicesID_37_"  overflow="visible"/>
	</clipPath>
	<rect x="250.7" y="232.8" clip-path="url(#FunnelwideServicesID_38_)" fill="#F48220" width="31.8" height="39.4"/>
</g>
<g>
	<defs>
		<polygon id="FunnelwideServicesID_39_" points="253,263.3 251.9,267.5 254.9,269.6 258.4,267.2 		"/>
	</defs>
	<clipPath id="FunnelwideServicesID_40_">
		<use xlink:href="#FunnelwideServicesID_39_"  overflow="visible"/>
	</clipPath>
	<rect x="246.9" y="258.3" clip-path="url(#FunnelwideServicesID_40_)" fill="#FCCF06" width="16.5" height="16.3"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_41_" d="M77.7,244.9c0,6.6-5.4,12-12.1,12c-6.7,0-12.1-5.4-12.1-12c0-6.6,5.4-12,12.1-12
			C72.3,232.9,77.7,238.2,77.7,244.9"/>
	</defs>
	<clipPath id="FunnelwideServicesID_42_">
		<use xlink:href="#FunnelwideServicesID_41_"  overflow="visible"/>
	</clipPath>
	<rect x="48.5" y="227.9" clip-path="url(#FunnelwideServicesID_42_)" fill="#FAB216" width="34.2" height="34.1"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_43_" d="M76.9,261.3l1-1.1l-5.3-6l-0.2-0.3c-2.1,1.7-4.7,2.6-7.6,2.6c-2.9,0-5.5-1-7.6-2.6l-5.8,6.6
			c-1.5,1.7-2.4,4-2.4,6.3v3.4h24.4C73.5,266.9,74.7,263.8,76.9,261.3"/>
	</defs>
	<clipPath id="FunnelwideServicesID_44_">
		<use xlink:href="#FunnelwideServicesID_43_"  overflow="visible"/>
	</clipPath>
	<rect x="44" y="248.9" clip-path="url(#FunnelwideServicesID_44_)" fill="#F48220" width="38.9" height="26.3"/>
</g>
<path fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
	M86.3,214.7c-2.8-1.5-6.3-0.5-8,2c-1.8-2.6-5.2-3.5-8-2c-3.1,1.6-4.3,5.3-2.7,8.4c1.5,2.9,5.7,6.1,8.8,8.2c0.7,0.5,1.4,0.9,2,1.3
	c0.6-0.4,1.2-0.8,2-1.3c3.1-2.1,7.3-5.4,8.8-8.2C90.6,220,89.4,216.3,86.3,214.7z"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_45_" d="M110.7,244.1c0,8-6.6,14.6-14.7,14.6c-8.1,0-14.7-6.5-14.7-14.6c0-8,6.6-14.6,14.7-14.6
			C104.1,229.5,110.7,236,110.7,244.1"/>
	</defs>
	<clipPath id="FunnelwideServicesID_46_">
		<use xlink:href="#FunnelwideServicesID_45_"  overflow="visible"/>
	</clipPath>
	<rect x="76.4" y="224.5" clip-path="url(#FunnelwideServicesID_46_)" fill="#FCCF06" width="39.3" height="39.1"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_47_" d="M86.9,255c2.5,2,5.7,3.2,9.2,3.2c3.5,0,6.7-1.2,9.2-3.2l0.3,0.3l6.8,7.6c1.9,2.1,2.9,4.8,2.9,7.6v4.2H76.9
			v-4.2c0-2.8,1-5.5,2.9-7.6L86.9,255z"/>
	</defs>
	<clipPath id="FunnelwideServicesID_48_">
		<use xlink:href="#FunnelwideServicesID_47_"  overflow="visible"/>
	</clipPath>
	<rect x="71.9" y="250" clip-path="url(#FunnelwideServicesID_48_)" fill="#FAB216" width="48.3" height="29.7"/>
</g>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="39.7" y1="93.8" x2="39.7" y2="86.6"/>
<path fill="none" stroke="#FCCF06" stroke-width="2.6" stroke-miterlimit="10" d="M33,85.8h13.3c0.9,0,1.6-0.7,1.6-1.6V71
	c0-0.9-0.7-1.6-1.6-1.6H33c-0.9,0-1.6,0.7-1.6,1.6v13.3C31.4,85.1,32.1,85.8,33,85.8z"/>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="25.7" y1="107.8" x2="18.5" y2="107.8"/>
<path fill="none" stroke="#343434" stroke-width="2.6" stroke-miterlimit="10" d="M17.7,114.4v-13.3c0-0.9-0.7-1.6-1.6-1.6H2.9
	c-0.9,0-1.6,0.7-1.6,1.6v13.3c0,0.9,0.7,1.6,1.6,1.6h13.3C17,116,17.7,115.3,17.7,114.4z"/>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="39.7" y1="121.7" x2="39.7" y2="129"/>
<path fill="none" stroke="#FAB216" stroke-width="2.6" stroke-miterlimit="10" d="M46.3,129.7H33c-0.9,0-1.6,0.7-1.6,1.6v13.3
	c0,0.9,0.7,1.6,1.6,1.6h13.3c0.9,0,1.6-0.7,1.6-1.6v-13.3C47.9,130.4,47.2,129.7,46.3,129.7z"/>
<line fill="none" stroke="#343434" stroke-width="2.6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="53.6" y1="107.8" x2="60.8" y2="107.8"/>
<path fill="none" stroke="#F48220" stroke-width="2.6" stroke-miterlimit="10" d="M61.6,101.1v13.3c0,0.9,0.7,1.6,1.6,1.6h13.3
	c0.9,0,1.6-0.7,1.6-1.6v-13.3c0-0.9-0.7-1.6-1.6-1.6H63.1C62.3,99.6,61.6,100.3,61.6,101.1z"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_49_" d="M31.2,118.2h16.9c1.1,0,2-0.9,2-2V99.3c0-1.1-0.9-2-2-2H31.2c-1.1,0-2,0.9-2,2v16.9
			C29.2,117.3,30.1,118.2,31.2,118.2"/>
	</defs>
	<clipPath id="FunnelwideServicesID_50_">
		<use xlink:href="#FunnelwideServicesID_49_"  overflow="visible"/>
	</clipPath>
	<rect x="24.2" y="92.3" clip-path="url(#FunnelwideServicesID_50_)" fill="#FAB216" width="30.9" height="30.9"/>
</g>
<line fill="none" stroke="#FFFFFF" stroke-width="1.3" stroke-linecap="round" stroke-miterlimit="10" x1="32.9" y1="102.7" x2="46.4" y2="102.7"/>
<line fill="none" stroke="#FFFFFF" stroke-width="1.3" stroke-linecap="round" stroke-miterlimit="10" x1="32.9" y1="107.5" x2="46.4" y2="107.5"/>
<line fill="none" stroke="#FFFFFF" stroke-width="1.3" stroke-linecap="round" stroke-miterlimit="10" x1="32.9" y1="112.8" x2="42.8" y2="112.8"/>
<g>
	<defs>
		<path id="FunnelwideServicesID_51_" d="M46.9,112.8c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9c0-0.5,0.4-0.9,0.9-0.9
			C46.5,111.9,46.9,112.3,46.9,112.8"/>
	</defs>
	<clipPath id="FunnelwideServicesID_52_">
		<use xlink:href="#FunnelwideServicesID_51_"  overflow="visible"/>
	</clipPath>
	<rect x="40.1" y="106.9" clip-path="url(#FunnelwideServicesID_52_)" fill="#FFFFFF" width="11.8" height="11.8"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_53_" d="M176.3,155.5c1.7,1.2,2.6,2.6,2.6,4.1c0,1.5-0.9,2.9-2.6,4.1c-1.4,1-3.2,1.8-5.6,2.5
			c-4.7,1.4-12.6,2-17.5,2.2c-2.9,0.1-7.2,0.4-9.9,1.1c-2.8,0.8-3.7,1.6-3.8,1.9c0.1,0.3,1.1,1.1,3.8,1.9c2.7,0.7,6.2,1.1,9.9,1.1
			c3.7,0,7.2-0.4,9.9-1.1c2.8-0.8,3.7-1.7,4-2.5c0.6-1.5,2.9-1,2.9,0.6c0,0,0,0,0,0c0,0.8-0.3,2-1.9,3c-0.9,0.6-2.2,1.2-3.7,1.7
			c-3,0.9-7,1.4-11.3,1.4c-4.2,0-8.2-0.5-11.3-1.4c-1.6-0.5-2.8-1-3.7-1.7c-1.5-1.1-1.9-2.3-1.9-3c0-0.8,0.3-2,1.9-3
			c0.9-0.6,2.2-1.2,3.7-1.7c3-0.9,6.8-1.3,11.3-1.4c4.9-0.1,12.2-0.7,16.6-2c4.1-1.2,5.9-2.7,5.9-3.6c0-0.5-0.9-1.4-2.1-2.1
			c-0.7-0.4-0.9-1.3-0.6-2c0.4-0.8,1.5-1.1,2.2-0.6C175.5,155,176.1,155.4,176.3,155.5"/>
	</defs>
	<clipPath id="FunnelwideServicesID_54_">
		<use xlink:href="#FunnelwideServicesID_53_"  overflow="visible"/>
	</clipPath>
	<rect x="131.4" y="149.6" clip-path="url(#FunnelwideServicesID_54_)" fill="#464646" width="52.5" height="32.8"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_55_" d="M131.3,151.4c0.8,0.2,1.6-0.2,1.9-0.9c0.3-0.9-0.1-1.8-1-2.1c-0.1,0-0.1,0-0.2-0.1c-5.8-1.7-7.9-3.8-7.9-5
			c0-1.2,2.1-3.2,7.9-5c3.6-1.1,7.9-1.8,12.6-2.2c-0.1-0.5-0.2-1.1-0.2-1.6c0-0.5,0.1-1,0.1-1.5c-5,0.4-9.7,1.2-13.5,2.4
			c-6.6,2-10.1,4.7-10.1,8c0,3.2,3.5,6,10.1,8C131.2,151.3,131.3,151.3,131.3,151.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_56_">
		<use xlink:href="#FunnelwideServicesID_55_"  overflow="visible"/>
	</clipPath>
	<rect x="116" y="128" clip-path="url(#FunnelwideServicesID_56_)" fill="#464646" width="33.6" height="28.4"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_57_" d="M176,135.4c-3.8-1.1-8.5-1.9-13.5-2.4c0.1,0.5,0.1,1,0.1,1.5c0,0.6-0.1,1.1-0.2,1.6
			c4.7,0.4,9,1.2,12.6,2.2c5.8,1.7,7.9,3.8,7.9,5c0,1.2-2.1,3.2-7.9,5c-5.7,1.7-14.2,2.5-21.3,2.6c-6.2,0.1-12.8,0.8-17.5,2.2
			c-2.4,0.7-4.3,1.5-5.6,2.5c-1.7,1.2-2.6,2.6-2.6,4.1c0,1.5,0.9,2.9,2.6,4.1c1.1,0.8,2.5,1.5,4.3,2.1c0.1,0,0.2,0.1,0.3,0.1
			c0.8,0.3,1.7-0.2,2-1c0.2-0.8-0.2-1.7-1-1.9c-0.2-0.1-0.5-0.2-0.6-0.2c-3-1.1-4.4-2.3-4.4-3.1c0-0.9,1.9-2.4,5.9-3.6
			c4.4-1.3,10.3-1.9,16.6-2c8.4-0.2,16.2-1,22.2-2.7c6.6-2,10.1-4.7,10.1-8C186.1,140.1,182.6,137.3,176,135.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_58_">
		<use xlink:href="#FunnelwideServicesID_57_"  overflow="visible"/>
	</clipPath>
	<rect x="123.2" y="128" clip-path="url(#FunnelwideServicesID_58_)" fill="#464646" width="67.9" height="43"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_59_" d="M153.2,144.9c-5.8,0-10.5-4.7-10.5-10.5c0-5.8,4.7-10.5,10.5-10.5c5.8,0,10.5,4.7,10.5,10.5
			C163.7,140.2,159,144.9,153.2,144.9 M153.2,127.4c-3.9,0-7.1,3.2-7.1,7.1c0,3.9,3.2,7.1,7.1,7.1c3.9,0,7.1-3.2,7.1-7.1
			C160.3,130.6,157.1,127.4,153.2,127.4"/>
	</defs>
	<clipPath id="FunnelwideServicesID_60_">
		<use xlink:href="#FunnelwideServicesID_59_"  overflow="visible"/>
	</clipPath>
	<rect x="137.7" y="119" clip-path="url(#FunnelwideServicesID_60_)" fill="#464646" width="31" height="30.9"/>
</g>
<g>
	<defs>
		<path id="FunnelwideServicesID_61_" d="M153.7,105.8c25.3,0,45.9,20.6,45.9,45.8c0,25.3-20.6,45.8-45.9,45.8c-25.3,0-45.9-20.5-45.9-45.8
			C107.8,126.4,128.4,105.8,153.7,105.8 M153.7,96C122.9,96,98,120.9,98,151.6c0,30.7,24.9,55.6,55.7,55.6
			c30.8,0,55.7-24.9,55.7-55.6C209.4,120.9,184.4,96,153.7,96"/>
	</defs>
	<clipPath id="FunnelwideServicesID_62_">
		<use xlink:href="#FunnelwideServicesID_61_"  overflow="visible"/>
	</clipPath>
	<rect x="93" y="91" clip-path="url(#FunnelwideServicesID_62_)" fill="#FCCF06" width="121.4" height="121.3"/>
</g>
</svg>
