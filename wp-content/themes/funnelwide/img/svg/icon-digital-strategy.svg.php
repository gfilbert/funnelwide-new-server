<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<circle fill="#F48220" cx="114.2" cy="43.7" r="4"/>
<circle fill="#FAB216" cx="114.2" cy="27.7" r="4"/>
<circle fill="#FCCF06" cx="114.2" cy="11.7" r="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 23.8886 148.8542)" fill="#FCCF06" cx="146.2" cy="52.9" rx="4" ry="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 39.3744 149.6665)" fill="#F48220" cx="154.7" cy="39.3" rx="4" ry="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 54.8688 150.4742)" fill="#FAB216" cx="163.2" cy="25.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -16.9701 81.7116)" fill="#FCCF06" cx="168.5" cy="77.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -12.4362 87.3034)" fill="#FAB216" cx="182.9" cy="70.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -7.9102 92.899)" fill="#F48220" cx="197.2" cy="63.6" rx="4" ry="4"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="77.6" y1="112.1" x2="77.6" y2="139.3"/>
<line fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="140.6" y1="188.6" x2="140.9" y2="212.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M97.3,59.3
	c4.1-1.3,8.4-2,12.7-2.2c26.9-1.1,49.9,19.2,52.2,46c0.2,2.6,0.8,5.2,1.7,7.7l11.1,29.9c0.5,1.3-0.2,2.8-1.5,3.3
	c-0.3,0.1-0.6,0.2-0.9,0.2l-9.8,0.1l0.4,29.4c0.1,8-6.3,14.5-14.2,14.6c0,0,0,0,0,0l-21,0.2"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M88.4,212.7l-0.3-19.4
	c-0.1-5.8-1.5-11.5-4.1-16.7"/>
<path fill="#FCCF06" d="M43.2,74.2h68.6c2.6,0,4.7,2.1,4.7,4.7v29.6c0,2.6-2.1,4.7-4.7,4.7H43.2c-2.6,0-4.7-2.1-4.7-4.7V78.9
	C38.5,76.3,40.6,74.2,43.2,74.2z"/>
<circle fill="#464646" cx="77.6" cy="93.7" r="4.7"/>
<circle fill="#464646" cx="96.3" cy="93.7" r="4.7"/>
<circle fill="#464646" cx="58.8" cy="93.7" r="4.7"/>
<polyline fill="none" stroke="#464646" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="112.4,139.3
	112.4,129.9 42.7,129.9 42.7,139.3 "/>
<circle fill="#FAB216" cx="42.7" cy="155.7" r="7.8"/>
<circle fill="#FCCF06" cx="77.6" cy="155.7" r="7.8"/>
<circle fill="#F48220" cx="112.4" cy="155.7" r="7.8"/>
</svg>
