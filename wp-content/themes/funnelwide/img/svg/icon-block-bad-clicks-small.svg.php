<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 130 130" enable-background="new 0 0 130 130" xml:space="preserve">
<path fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M44.9,51.6
	c0-1.5,0.9-2.9,2.2-3.6"/>
<path fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M60.9,54.1v-23
	c0-2.2-1.8-4-4-4s-4,1.8-4,4v23"/>
<path fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M68.9,56.5V45.9
	c0-2.2-1.8-4-4-4c-2.2,0-4,1.8-4,4v8.2"/>
<path fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M76.9,54.4v-5.7
	c0-2.2-1.8-4-4-4s-4,1.8-4,4v7.8"/>
<polyline fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="84.9,60.1
	84.9,63.5 80.9,82.9 80.9,92.8 "/>
<path fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M84.7,53.2
	c-0.7-2.1-3-3.2-5.1-2.5c-1.6,0.5-2.7,2.1-2.7,3.8v0.9"/>
<polyline fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="49.6,71.5
	56.9,82.9 56.9,86 "/>
<polyline fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="44.9,51.6
	44.9,63.5 46.2,65.4 "/>
<line fill="none" stroke="#FAB216" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="52.9" y1="54.1" x2="52.9" y2="59.4"/>
<polyline fill="none" stroke="#FCCF06" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="51.8,103.2
	51.8,92.8 80.9,92.8 80.9,109.8 "/>
<circle fill="#F48220" cx="56.9" cy="97.6" r="1.7"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" x1="8.1" y1="85" x2="124.5" y2="47.2"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M127.2,57.5
	c4.8,33.6-18.6,64.8-52.2,69.6c-29.6,4.2-58-13.5-67.2-41.9"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M5.2,73.1
	c-0.3-2.3-0.4-4.7-0.4-7c0-34,27.6-61.5,61.5-61.5c26.6,0,50.2,17.2,58.5,42.5"/>
<path fill="none" stroke="#F48220" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M46.9,33.9
	c-2.5-5.5-0.1-12.1,5.4-14.6c1.4-0.7,3-1,4.6-1"/>
<path fill="none" stroke="#F48220" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" d="M66.9,33.9
	c2.5-5.5,0.1-12.1-5.4-14.6c-1.4-0.7-3-1-4.6-1"/>
</svg>
