<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 130 130" enable-background="new 0 0 130 130" xml:space="preserve">
<path fill="#FCCF06" d="M55.1,83L55.1,83c-6.4-9.1-9.9-19.9-9.9-31l0,0c0-10.6,6.4-20.1,16.2-24c1.2-0.5,2.4-0.8,3.7-1.1
	c-1.5-0.3-3.1-0.5-4.6-0.6C47,26,35.7,36.2,34.7,49.6c-0.1,1.3-0.4,2.6-0.8,3.8l-5.3,15c-0.2,0.7,0.1,1.4,0.8,1.6
	c0.1,0.1,0.3,0.1,0.4,0.1h4.9v14.6c0,4,3.2,7.2,7.2,7.2l0,0H49v11.7h9.5V94C58.6,90.1,57.4,86.2,55.1,83z"/>
<path fill="#F48220" d="M101.4,68.4l-5.3-15c-0.4-1.2-0.7-2.5-0.8-3.8C94.3,36.2,83,26,69.6,26.3c-1.6,0-3.1,0.2-4.6,0.6
	c1.2,0.3,2.5,0.6,3.6,1.1c9.8,3.9,16.2,13.4,16.2,24l0,0c0,11.1-3.5,21.9-9.9,31l0,0c-2.3,3.2-3.5,7.1-3.5,11v9.7h9.6V91.9h7.2
	c4,0,7.2-3.2,7.2-7.2c0,0,0,0,0,0V70.1h4.9c0.7,0,1.3-0.6,1.3-1.3C101.5,68.7,101.4,68.6,101.4,68.4z"/>
<path fill="#FAB216" d="M74.9,83L74.9,83c6.4-9.1,9.9-19.9,9.9-31l0,0c0-10.6-6.4-20.1-16.2-24c-1.2-0.5-2.4-0.8-3.6-1.1
	c-1.2,0.3-2.5,0.6-3.7,1.1c-9.8,3.9-16.2,13.4-16.2,24l0,0c0,11.1,3.5,21.9,9.9,31l0,0c2.3,3.2,3.5,7.1,3.5,11v9.7h12.8V94
	C71.4,90.1,72.6,86.2,74.9,83z"/>
<ellipse transform="matrix(0.2589 -0.9659 0.9659 0.2589 -6.4879 86.1991)" fill="#FCCF06" cx="52.9" cy="47.3" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -25.8958 57.0925)" fill="#F48220" cx="56" cy="59.8" rx="2" ry="2"/>
<ellipse transform="matrix(0.9659 -0.2588 0.2588 0.9659 -14.0069 19.8218)" fill="#FCCF06" cx="68.3" cy="63.1" rx="2" ry="2"/>
<ellipse transform="matrix(0.2588 -0.9659 0.9659 0.2588 5.0457 114.7626)" fill="#F48220" cx="77.3" cy="54.1" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -7.8612 64.5613)" fill="#FCCF06" cx="74" cy="41.8" rx="2" ry="2"/>
<ellipse transform="matrix(0.9659 -0.2588 0.2588 0.9659 -7.8562 17.2763)" fill="#F48220" cx="61.7" cy="38.5" rx="2" ry="2"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="12.3,75.6
	9.4,81.3 3.8,78.4 "/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M8.9,80
	c-3.1-11.7-2.5-24.1,1.7-35.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -23.3656 20.6559)" fill="#FCCF06" cx="13.3" cy="38.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -17.6056 21.6359)" fill="#F48220" cx="17.3" cy="32.1" rx="1.5" ry="1.5"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="75.6,117.7
	81.3,120.6 78.4,126.2 "/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M80,121.1
	c-11.7,3.1-24.1,2.5-35.4-1.7"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -71.2696 61.4456)" fill="#FCCF06" cx="38.5" cy="116.8" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -69.9925 56.3886)" fill="#F48220" cx="33.1" cy="112.7" rx="1.5" ry="1.5"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="117.7,54.4
	120.6,48.7 126.2,51.6 "/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M121.1,50
	c3.1,11.7,2.5,24.1-1.7,35.4"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -30.4828 109.3425)" fill="#FCCF06" cx="116.7" cy="91.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 -35.9457 109.0655)" fill="#F48220" cx="113.7" cy="97.9" rx="1.5" ry="1.5"/>
<polyline fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" points="54.4,12.3
	48.7,9.4 51.6,3.8 "/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M50,8.9
	c11.7-3.1,24.1-2.5,35.4,1.7"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 17.4212 68.5528)" fill="#FCCF06" cx="91.5" cy="13.2" rx="2" ry="2"/>
<ellipse transform="matrix(0.7071 -0.7071 0.7071 0.7071 16.7241 75.0216)" fill="#F48220" cx="98.9" cy="17.3" rx="1.5" ry="1.5"/>
</svg>
