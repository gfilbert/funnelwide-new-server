<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 270 270" enable-background="new 0 0 270 270" xml:space="preserve">
<circle fill="#464646" cx="135" cy="135" r="135"/>
<line fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="145.5" y1="117.6" x2="166.5" y2="117.6"/>
<polyline fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="65.7,117.6
	101.1,117.6 108.6,117.6 135.3,117.6 "/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="86.2,135.3
	112.3,135.3 119.8,135.3 165.3,135.3 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="65.7" y1="135.3" x2="75.9" y2="135.3"/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="131.8" y1="100" x2="175.8" y2="100"/>
<polyline fill="none" stroke="#F48220" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="65.7,100
	87.9,100 95.4,100 121.5,100 "/>
<polyline fill="none" stroke="#FCCF06" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="65.7,152.9
	72.2,152.9 79.7,152.9 89.5,152.9 97.1,152.9 117.8,152.9 "/>
<line fill="none" stroke="#FAB216" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="127.8" y1="152.9" x2="139.4" y2="152.9"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="86.9,170.6
	117.6,170.6 125.4,170.6 132,170.6 "/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="65.7" y1="170.6" x2="76.6" y2="170.6"/>
<circle fill="#F48220" cx="175.8" cy="117.6" r="2.4"/>
<circle fill="#FAB216" cx="147.6" cy="152.9" r="2.4"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="142.2" y1="170.6" x2="156.2" y2="170.6"/>
<path fill="#F48220" d="M214.5,100.4L214.5,100.4c2.7,1.9,3.4,5.6,1.5,8.4l-2.1,2.9l0,0l-9.8-6.9l0,0l2-2.9
	C208,99.2,211.8,98.5,214.5,100.4C214.5,100.4,214.5,100.4,214.5,100.4z"/>
<path fill="#FFFFFF" d="M164.4,166.3l5.3,3.7l-2.8,2.1c-1,0.7-2.4,0.5-3.1-0.5c-0.4-0.6-0.5-1.3-0.3-2L164.4,166.3z"/>
<rect x="204.5" y="104.7" transform="matrix(0.5746 -0.8185 0.8185 0.5746 -2.3786 216.8044)" fill="#8C8E90" width="5.8" height="12.1"/>
<rect x="158.4" y="131.6" transform="matrix(0.5746 -0.8185 0.8185 0.5746 -32.4821 212.7896)" fill="#FAB216" width="60" height="12.1"/>
<rect x="160.9" y="136.3" transform="matrix(0.5746 -0.8185 0.8185 0.5746 -32.8437 215.5314)" fill="#F48220" width="60" height="6"/>
<polygon fill="#FCCF06" points="166.3,158.7 164.4,166.3 169.7,170 176.1,165.7 "/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M191.5,223.7
	c-35.8,23.8-82.4,23.8-118.2,0"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" d="M73.3,46.2
	c35.8-23.8,82.4-23.8,118.2,0"/>
<path fill="#F48220" d="M54,62.3c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C55.8,63.5,54.8,63,54,62.3z"/>
<path fill="#FCCF06" d="M205.1,62.3c-1.6-1.6-1.6-4.1,0-5.7s4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.7-1.7,1.2-2.8,1.2
	C206.9,63.5,205.9,63,205.1,62.3z"/>
<path fill="#FAB216" d="M54.1,213.3c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C55.9,214.5,54.9,214.1,54.1,213.3z"/>
<path fill="#F48220" d="M205.1,213.3c-1.6-1.6-1.6-4.1,0-5.7c1.6-1.6,4.1-1.6,5.7,0c1.6,1.6,1.6,4.1,0,5.7c-0.7,0.8-1.8,1.2-2.8,1.2
	C206.8,214.5,205.8,214.1,205.1,213.3z"/>
</svg>
