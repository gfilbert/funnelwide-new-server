<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="192px" height="394.1px" viewBox="0 0 192 394.1" enable-background="new 0 0 192 394.1" xml:space="preserve">
<g>
		<line fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-miterlimit="10" x1="56.2" y1="2" x2="56.2" y2="42"/>
	<path fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
		M9,71l0-19c0-5.5,4.5-10,10-10h161c5.5,0,10,4.5,10,10v6.3V326c0,5.5-4.5,10-10,10H61c-5.5,0-10,4.5-10,10v46.1"/>

		<polyline fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
		15.8,68 8.9,74.9 2,68 	"/>

		<polyline fill="none" stroke="#FFB502" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
		57.9,385.3 51,392.1 44.1,385.3 	"/>
</g>
</svg>
