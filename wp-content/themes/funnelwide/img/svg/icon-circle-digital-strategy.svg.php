<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 270 270" enable-background="new 0 0 270 270" xml:space="preserve">
<circle fill="#464646" cx="135" cy="135" r="135"/>
<circle fill="#F48220" cx="131.2" cy="68.5" r="4"/>
<circle fill="#FAB216" cx="131.2" cy="52.5" r="4"/>
<circle fill="#FCCF06" cx="131.2" cy="36.5" r="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 10.8291 174.8789)" fill="#FCCF06" cx="163.2" cy="77.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 26.3235 175.6866)" fill="#F48220" cx="171.6" cy="64.1" rx="4" ry="4"/>
<ellipse transform="matrix(0.5299 -0.848 0.848 0.5299 41.8226 176.5027)" fill="#FAB216" cx="180.1" cy="50.5" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -26.1214 91.6454)" fill="#FCCF06" cx="185.4" cy="102.4" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -21.591 97.24)" fill="#FAB216" cx="199.8" cy="95.4" rx="4" ry="4"/>
<ellipse transform="matrix(0.8988 -0.4384 0.4384 0.8988 -17.0571 102.8318)" fill="#F48220" cx="214.2" cy="88.4" rx="4" ry="4"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="94.5" y1="136.9" x2="94.5" y2="164.1"/>
<line fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" x1="157.5" y1="213.3" x2="157.9" y2="236.9"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M114.2,84.1
	c4.1-1.3,8.4-2,12.7-2.1c26.9-1.1,49.9,19.2,52.2,46c0.2,2.6,0.8,5.2,1.7,7.7l11.1,29.9c0.5,1.3-0.2,2.8-1.5,3.3
	c-0.3,0.1-0.6,0.2-0.9,0.2l-9.8,0.1l0.4,29.4c0.1,8-6.3,14.5-14.2,14.6l-21,0.2"/>
<path fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" d="M105.4,237.5l-0.3-19.4
	c-0.1-5.8-1.5-11.5-4.1-16.6"/>
<path fill="#FCCF06" d="M60.2,99h68.6c2.6,0,4.7,2.1,4.7,4.7v29.6c0,2.6-2.1,4.7-4.7,4.7H60.2c-2.6,0-4.7-2.1-4.7-4.7v-29.6
	C55.5,101.1,57.6,99,60.2,99z"/>
<circle fill="#464646" cx="94.5" cy="118.5" r="4.7"/>
<circle fill="#464646" cx="113.2" cy="118.5" r="4.7"/>
<circle fill="#464646" cx="75.8" cy="118.5" r="4.7"/>
<polyline fill="none" stroke="#FFFFFF" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" points="129.4,164.1
	129.4,154.7 59.6,154.7 59.6,164.1 "/>
<circle fill="#FAB216" cx="59.6" cy="180.5" r="7.8"/>
<circle fill="#FCCF06" cx="94.5" cy="180.5" r="7.8"/>
<circle fill="#F48220" cx="129.4" cy="180.5" r="7.8"/>
</svg>
