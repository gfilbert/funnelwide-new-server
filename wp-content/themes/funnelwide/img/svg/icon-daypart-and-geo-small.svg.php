<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 130 130" enable-background="new 0 0 130 130" xml:space="preserve">
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M17.5,120.2
	c-5.8-26.2,10.8-52.2,37-58s52.2,10.8,58,37c1.5,6.9,1.5,14,0,20.9"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="65" y1="61" x2="65" y2="120.2"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="113.6" y1="109.6" x2="16.4" y2="109.6"/>
<line fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" x1="21.2" y1="88.5" x2="108.8" y2="88.5"/>
<path fill="none" stroke="#464646" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M38.1,120.2
	c-0.4-3.5-0.7-7-0.7-10.5C37.4,82.8,49.8,61,65,61s27.6,21.8,27.6,48.6c0,3.5-0.2,7-0.7,10.5"/>
<path fill="#FCCF06" d="M94.7,61.4c3.5-16.4-6.9-32.5-23.3-36.1s-32.5,6.9-36.1,23.3c-0.9,4.2-0.9,8.6,0,12.8c0.2,0.8,1,1.3,1.8,1.2
	c0.2,0,0.3-0.1,0.4-0.2c16.9-9.9,37.9-9.9,54.8,0c0.7,0.4,1.7,0.2,2.1-0.6C94.6,61.7,94.6,61.5,94.7,61.4z"/>
<circle fill="#F48220" cx="28.6" cy="55" r="2"/>
<circle fill="#FCCF06" cx="20.8" cy="55" r="2"/>
<circle fill="#FAB216" cx="13" cy="55" r="2"/>
<ellipse transform="matrix(0.5 -0.866 0.866 0.5 -15.1169 47.4157)" fill="#F48220" cx="33.5" cy="36.8" rx="2" ry="2"/>
<ellipse transform="matrix(0.4983 -0.867 0.867 0.4983 -14.9435 39.7386)" fill="#FCCF06" cx="26.9" cy="32.8" rx="2" ry="2"/>
<ellipse transform="matrix(0.4983 -0.867 0.867 0.4983 -14.9801 31.9052)" fill="#FAB216" cx="20.1" cy="28.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.866 -0.5 0.5 0.866 -5.4771 26.5623)" fill="#F48220" cx="46.8" cy="23.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.866 -0.5 0.5 0.866 -2.6135 23.6963)" fill="#FCCF06" cx="42.9" cy="16.7" rx="2" ry="2"/>
<ellipse transform="matrix(0.866 -0.5 0.5 0.866 0.2515 20.8326)" fill="#FAB216" cx="39" cy="9.9" rx="2" ry="2"/>
<circle fill="#F48220" cx="65" cy="18.6" r="2"/>
<circle fill="#FCCF06" cx="65" cy="10.8" r="2"/>
<circle fill="#FAB216" cx="65" cy="3" r="2"/>
<ellipse transform="matrix(0.5 -0.866 0.866 0.5 21.2444 83.7844)" fill="#F48220" cx="83.2" cy="23.5" rx="2" ry="2"/>
<ellipse transform="matrix(0.5 -0.866 0.866 0.5 29.0644 83.7767)" fill="#FCCF06" cx="87.1" cy="16.7" rx="2" ry="2"/>
<ellipse transform="matrix(0.5 -0.866 0.866 0.5 36.8844 83.779)" fill="#FAB216" cx="91" cy="9.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.867 -0.4983 0.4983 0.867 -5.5239 53.1845)" fill="#F48220" cx="96.9" cy="36.9" rx="2" ry="2"/>
<ellipse transform="matrix(0.867 -0.4983 0.4983 0.867 -2.6687 56.0495)" fill="#FCCF06" cx="103.7" cy="33" rx="2" ry="2"/>
<ellipse transform="matrix(0.866 -0.5 0.5 0.866 0.2499 58.8973)" fill="#FAB216" cx="110" cy="29" rx="2" ry="2"/>
<circle fill="#F48220" cx="101.4" cy="55" r="2"/>
<circle fill="#FCCF06" cx="109.2" cy="55" r="2"/>
<circle fill="#FAB216" cx="117" cy="55" r="2"/>
</svg>
