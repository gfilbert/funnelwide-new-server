<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  width="100%" height="100%"  viewBox="0 0 230 230" enable-background="new 0 0 230 230" xml:space="preserve">
<circle fill="#FCCF06" cx="115.7" cy="114.2" r="36.2"/>
<circle fill="#464646" cx="115.7" cy="114.2" r="18.5"/>
<circle fill="#FFFFFF" cx="103" cy="101.6" r="11.1"/>
<path fill="none" stroke="#464646" stroke-width="4" stroke-miterlimit="10" d="M177.5,114.2c0,0-20,36.2-61.8,36.2
	s-61.8-36.2-61.8-36.2S73.8,78,115.7,78S177.5,114.2,177.5,114.2z"/>
<circle fill="#F48220" cx="113.9" cy="56.8" r="4"/>
<circle fill="#F48220" cx="113.9" cy="41.7" r="4"/>
<circle fill="#FCCF06" cx="113.9" cy="25.1" r="4"/>
<circle fill="#FAB216" cx="113.9" cy="8.4" r="4"/>
<circle fill="#F48220" cx="113.9" cy="170.1" r="4"/>
<circle fill="#F48220" cx="113.9" cy="186.7" r="4"/>
<circle fill="#FCCF06" cx="113.9" cy="203.3" r="4"/>
<circle fill="#F48220" cx="113.9" cy="219.9" r="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -23.7147 59.0335)" fill="#FCCF06" cx="79" cy="66" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -17.6776 50.7166)" fill="#F48220" cx="69.2" cy="52.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -11.6422 42.4106)" fill="#FAB216" cx="59.4" cy="39.1" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -5.5982 34.1032)" fill="#F48220" cx="49.7" cy="25.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.309 -0.9511 0.9511 0.309 -58.8655 104.5056)" fill="#FCCF06" cx="42.5" cy="92.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.309 -0.9511 0.9511 0.309 -64.9006 85.9158)" fill="#FAB216" cx="26.7" cy="87.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 -98.7945 130.8181)" fill="#F48220" cx="79" cy="162.4" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 -113.6994 128.4652)" fill="#FAB216" cx="69.2" cy="175.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 -128.6029 126.1033)" fill="#F48220" cx="59.4" cy="189.3" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 -143.503 123.7438)" fill="#F48220" cx="49.7" cy="202.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.9511 -0.309 0.309 0.9511 -39.8286 19.7635)" fill="#FCCF06" cx="42.5" cy="135.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.9511 -0.309 0.309 0.9511 -42.1874 15.1327)" fill="#F48220" cx="26.7" cy="140.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -66.3339 120.5592)" fill="#FCCF06" cx="152.4" cy="162.4" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -72.371 128.8761)" fill="#F48220" cx="162.1" cy="175.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -78.4064 137.1821)" fill="#F48220" cx="171.9" cy="189.2" rx="4" ry="4"/>
<ellipse transform="matrix(0.809 -0.5878 0.5878 0.809 -84.4455 145.4931)" fill="#FAB216" cx="181.7" cy="202.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.3102 -0.9507 0.9507 0.3102 1.0618 273.3476)" fill="#F48220" cx="188.9" cy="135.9" rx="4" ry="4"/>
<ellipse transform="matrix(0.309 -0.9511 0.9511 0.309 7.5577 291.9041)" fill="#FAB216" cx="204.7" cy="140.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 9.215 150.5716)" fill="#F48220" cx="152.4" cy="66.2" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 24.1199 152.9245)" fill="#FCCF06" cx="162.1" cy="52.8" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 39.0275 155.2945)" fill="#F48220" cx="171.9" cy="39.3" rx="4" ry="4"/>
<ellipse transform="matrix(0.5878 -0.809 0.809 0.5878 53.9325 157.6474)" fill="#FAB216" cx="181.7" cy="25.9" rx="4" ry="4"/>
<ellipse transform="matrix(0.9511 -0.309 0.309 0.9511 -19.4912 62.9127)" fill="#FCCF06" cx="188.9" cy="93" rx="4" ry="4"/>
<ellipse transform="matrix(0.9507 -0.3102 0.3102 0.9507 -17.07 67.7682)" fill="#F48220" cx="204.5" cy="87.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.9511 -0.309 0.309 0.9511 -14.772 72.1781)" fill="#F48220" cx="220.5" cy="82.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.309 -0.9511 0.9511 0.309 -71.1638 67.4992)" fill="#F48220" cx="10.9" cy="82.7" rx="4" ry="4"/>
<ellipse transform="matrix(0.309 -0.9511 0.9511 0.309 13.8209 310.3207)" fill="#F48220" cx="220.5" cy="145.6" rx="4" ry="4"/>
<ellipse transform="matrix(0.9511 -0.309 0.309 0.9511 -44.4772 10.4866)" fill="#F48220" cx="10.9" cy="145.7" rx="4" ry="4"/>
</svg>
