<?php

global $post;
$post = get_page_by_title( 'blog', OBJECT );
setup_postdata( $post );

get_header();

//Do something
wp_reset_postdata();
?>
  <section class="Blog-posts">
    <main id="main" class="Blog-posts__list" role="main">
      <div class="text-center">
        <span style="color:#fff;">Category</span>
        <h2><?php single_cat_title(); ?></h2>
      </div>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article class="Blog-posts__item  Card" href="<?php the_permalink() ?>">
          <div class="Card__image">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="Card__content">
            <h3 class="card-post-title"><?php the_title(); ?></h3>
            <p class="card-post-date"><?php the_date(); ?></p>
			<p class="card-post-excerpt"><?php the_excerpt(); ?></p>
            <a href="<?php the_permalink(); ?>" class="Button Button--ghost">Read Article</a>
          </div>
        </article>
      <?php endwhile;endif; ?>
      <?php $pagination =  paginate_links( array('total' => $query->max_num_pages, 'type' => 'array', 'prev_next' => false)); ?>

      <ul class="Pagination">
        <li class="Pagination__prev"><?php echo get_previous_posts_link('Previous'); ?></li>

        <li class="Pagination__pages">
          <?php foreach ($pagination as $item): ?>
            <?php echo $item; ?>
          <?php endforeach; ?>
        </li
        >
        <li class="Pagination__prev"><?php echo next_posts_link( 'Next' , $query->max_num_pages ); ?></li>
      </ul>
    </main><!-- #main -->
    <aside class="Blog-posts__categories">
      <h3 class="Blog-posts__categories__heading">Categories</h3>
      <ul class="Vertical-menu">
        <?php $terms = get_terms('category');  ?>
        <?php foreach ($terms as $term):?>
          <li class="Vertical-menu__item"><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
        <?php endforeach; ?>
      </ul>
    </aside>
  </section>

<?php
get_footer();
