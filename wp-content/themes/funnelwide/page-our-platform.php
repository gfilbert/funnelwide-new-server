<?php get_header(); ?>

<div class="site-stick-marker"></div>

<?php include(locate_template('components/flowchart-menu.php')); ?>
 <!-- End Sidebar -->

<div class="Panel Panel--center Panel--soft-double">
  <div class="Panel__container">
      <?php include(locate_template('components/flowchart.php')); ?>
  </div>
</div>


<div class="Panel Panel--soft Panel--fixed Panel--tall Panel--neon-carrot Panel--center">
  <div class="Panel__container">
    <div class="Accent--platform-panel-accent-one">
      <?php get_template_part('img/svg/platform', 'panel-accent-one.svg'); ?>
    </div>
    <div class="Grid">
      <div class="Grid__col-8-12">
        <h3 class="panel--fixed__copy"><?php the_field('orange_panel_content'); ?></h3>
    </div>
    </div>
  <div class="Accent--platform-panel-accent-two">
      <?php get_template_part('img/svg/platform', 'panel-accent-two.svg'); ?>
    </div>
  </div>
</div>

<div class="Grid Grid--soft Grid--align-right">
  <div class="Grid__col-10-12">

    <div class="Panel Panel--soft">
      <div class="Panel__container">
        <div id="retargeting"  class="Flowchart-menu__anchor">
          <div class="Icon-copy Icon-copy--group">
            <div class="Icon-copy__icon">
              <?php get_template_part('img/svg/icon', 'funnel-retargeting-small.svg'); ?>
            </div>
            <div class="Icon-copy__content">
              <?php the_field('retargeting_content'); ?>
            </div>
          </div>
        </div>

        <div id="testing" class="Flowchart-menu__anchor">
        <div class="Icon-copy Icon-copy--flipped Icon-copy--group">
          <div class="Icon-copy__icon">
            <?php get_template_part('img/svg/icon', 'funnel-clickpath-small.svg'); ?>
          </div>
          <div class="Icon-copy__content">
            <?php the_field('testing_content'); ?>
          </div>
        </div>
      </div>
      </div>
    </div>

        <div id="predictions"  class="Flowchart-menu__anchor">
        <div class="Panel Panel--soft Panel--center">
            <div class="Panel__container">
              <?php the_field('predictions_content'); ?>

              <div class="Calculator">
                <div class="Calculator__item" id="calculatorToggleAudienceSize">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Audience <br> Size</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
                <div class="Calculator__item" id="calculatorToggleAdCTR">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Ad <br> CTR</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
                <div class="Calculator__item" id="calculatorToggleSponsoredContentCTR">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Sponsored Content CTR</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
                <div class="Calculator__item" id="calculatorToggleShoppingCartCTR">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Shopping <br> Cart CTR</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
                <div class="Calculator__item Calculator__item--selective-yellow" id="calculatorToggleConversions">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Number of <br> Conversions</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
                <div class="Calculator__item Calculator__item--hide-small" id="calculatorToggleCostPerConversion">
                  <div class="Calculator__slider"></div>
                  <div class="Calculator__label">Cost Per Conversion</div>
                  <input disabled type="text" name="" class="Calculator__value" value="">
                </div>
              </div>

            </div>
        </div>
      </div>

  </div>
</div> <!-- Grid end -->
<div class="Panel Panel--soft-double">
  <div class="Panel__container">
<div class="site-stick-marker"></div>
    <div id="optimization" class="Flowchart-menu__anchor">
      <div class="Grid Grid--align-right Grid--soft">
        <div class="Grid__col-4-12 Grid--soft-small-screen">
          <?php the_field('optimization_content'); ?>
          <?php if( have_rows('optimization_repeater') ): ?>

            <?php while ( have_rows('optimization_repeater') ) : the_row();
              $title = get_sub_field('optimization_title');
              $description = get_sub_field('optimization_description');
              $id = get_sub_field('rollover-id');
            ?>

            <div class='optimization-rollover optimization-<?php echo $id ?>'>
              <h1 class='beta'><?php echo $title ?></h1>
              <p><?php echo $description ?></p>
            </div>

            <?php endwhile; ?>

          <?php endif; ?>
        </div>
        <div class="Grid__col-6-12">
          <div class="Grid text-center">
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-bids">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'bids-and-conversion-small.svg'); ?>
              </div>
              <p>Bids &amp; <br> Conversion</p>
            </div>
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-block-bad-clicks">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'block-bad-clicks-small.svg'); ?>
              </div>
              <p>Block Bad <br> Quality Clicks</p>
            </div>
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-demographs">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'demo-and-psychographic-small.svg'); ?>
              </div>
              <p>Demographics &amp; <br> Psychographics</p>
            </div>
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-pub-net">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'publisher-network-small.svg'); ?>
              </div>
              <p>Publisher, Platform <br> &amp; Network</p>
            </div>
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-day-geo">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'daypart-and-geo-small.svg'); ?>
              </div>
              <p>Daypart &amp; <br> Geotarget</p>
            </div>
            <div class="Grid__col-4-12 Grid__col-4-12--small rollover-trigger optimization-refresh">
              <div class="Icon Icon--small">
                <?php get_template_part('img/svg/icon', 'refreshed-creative-small.svg'); ?>
              </div>
              <p>Refresh Aging <br> Creative Assets</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?php get_footer();
